@layout('tema_luna/layout')
@section('contenido')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <div class="row mb-3">
            <div class="col-md-8">
            </div>
            <div class="col-md-4" align="right">
                <a class="btn btn-primary"
                    href="<?php echo base_url('catalogos/clientesController/crear'); ?>">Nuevo
                    Cliente</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tbl_clientes" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>N° cliente</th>
                                <th>Razón Social</th>
                                <th>Nombre</th>
                                <th>RFC</th>
                                <th>Tel.</th>
                                <th>Correo</th>
                                <th>-</th>
                                <th>-</th>
                                <th>-</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>N° cliente</th>
                                <th>Razón Social</th>
                                <th>Nombre</th>
                                <th>RFC</th>
                                <th>Tel.</th>
                                <th>Correo</th>
                                <th>-</th>
                                <th>-</th>
                                <th>-</th>
                                <th>-</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            try {
                var tabla_clientes = $('#tbl_clientes').DataTable({
                    ajax: base_url + "catalogos/ClientesController/ajax_catalogo_clientes",
                    columns: [{
                            'data': function(data) {
                                return data.numero_cliente
                            }
                        },
                        {
                            'data': function(data) {
                                return data.nombre_empresa
                            }
                        },
                        {
                            'data': function(data) {
                                return data.nombre
                            }
                        },
                        {
                            'data': function(data) {
                                return data.rfc
                            }
                        },
                        {
                            'data': function(data) {
                                return data.telefono
                            }
                        },
                        {
                            'data': function(data) {
                                return data.correo_electronico
                            }
                        },
                        {
                            'data': function(data) {
                                return "<a href='" + site_url +
                                    'catalogos/clientesController/editar/' + data.id +
                                    "' class='btn btn-success'><i class='fas fa-pen'></i></a>";
                            }
                        },
                        {
                            'data': function(data) {
                                return "<a href='" + site_url +
                                    'catalogos/clientesController/listado_contacto/' + data.id +
                                    "' class='btn btn-warning'><i class='far fa-address-book'></i></a>";
                            }
                        },
                        {
                            'data': function(data) {
                                return "<a href='" + site_url +
                                    'catalogos/clientesController/listado_vehiculo/' + data.id +
                                    "' class='btn btn-info'><i class='fas fa-car'></i></a>";
                            }
                        },
                        {
                            'data': function(data) {
                                return "<button onclick='borrar(" + data.id +
                                    ")' class='btn btn-danger' type='button' data-id='" + data.id +
                                    "'><i class='far fa-trash-alt'></i></button>";
                            }
                        }
                    ]
                });
            } catch (err) {
                alert("ok");
            }
        });

        // $(".btn-borrar").on("click", ".btn-borrar", function() {
        //     var id = $(this).data('id')
        //     console.log(id)
        //     // borrar(id)
        // });

        function borrar(id) {
            utils.displayWarningDialog("¿Desea borrar el registro?", "warning", function(data) {
                if (data.value) {
                    ajax.delete(`api/clientes/${id}`, null, function(response, headers) {
                        if (headers.status != 204) {
                            return utils.displayWarningDialog(headers.message)
                        }
                        location.reload(true)
                    })

                }
            }, true)
        }

    </script>
@endsection
