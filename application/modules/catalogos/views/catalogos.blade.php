@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tabla-facturas">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Catálogos</th>
                    <th scope="col">- </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                    <tr>
                        <td scope="row">{{ $item['id'] }}</td>
                        <td>{{ $item['nombre']}}</td>
                        <td><a href="{{ $item['url'] }}" class="btn btn-primary" type="button">ir</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$("#tabla-facturas").DataTable({});
</script>
@endsection