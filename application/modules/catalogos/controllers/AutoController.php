<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AutoController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300); 
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $dataFromApi = $this->curl->curlGet('api/catalogo-autos');
        
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Auto";
        $data['subtitulo'] = "Listado";

        $this->blade->render('autos/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $marca_autos = $this->curl->curlGet('api/marca-autos');
        $data['cat_unidades'] = procesarResponseApiJsonToArray($marca_autos);

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Auto";
        $data['subtitulo'] = "Registrar Auto";

        $this->blade->render('autos/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            
            $marca_autos = $this->curl->curlGet('api/marca-autos');
            $data['cat_unidades'] = procesarResponseApiJsonToArray($marca_autos);

            $dataFromApi = $this->curl->curlGet('api/catalogo-autos/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Auto";
            $data['subtitulo'] = "Editar Auto";
            
            $this->blade->render('autos/formulario', $data);
        }
    }

    public function ajax_catalogo()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/catalogo-autos');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }
}

/* End of file AnioController.php */
