<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VendedoresController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
        $this->load->helper('general');
        $this->load->library('curl');
        $vendedores = $this->curl->curlGet('api/vendedor');
        
        $data['data'] = procesarResponseApiJsonToArray($vendedores);
        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Vendedores";
        $data['subtitulo'] = "Listado";

        //$this->blade->render('vendedores/listado', $data);
        $this->blade->render('vendedores/listadoV2', $data);
    }

    public function crear()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        //Creamos numero del cliente
        $str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $clave = "V".date("is");
        for($j=0; $j<=2; $j++ ){
           $clave .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $data['num_vendedor'] = $clave;

        $this->load->library('curl');
        $this->load->helper('general');
        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Vendedores";
        $data['subtitulo']  = "Registrar Vendedor";
        $this->blade->render('vendedores/alta', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            
            $dataFromApi = $this->curl->curlGet('api/vendedor/' . $id);
            $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataRegistro)  ? $dataRegistro : [];

            if (!isset($data['data']->numero_cliente)) {
                //Creamos numero del contacto
                $str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $clave = "VX".date("is");
                for($j=0; $j<=1; $j++ ){
                   $clave .= substr($str, rand(0,strlen($str)-1) ,1 );
                }
                $data['num_vendedor'] = $clave;
            }
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Vendedores";
            $data['subtitulo']  = "Editar Vendedor";

            $this->blade->render('vendedores/alta', $data);
        }
    }
}

/* End of file VendedoresController.php */
