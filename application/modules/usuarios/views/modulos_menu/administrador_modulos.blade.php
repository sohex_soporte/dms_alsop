@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <button type="button" id="btn-modal-rol" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalModulos">
                Agregar Modulo
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla-modulos" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre Modulo</th>
                        <th>Icono</th>
                        <th>Orden</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($modulos))
                    @foreach ($modulos as $key => $modulo)
                    <tr>
                        <td>{{ $modulo->id }} </td>
                        <td>{{ $modulo->nombre }} </td>
                        <td>{{ $modulo->icono }} </td>
                        <td>{{ $modulo->orden }} </td>
                        <td>
                            <button data-id="{{ $modulo->id }}" class="btn btn-primary btn-editar">
                                <i class="fa fa-list"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>
                            No se encontraron resultados
                        </td>
                    </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Nombre Modulo</th>
                        <th>Icono</th>
                        <th>Orden</th>
                        <th>-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalModulos" tabindex="-1" role="dialog" aria-labelledby="modalEtiquetas" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalEtiquetas">Modulo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "nombre", "Nombre de modulo", ''); ?>
                        <input type="hidden" name="modulo_id" id="modulo_id" value="">
                    </div>
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "icono", "Icono", ''); ?>
                    </div>
                    <div class="col-md-12">
                        <?php echo renderInputText("number", "orden", "Orden", ''); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-agregar" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#tabla-modulos").on("click", ".btn-editar", function() {
        var id = $(this).data('id');
        $("#modalModulos").modal('hide');
        $("#modulo_id").val(id);
        ajax.get(`api/menu/modulos/${id}`, null, function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            $("#nombre").val(response.nombre);
            $("#icono").val(response.icono);
            $("#orden").val(response.orden);
            $("#modalModulos").modal('show');
        })
    });

    $("#btn-agregar").on('click', function() {

        if ($("#modulo_id").val() == '') {
            ajax.post('api/menu/modulos', {
                'nombre': $("#nombre").val(),
                'icono': $("#icono").val(),
                'orden': $("#orden").val()
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                $("#rol").val('');
                $("#icono").val('');
                $("#orden").val('');
                $("#modalModulos").modal('hide');
                utils.displayWarningDialog("Modulo creado", "success", function(data) {
                    return window.location.href = base_url + 'usuarios/modulos';
                })
            });
        } else {
            let modulo_id = $("#modulo_id").val();
            ajax.put(`api/menu/modulos/${modulo_id}`, {
                'nombre': $("#nombre").val(),
                'icono': $("#icono").val(),
                'orden': $("#orden").val()
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                $("#rol").val('');
                $("#icono").val('');
                $("#orden").val('');
                $("#modalModulos").modal('hide');

                return window.location.href = base_url +  'usuarios/modulos';
            })
        }
    });
</script>
@endsection