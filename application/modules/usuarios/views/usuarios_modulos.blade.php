@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Usuario:</label>
                <select class="form-control" id="usuario_id" name="usuario_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($usuarios))
                    @foreach ($usuarios as $usuario)
                    <option value="{{ $usuario->id}}"> {{$usuario->usuario}} - {{ $usuario->nombre. " ".$usuario->apellido_paterno }}</option>
                    @endforeach
                    @endif
                </select>
                <div id="usuario_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-sm-2">
                    <label for="">Modulos</label>
                </div>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="sin_modulos" data-item_id="0">
                        <label class="form-check-label" for="">
                            SIN MODULOS
                        </label>
                    </div>
                    <br>
                    @foreach ($modulos as $modulo)
                    
                    @if ($modulo->default != 1)
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="{{ "modulo_" .$modulo->id }}" data-item_id="{{ $modulo->id }}">
                        <label class="form-check-label" for=" {{ $modulo->nombre }}">
                            {{ $modulo->nombre }}
                        </label>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <button id="btn-actualizar-modulos" class="btn btn-primary" type="button">
                Guardar Modulos
            </button>
        </div>
    </div>
    <hr>
    <h1>Secciones</h1>
    <div class="row">
        <div class="col-md-6">
            <?php renderSelectArray('modulo_id', 'Modulo', $modulos, 'id', 'nombre',  null) ?>
        </div>
        <div class="col-md-6">
            <?php echo renderInputText("text", "nombre_seccion", "Nombre seccion", ''); ?>
        </div>
        <div class="col-md-12">
            <button id="btn-guardar-seccion" class="btn btn-primary" type="button">
                Guardar Seccion
            </button>
        </div>
    </div>
    <hr>
    <h1>Sub menus</h1>
    <div class="row">
        <div class="col-md-4">
            <?php renderSelectArray('seccion_modulo_id', 'Modulo', $modulos, 'id', 'nombre',  null) ?>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Secciones</label>
                <select class="form-control dinamic-seccion" name="seccion_id" id="seccion_id">
                </select>
                <div id="seccion_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "nombre_submenu", "Nombre submenu", ''); ?>
        </div>
        <div class="col-md-12">
            <button id="btn-guardar-submenu" class="btn btn-primary" type="button">
                Guardar submenu
            </button>
        </div>
    </div>
    <hr>
    <h1>Pantallas</h1>
    <div class="row">
        <div class="col-md-4">
            <?php renderSelectArray('seccion_modulo_vista_id', 'Modulo', $modulos, 'id', 'nombre',  null) ?>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Secciones</label>
                <select class="form-control dinamic-seccion-vista" name="seccion_vista_id" id="seccion_vista_id">
                </select>
                <div id="seccion_vista_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Submenus</label>
                <select class="form-control dinamic-submenu-vista" name="submenu_vista_id" id="submenu_vista_id">
                </select>
                <div id="submenu_vista_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "nombre_vista", "Nombre Vista", ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "link", "Link", ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "controlador", "Controlador", ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "modulo", "modulo", ''); ?>
        </div>
        <div class="col-md-12">
            <button id="btn-guardar-vista" class="btn btn-primary" type="button">
                Guardar vista
            </button>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    //inicio vistas

    $("#seccion_modulo_vista_id").on('change', function() {
        let modulo_id = $("#seccion_modulo_vista_id").val();
        ajax.get('api/menu-secciones?modulo_id=' + modulo_id, {}, function(response, headers) {
            $('#seccion_vista_id').html('');
            $('#seccion_vista_id').append('<option value="">Seleccionar ...</option>');
            response.map(item => {
                $('#seccion_vista_id').append('<option value="' + item.id + '">' + item.nombre + '</option>');
            })
        });
    });

    $("#seccion_vista_id").on('change', function() {
        let seccion_id = $("#seccion_vista_id").val();
        ajax.get('api/menu-submenu?seccion_id=' + seccion_id, {}, function(response, headers) {
            $('#submenu_vista_id').html('');
            $('#submenu_vista_id').append('<option value=""> Seleccionar ...</option>');
            response.map(item => {
                $('#submenu_vista_id').append('<option value="' + item.id + '">' + item.nombre_submenu + '</option>');
            })
        });
    });

    $("#btn-guardar-vista").on('click', function() {

        if ($("#seccion_modulo_vista_id").val() == '') {
            toastr.error("Seleccionar modulo de menu...");
            return false;
        }

        if (!$(".dinamic-seccion-vista option:selected").val()) {
            toastr.error("Seleccionar seccion de menu...");
            return false;
        }

        if ($(".dinamic-submenu-vista option:selected").val() == '') {
            toastr.error("Seleccionar submenu ...");
            return false;
        }


        if ($("#controlador").val() == '') {
            toastr.error("Indicar el controlador ...");
            return false;
        }

        if ($("#modulo").val() == '') {
            toastr.error("Indicar el modulo ...");
            return false;
        }

        if ($("#link").val() == '') {
            toastr.error("Indicar link de la vista ...");
            return false;
        }

        if ($("#nombre_vista").val() == '') {
            toastr.error("Indicar nombre de la vista ...");
            return false;
        }

        ajax.post('api/menu-vistas', {
            "controlador": $("#controlador").val(),
            "nombre": $("#nombre_vista").val(),
            "link": $("#link").val(),
            "modulo": $("#modulo").val(),
            "submenu_id": $(".dinamic-submenu-vista option:selected").val()
        }, function(response, headers) {
            if (headers.status == 201) {
                toastr.success("Pantalla creada....");
            }
        })
    });

    //fin vistas


    $("#seccion_modulo_id").on('change', function() {
        let modulo_id = $("#seccion_modulo_id").val();
        ajax.get('api/menu-secciones?modulo_id=' + modulo_id, {}, function(response, headers) {
            $('#seccion_id').html('');
            $('#seccion_id').append('<option value=""> Seleccionar ...</option>');
            response.map(item => {
                $('#seccion_id').append('<option value="' + item.id + '">' + item.nombre + '</option>');
            })
        });
    });

    $("#btn-guardar-submenu").on('click', function() {
        if (!$(".dinamic-seccion option:selected").val()) {
            toastr.error("Seleccionar seccion de menu...");
            return false;
        }

        if ($("#nombre_submenu").val() == '') {
            toastr.error("Seleccionar nombre del submenu...");
            return false;
        }

        ajax.post('api/menu-submenu', {
            "nombre_submenu": $("#nombre_submenu").val(),
            "seccion_id": $(".dinamic-seccion option:selected").val()
        }, function(response, headers) {
            if (headers.status == 201) {
                toastr.success("Seccion creada....");
            }
        })
    });

    $("#btn-guardar-seccion").on('click', function() {
        if (!$("#modulo_id").val()) {
            toastr.success("Seleccionar el modulo ....");
            return false;
        }

        ajax.post('api/menu-secciones', {
            "nombre": $("#nombre_seccion").val(),
            "modulo_id": $("#modulo_id").val()
        }, function(response, headers) {
            if (headers.status == 201) {
                toastr.success("Seccion creada....");
            }
        })
    });

    function disableChecks() {
        $('input[type=checkbox]').each(function() {
            $(this).prop('checked', false);
            let modulo_id = $(this).data('item_id');
            if (modulo_id == '0') {
                document.getElementById("sin_modulos").checked = true;
            } else {
                document.getElementById("modulo_" + modulo_id).disabled = true;
            }
        });
    }

    $("#sin_modulos").on('change', function() {
        if ($('#sin_modulos').prop('checked')) {
            disableChecks();
        } else {
            $('input[type=checkbox]').each(function() {
                let modulo_id = $(this).data('item_id');
                if (modulo_id != '0') {
                    document.getElementById("modulo_" + modulo_id).disabled = false;
                }
            });
        }
    });


    $("#usuario_id").on('change', function() {
        $('input[type=checkbox]').each(function() {
            $(this).prop('checked', false);
        });

        // return false;
        let usuario_id = $("#usuario_id").val();
        ajax.get('api/usuarios/modulos-by-usuario?usuario_id=' + usuario_id, {}, function(response, headers) {
            // console.log(response);
            if (response.length > 0) {
                response.map(item => {
                    if (item.modulo_id) {
                        document.getElementById("modulo_" + item.modulo_id).checked = true;
                    }
                })
            } else {
                disableChecks();
                toastr.warning("Usuario sin modulos ....");
            }
        })
    });

    $("#btn-actualizar-modulos").on('click', function() {
        var array = [];
        $('input[type=checkbox]').each(function() {
            if ($(this).prop('checked')) {
                let modulo_id = $(this).data('item_id');
                array.push(modulo_id);
            }
        });

        if (array.length == 0) {
            toastr.error("Seleccionar modulos ...");
            return false;
        }

        if ($("#usuario_id").val() == '') {
            toastr.error("Seleccionar usaurio ...");
            return false;
        }

        ajax.post('api/usuarios/modulos', {
            "modulo_id": array,
            "usuario_id": $("#usuario_id").val()
        }, function(response, headers) {
            console.log(response, headers);
            if (headers.status == 201 || headers.status == 204) {
                toastr.success("Modulos actualizados....");
            }
        })
    });
</script>
@endsection