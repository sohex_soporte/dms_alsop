<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
                    <th>Fecha</th>
					<th width="30%">Estatus</th>
					<th>Comentario</th>
					<th>Tipo de estatus</th>
                    <th>Usuario</th>
				</tr>
			</thead>
			<tbody>
				@if(count($comentarios)>0)
				@foreach($comentarios as $c => $value)
					<tr>
						<td>{{$value->created_at}}</td>
                        <td>{{($value->tipo_comentario==1)?$value->estatus:$value->estatus_piso}}</td>
						<td>{{$value->comentario}}</td>
						<td>{{($value->tipo_comentario==1)?'General':'Medición de tiempo'}}</td>
                        <td>{{$value->nombre.' '.$value->apellido_paterno.' ',$value->apellido_materno}}</td>
					</tr>
				@endforeach
				@else
				<tr class="text-center">
					<td colspan="5">Aún no se han registrado comentarios... </td>
				</tr>
				@endif
			</tbody>

		</table>
	</div>
</div>