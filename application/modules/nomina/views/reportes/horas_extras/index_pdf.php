<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>

<br/>
<div style="">
    <table style="" class="" cellpadding="5">
        <tr>
            <th width="150px" bgcolor="">
                Departamentos:
            </th>
            <td>
            <?php echo (isset($departamentos))? $departamentos : '' ?>
            </td>
        </tr>

        <tr>
            <th width="150px" bgcolor="">
                Puesto:
            </th>
            <td>
                <?php echo (isset($puesto))? $puesto : '' ?>
            </td>
        </tr>

        <tr>
            <th width="150px" bgcolor="">
                Jornada:
            </th>
            <td>
            <?php echo (isset($jornada))? $jornada : '' ?>
            </td>
        </tr>

        <tr>
            <th width="150px" bgcolor="">
                Fechas:
            </th>
            <td>
                <?php echo (isset($fecha_inicio))? utils::aFecha($fecha_inicio,true).' '.utils::aFecha($fecha_fin,true) : '' ?>
            </td>
        </tr>
    </table>
</div>
<br/>