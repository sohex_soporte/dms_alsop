<div id="sub_menu" class="mt-4">
	<div class="icon-bar">
		<a href="<?php echo site_url('nomina/catalogosconsultas/departamentos'); ?>">
			<i class="fa fa-book"></i><br/>
			<h6 class="text-dark">Departamentos</h6>
		</a>
		<a href="<?php echo site_url('nomina/catalogosconsultas/puestos'); ?>">
			<i class="fa fa-users"></i><br/>
			<h6 class="text-dark">Puestos</h6>
		</a>
		<a href="<?php echo site_url('nomina/catalogosconsultas/clasificaciones'); ?>">
			<i class="fa fa-building"></i><br/>
			<h6 class="text-dark">Clasificaciones</h6>
		</a>
	</div>
</div>