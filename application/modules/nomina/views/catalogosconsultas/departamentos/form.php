<script>
	const identity = "{id}";
</script>

<script id="template" type="x-tmpl-mustache">
{{#formContent}}
<div class="form-group row">
	<label for="{{name}}" class="col-sm-3 col-form-label">{{nombreCampo}}:</label>
	<div class="col-sm-9">
		<input type="{{type}}" class="{{class}}" id="{{name}}" name="{{name}}" value="{{value}}" {{options}} >
		<small id="msg_{{name}}" class="form-text text-danger"></small>
	</div>
</div>
{{/formContent}}
</script>

<form id="formContent">
	<div id="contenedor"></div>
	<div class="form-group row">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<button onclick="Apps.guardar();" type="button" class="btn btn-success col-md-4">Guardar</button>
		</div>
	</div>
</form>
