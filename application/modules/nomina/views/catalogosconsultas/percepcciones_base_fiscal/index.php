<script>
    var  $id = "{id}";
</script>

<div class="row">
    <div class="col-sm-12">
        <h3 class="mt-4 mb-5">Percepciones por base fiscal</h3>
    </div>
</div>

<div class="card">
	<div class="card-body">

		<form id="general_form">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Base fiscal</label>
					<div class="col-sm-10">
						<select class="custom-select" onchange="Apps.cargarDatos();" id="trabajador" name="trabajador" >
							{basesFiscales}
							<option value="{id}" >{Num} - {Descripcion}</option>
							{/basesFiscales}
						</select>

					</div>
				</div>
			</div>
			</div>

		</form>

	</div>
</div>


<div class="card mt-4">
	<div class="card-body ">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
			</div>
		</div>

	</div>
</div>
