<script>
	var id_trabajador = "{id_trabajador}";
	var id = "{id}";
	var id_periodo = "{id_periodo}";

</script>

<div class="row mt-5 mb-3">
	<div class="col-sm-12">
		<h3>Edicion de horas extras</h3>
	</div>
</div>

<div class="card mt-4">
	<div class="card-body ">

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group row">
					<label for="dia" class="col-sm-3 col-form-label">Clave</label>
					<div class="col-sm-9">
						<input type="" class="form-control-plaintext" value="<?php echo $trabajador['Clave']; ?>">
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group row">
					<label for="dia" class="col-sm-3 col-form-label">Nombre</label>
					<div class="col-sm-9">
						<input type="" class="form-control-plaintext"
							value="<?php echo $trabajador['Nombre'].' '.$trabajador['Apellido_1'].' '.$trabajador['Apellido_2']; ?>">
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group row">
					<label for="dia" class="col-sm-3 col-form-label">Departamento</label>
					<div class="col-sm-9">
						<input type="" class="form-control-plaintext"
							value="<?php echo $trabajador['Descripcion_Departamento']; ?>">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card mt-4">
	<div class="card-body ">

		<form id="general_form">

			<!-- <div class="col-sm-12">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Periodo</label>
					<div class="col-sm-5">
						<input type="" readonly class="form-control-plaintext"
							value="<?php echo utils::aFecha($periodo['FechaInicio']).' al '.utils::aFecha($periodo['FechaFin']); ?>">
					</div>
				</div>
			</div> -->

			<div class="col-sm-12">
				<div class="form-group row">
					<label for="Fecha" class="col-sm-2 col-form-label">Fecha</label>
					<div class="col-sm-5">
						<input type="" readonly class="form-control-plaintext"
							value="<?php echo utils::aFecha($registro['Fecha'],true); ?>">
						<small id="msg_Fecha" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row">
					<label for="Horas" class="col-sm-2 col-form-label">Horas</label>
					<div class="col-sm-5">
						<input type="number" class="form-control" id="Horas" name="Horas" step="0.01" min="0" max="12"
							value="<?php echo $registro['Horas']; ?>">
						<small id="msg_Horas" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row mt-3">
					<div class="col-sm-3">&nbsp;</div>
					<div class="col-sm-9">
						<button onclick="window.location.href = '<?php echo site_url('nomina/inicio/horas_extras'); ?>';" type="button"
							class="btn btn-danger col-md-4">Regresar</button>
						<button onclick="Apps.guardar(this);" type="button"
							class="btn btn-success col-md-4">Guardar</button>
					</div>
				</div>
			</div>

		</form>

	</div>
</div>
