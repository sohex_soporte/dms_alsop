<script>
	var id_tipo_perdiodo = "{id_tipo_perdiodo}";

</script>

<div class="row mt-4 mb-3">
	<div class="col-sm-12">
		<h3>Consulta de recibos electrónicos</h3>
	</div>
</div>


<div class="card">
	<div class="card-body">
		<form class="">
			<div class="row">
				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_tipo_periodo">Tipo de periodo:</label>
						<select onchange="Apps.buscar_periodos();" class="custom-select" id="id_tipo_periodo">
							<?php foreach ($tipo_periodos as $key => $value) { ?>
							<option <?php echo($id_tipo_perdiodo == $value['id'])? 'selected' : ''; ?>
								value="<?php echo $value['id']; ?>"><?php echo $value['Descripcion']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_periodo">Periodo:</label>
						<select class="custom-select" id="id_periodo">
							<?php foreach ($perdiodo as $key => $value) { ?>
							<option <?php echo($id_perdiodo == $value['id'])? 'selected' : ''; ?>
								value="<?php echo $value['id']; ?>"><?php echo utils::aFecha($value['FechaInicio'],true); ?> al
								<?php echo utils::aFecha($value['FechaFin'],true); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>


				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_periodo">&nbsp;</label>
						<div class="col-sm-3">
						<button type="button" class="btn btn-primary mb-2" onclick="var opcion=$('select#periodo_trabajador option:selected').val(); Apps.cargarDatos(opcion);">Buscar</button>
					</div>
					</div>
				</div>

			</div>
		</form>
	</div>
</div>

<div class="card mt-4">
	<div class="card-body ">
		<!-- <div class="row mb-4">
			<div class="col-md-11">&nbsp;</div>
			<div class="col-md-1">
				<button onclick="Apps.addContenido();" type="button" class="btn btn-success">Agregar</button>
			</div>
		</div> -->
		<div class="row">
			<div class="table-responsive">
				<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
			</div>
		</div>

	</div>
</div>


<div class="mx-auto col-sm-4 mt-4">
	<div class="card">
		<div class="card-body">
			<h4 class="card-subtitle mb-2 text-muted">Ayuda</h4>
			<ul class="list-unstyled mt-3">
				<li class="media">
					<i class="mr-3 far fa-list-alt" style="font-size: 1.2em;"></i>
					<div class="media-body">
						<h5 class="mt-0 mb-1 text-dark" style="font-size: 1.1em;">Ver recibo</h5>
					</div>
				</li>
				<li class="media">
					<i class="mr-3 fas fa-file-pdf text-danger" style="font-size: 1.2em;"></i>
					<div class="media-body">
						<h5 class="mt-0 mb-1 text-dark" style="font-size: 1.1em;">Descargar recibo</h5>
					</div>
				</li>
				<li class="media">
					<i class="mr-3 far fa-file-code" style="font-size: 1.2em;"></i>
					<div class="media-body">
						<h5 class="mt-0 mb-1 text-dark" style="font-size: 1.1em;">Descargar XML</h5>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
