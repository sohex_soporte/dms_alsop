<?php if(isset($periodo)){ ?>
<script>
var periodo = "<?php echo $periodo['id']; ?>";
</script>

<div class="row mt-3 mb-3">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12">
						<h4>Periodo: <?php echo utils::aFecha($periodo['FechaInicio'],true); ?> al <?php echo utils::aFecha($periodo['FechaFin'],true); ?></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<div class="row mt-4">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
		</div>
	</div>
</div>
