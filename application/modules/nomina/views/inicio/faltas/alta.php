<script>
	var id_trabajador = "{id_trabajador}";

</script>

<div class="row mt-4">
	<div class="col-sm-12">
		<h3>Captura de faltas</h3>
	</div>
</div>

<div class="card mt-4">
	<div class="card-body ">

		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					<label for="">Clave:</label>
					<input type="" class="form-control-plaintext" readonly value="<?php echo $trabajador['Clave']; ?>">
				</div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
					<label for="" class="">Nombre:</label>
					<input type="" class="form-control-plaintext" readonly
						value="<?php echo $trabajador['Nombre'].' '.$trabajador['Apellido_1'].' '.$trabajador['Apellido_2']; ?>">
				</div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
					<label for="">Departamento:</label>
					<input type="" class="form-control-plaintext" readonly
						value="<?php echo $trabajador['Descripcion_Departamento']; ?>">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card mt-4">
	<div class="card-body ">

		<form id="general_form">

			<div class="row">

				<div class="col-sm-6">
					{grupo1}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						<div class="">
							{input}
							<small id="msg_{key}" class="form-text text-danger"></small>
						</div>
					</div>
					{/grupo1}
				</div>

				<!-- <div class="col-sm-6">
					{grupo2}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						<div class="">
							{input}
							<small id="msg_{key}" class="form-text text-danger"></small>
						</div>
					</div>
					{/grupo2}
				</div> -->

				<div class="col-sm-6">
					{grupo3}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						<div class="">
							{input}
							<small id="msg_{key}" class="form-text text-danger"></small>
						</div>
					</div>
					{/grupo3}
				</div>
			</div>
			<div class="row mt-3">
				<div class="col-sm-6">
					{grupo4}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						<div class="">
							{input}
							<small id="msg_{key}" class="form-text text-danger"></small>
						</div>
					</div>
					{/grupo4}
				</div>

				<div class="col-sm-6">
					{grupo5}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						{input}
						<small id="msg_{key}" class="form-text text-danger"></small>
					</div>
					{/grupo5}
				</div>

				<div class="col-sm-6">
					{grupo6}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						{input}
						<small id="msg_{key}" class="form-text text-danger"></small>
					</div>
					{/grupo6}
				</div>

				<div class="col-sm-6">
					{grupo7}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						{input}
						<small id="msg_{key}" class="form-text text-danger"></small>
					</div>
					{/grupo7}
				</div>
			</div>
			<div class="row mt-3">

				<div class="col-sm-12">
					{grupo8}
					<div class="form-group">
						<label for="{key}" class="">{label}</label>
						{input}
						<small id="msg_{key}" class="form-text text-danger"></small>
					</div>
					{/grupo8}
				</div>

			</div>

			<div class="row mt-3">
				<div class="col-sm-6">
					{grupo9}
					<div class="form-group">
						<label for="{key}" class="col-form-label">{label}</label>
						{input}
						<small id="msg_{key}" class="form-text text-danger"></small>
					</div>
					{/grupo9}
				</div>
			</div>



		</form>

	</div>
	
	<div class="col-sm-12">
		<div class="form-group row mt-3">
			<div class="mx-auto col-sm-5">
				<button onclick="window.location.href = '<?php echo site_url('nomina/inicio/faltas'); ?>';" type="button"
					class="btn btn-danger col-md-4">Regresar</button>
				<button onclick="Apps.guardar(this);" type="button"
					class="btn btn-success col-md-4">Guardar</button>
			</div>
		</div>
	</div>
	
</div>
