<input type="hidden" class="form-control" name="id" value="{{id}}">

<div class="row">
	<div class="col-sm-8">
		<div class="card">
			<div class="card-body">
				<h6 class="text-dark">Salario actual</h6>
				<hr class="style-six" />

                <div class="form-group row">
                    <label for="JornadaCompleta" class="col-sm-4 col-form-label">Jornada completa:</label>
					<div class="col-sm-8">
                        <select class="form-control" id="JornadaCompleta" name="JornadaCompleta" attr-id="{{JornadaCompleta}}"> 
                            <option value="si" >si</option>
                            <option value="no" >no</option>
                        </select>
                        <small id="msg_JornadaCompleta" class="form-text text-danger"></small>
                    </div>
				</div>

				<div class="form-group row"><label for="SalarioDia" class="col-sm-4 col-form-label">Salario por día:</label>
					<div class="col-sm-8">
						<input type="number" class="form-control" id="SalarioDia" name="SalarioDia" value="{{SalarioDia}}">
						<small id="msg_SalarioDia" class="form-text text-danger"></small>
					</div>
				</div>
				<div class="form-group row"><label for="SalarioHora" class="col-sm-4 col-form-label">Salario por
						hora:</label>
					<div class="col-sm-8"><input type="number" class="form-control" id="SalarioHora" name="SalarioHora"
							value="{{SalarioHora}}"><small id="msg_SalarioHora" class="form-text text-danger"></small>
					</div>
				</div>
				<div class="form-group row"><label for="HorasDia" class="col-sm-4 col-form-label">Horas por día:</label>
					<div class="col-sm-8"><input type="number" class="form-control" id="HorasDia" name="HorasDia"
							value="{{HorasDia}}"><small id="msg_HorasDia" class="form-text text-danger"></small></div>
				</div>
				<div class="form-group row"><label for="DiasSemana" class="col-sm-4 col-form-label">Días por
						semana:</label>
					<div class="col-sm-8"><input type="number" class="form-control" id="DiasSemana" name="DiasSemana"
							value="{{DiasSemana}}"><small id="msg_DiasSemana" class="form-text text-danger"></small>
					</div>
				</div>
				<div class="form-group row"><label for="DiasPeriodo" class="col-sm-4 col-form-label">Días por
						periodo:</label>
					<div class="col-sm-8"><input type="number" class="form-control" id="DiasPeriodo" name="DiasPeriodo"
							value="{{DiasPeriodo}}"><small id="msg_DiasPeriodo" class="form-text text-danger"></small>
					</div>
				</div>
				<div class="form-group row"><label for="FechaAplicacion" class="col-sm-4 col-form-label">Fecha de
						aplicación:</label>
					<div class="col-sm-8"><input type="date" class="form-control" id="FechaAplicacion"
							name="FechaAplicacion" value="{{FechaAplicacion}}"><small id="msg_FechaAplicacion"
							class="form-text text-danger"></small></div>
				</div>

				<div class="form-group row">
                    <label for="FormaPago" class="col-sm-4 col-form-label">Forma de pago:</label>
					<div class="col-sm-8">
                        <select class="form-control" id="FormaPago" name="FormaPago" attr-id="{{id_FormaPago}}" > 
                            {{#FormaPago}}<option value="{{id}}" > {{Descripcion}}</option> {{/FormaPago}}
                        </select>
                        <small id="msg_FormaPago" class="form-text text-danger"></small>
                    </div>
				</div>
			</div>
		</div>
	</div>

    <div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<h6 class="text-dark">Deposito de la nomina</h6>
				<hr class="style-six" />
				<div class="form-group row">
                    <label for="id_BancoOperador" class="col-form-label">Banco:</label>
                    <select class="form-control" id="id_BancoOperador" name="id_BancoOperador" attr-id="{{id_BancoOperador}}"> 
                        {{#BancoOperador}}<option value="{{id}}" >{{Clave}} - {{Descripcion}}</option> {{/BancoOperador}}
                    </select>
                    <small id="msg_id_BancoOperador" class="form-text text-danger"></small>
				</div>
                <div class="form-group row">
                    <label for="LocalidadSucursal" class="col-form-label">Localidad/Sucursal:</label>
					<input type="text" class="form-control" id="LocalidadSucursal" name="LocalidadSucursal" value="{{LocalidadSucursal}}">
                    <small id="msg_LocalidadSucursal" class="form-text text-danger"></small>
				</div>
                <div class="form-group row">
                    <label for="CuentaDeposito" class="col-form-label">Cuenta deposito:</label>
					<input type="text" class="form-control" id="CuentaDeposito" name="CuentaDeposito" value="{{CuentaDeposito}}">
                    <small id="msg_CuentaDeposito" class="form-text text-danger"></small>
				</div>
                <div class="form-group row">
                    <label for="CuentaBanco" class="col-form-label">Control de banco:</label>
					<input type="text" class="form-control" id="CuentaBanco" name="CuentaBanco" value="{{CuentaBanco}}">
                    <small id="msg_CuentaBanco" class="form-text text-danger"></small>
				</div>

			</div>
		</div>
	</div>

</div>

<div class="row mt-3">
    <div class="col-sm-8">
		<div class="card">
			<div class="card-body">
				<h6 class="text-dark">IMSS</h6>
				<hr class="style-six" />
				<div class="form-group row">
                    <label for="id_BaseCotizacion" class="col-form-label col-sm-4">Base cotización:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_BaseCotizacion" name="id_BaseCotizacion" attr-id="{{id_BaseCotizacion}}"> 
							{{#BaseCotizacion}}<option value="{{id}}" >{{Descripcion}}</option> {{/BaseCotizacion}}
						</select>
						<small id="msg_id_BaseCotizacion" class="form-text text-danger"></small>
					</div>
				</div>

                <div class="form-group row">
					<label for="id_TablaSistema" class="col-form-label col-sm-4">Tabla de SDI:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_TablaSistema" name="id_TablaSistema" attr-id="{{id_TablaSistema}}"> 
							{{#TablaSDI}}<option value="{{id}}" >{{id}} - {{Descripcion}}</option> {{/TablaSDI}}
						</select>
                    	<small id="msg_id_TablaSistema" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="Calculado" class="col-sm-4 col-form-label">Calculado:</label>
					<div class="col-sm-8">
						<input type="number" class="form-control" id="Calculado" name="Calculado" 	value="{{Calculado}}">
						<small id="msg_Calculado" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row"><label for="SDICapturado" class="col-sm-4 col-form-label">SDI capturado:</label>
					<div class="col-sm-8"><input type="number" class="form-control" id="SDICapturado" name="SDICapturado" 	value="{{SDICapturado}}">
					<small id="msg_SDICapturado" class="form-text text-danger"></small>
					</div>
				</div>

              

			</div>
		</div>
	</div>

	<div class="col-sm-4">
		<div class="card">
			<div class="card-body">
				<!-- <h4 class="text-dark">Deposito de la nomina</h4>
				<hr class="style-six" /> -->
				<div class="form-group row">
                    <label for="NoFonacot" class="col-form-label">NO. FONACOT:</label>
                    <input type="text" class="form-control" id="NoFonacot" name="NoFonacot" value="{{NoFonacot}}">
                    <small id="msg_NoFonacot" class="form-text text-danger"></small>
				</div>
                <!-- <div class="form-group row">
                    <label for="id_Vacacion" class="col-form-label">Tabla vacaciones:</label>
					<input type="text" class="form-control" id="TablaVacaciones" name="TablaVacaciones" value="{{id_Vacacion}}">
                    <small id="msg_TablaVacaciones" class="form-text text-danger"></small>
				</div> -->
                <div class="form-group row">
                	<div class="row">
						<label for="" class="col-form-label col-sm-12">Días descanso:</label>
						<div class="col-sm-12">
							<small id="msg_DiasDescanso" class="form-text text-danger"></small>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="2" id="diaLunes" name="diasDescanso[1]" >
								<label class="form-check-label" for="diaLunes">
									Lunes
								</label>
							</div>

							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="3" id="diaMartes" name="diasDescanso[2]" >
								<label class="form-check-label" for="diaMartes">
									Martes
								</label>
							</div>

							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="4" id="diaMiercoles" name="diasDescanso[3]" >
								<label class="form-check-label" for="diaMiercoles">
									Miercoles
								</label>
							</div>

							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="5" id="diaJueves" name="diasDescanso[4]" >
								<label class="form-check-label" for="diaJueves">
									Jueves
								</label>
							</div>

							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="6" id="diaViernes" name="diasDescanso[5]" >
								<label class="form-check-label" for="diaViernes">
									Viernes
								</label>
							</div>
							
							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="7" id="diaSabado" name="diasDescanso[6]" >
								<label class="form-check-label" for="diaSabado">
									Sabado
								</label>
							</div>

							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="1" id="diaDomingo" name="diasDescanso[7]" >
								<label class="form-check-label" for="diaDomingo">
									Domingo
								</label>
							</div>

							
						</div>                    
					</div>
				</div>
                

			</div>
		</div>
	</div>

</div>