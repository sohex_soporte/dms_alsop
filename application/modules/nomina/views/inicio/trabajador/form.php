<script>
	const identity = "{identity}";
	const NOMINA_API_IMAGEN = "<?php echo NOMINA_API; ?>";
</script>

<script id="datosGenerales" type="x-tmpl-mustache">
	<?php include('trabajador_datosGenerales.php'); ?>
</script>

<script id="datosSalario" type="x-tmpl-mustache">
	<?php include('trabajador_datosSalario.php'); ?>
</script>

<script id="datosPersonales" type="x-tmpl-mustache">
	<?php include('trabajador_datosPersonales.php'); ?>
</script>

<script id="datosIMSS" type="x-tmpl-mustache">
	<?php include('trabajador_datosIMSS.php'); ?>
</script>

<script id="datosSalud" type="x-tmpl-mustache">
	<?php include('trabajador_datosSalud.php'); ?>
</script>

<script id="datosFiscales" type="x-tmpl-mustache">
	<?php include('trabajador_datosFiscales.php'); ?>
</script>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group row">
            <label for="Clave" class="col-sm-1 col-form-label">Clave:</label>
            <div class="col-sm-10">
                <input type="number" class="form-control-plaintext" readonly="true" id="Clave" name="Clave" value="">
                <small id="msg_Clave" class="form-text text-danger"></small>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs nav-fill" id="tab_content" role="tablist">
	<li class="nav-item" role="presentation">
		<a class="nav-link active" id="generales-tab" data-toggle="tab" href="#generales" role="tab"
			aria-controls="generales" aria-selected="true">Datos Generales</a>
	</li>

	<li class="nav-item" role="presentation">
		<a class="nav-link" onclick="Apps.showTab(this);" id="salario-tab" data-toggle="tab" href="#salario" role="tab" aria-controls="salario"
			aria-selected="false">Salario</a>
	</li>

	<li class="nav-item" role="presentation">
		<a class="nav-link" onclick="Apps.showTab(this);" id="personales-tab" data-toggle="tab" href="#personales" role="tab" aria-controls="personales"
			aria-selected="false">Datos Personales</a>
	</li>

	<li class="nav-item" role="presentation">
		<a class="nav-link" onclick="Apps.showTab(this);" id="imss-tab" data-toggle="tab" href="#imss" role="tab" aria-controls="imss"
			aria-selected="false">IMSS</a>
	</li>

	<li class="nav-item" role="presentation">
		<a class="nav-link" onclick="Apps.showTab(this);" id="salud-tab" data-toggle="tab" href="#salud" role="tab" aria-controls="salud"
			aria-selected="false">Salud</a>
	</li>

	<li class="nav-item" role="presentation">
		<a class="nav-link" onclick="Apps.showTab(this);" id="fiscales-tab" data-toggle="tab" href="#fiscales" role="tab" aria-controls="fiscales"
			aria-selected="false">Fiscales</a>
	</li>
</ul>
<div class="tab-content" id="myTabContent">
	<div class="tab-pane fade show active" id="generales" role="tabpanel" aria-labelledby="generales-tab">
		<div class="col-sm-12 mb-5 mt-2"></div>
		<form id="formContent_General"></form>
		<div class="col-sm-12 mb-5 mt-2"></div>
	</div>

	<div class="tab-pane fade " id="salario" role="tabpanel" aria-labelledby="salario-tab">
		<div class="col-sm-12 mb-5 mt-2"></div>
		<form id="formContent_Salario"></form>
		<div class="col-sm-12 mb-5 mt-2"></div>
	</div>

	<div class="tab-pane fade" id="personales" role="tabpanel" aria-labelledby="personales-tab">
		<div class="col-sm-12 mb-5 mt-2"></div>
		<form id="formContent_Personales"></form>
		<div class="col-sm-12 mb-5 mt-2"></div>
	</div>

	<div class="tab-pane fade" id="imss" role="tabpanel" aria-labelledby="imss-tab">
		<div class="col-sm-12 mb-5 mt-2"></div>
		<form id="formContent_imss"></form>
		<div class="col-sm-12 mb-5 mt-2"></div>
	</div>

	<div class="tab-pane fade" id="salud" role="tabpanel" aria-labelledby="salud-tab">
		<div class="col-sm-12 mb-5 mt-2"></div>
		<form id="formContent_salud"></form>
		<div class="col-sm-12 mb-5 mt-2"></div>
	</div>
	
	<div class="tab-pane fade" id="fiscales" role="tabpanel" aria-labelledby="fiscales-tab">
		<div class="col-sm-12 mb-5 mt-2"></div>	
		<form id="formContent_fiscales"></form>
		<div class="col-sm-12 mb-5 mt-2"></div>
	</div>
</div>
<!-- <hr class="style-six" /> -->
<div class="form-group row mt-3">
	<div class="col-sm-3">&nbsp;</div>
	<div class="col-sm-9">
		<button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
	</div>
</div>


<!-- Modal -->
<?php echo isset($contrato)?  $contrato : ''; ?>