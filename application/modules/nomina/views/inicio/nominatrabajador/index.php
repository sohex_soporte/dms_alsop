<div class="row mt-2 mb-3">
	<div class="col-sm-12">
		<h3>Nómina del trabajador</h3>
	</div>
</div>

<div class="row mt-4">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="mx-auto col-sm-4 mt-4">
	<div class="card">
		<div class="card-body">
		<h4 class="card-subtitle mb-2 text-muted">Ayuda</h4>
			<ul class="list-unstyled mt-3">
				<li class="media">
					<i class="mr-3 far fa-list-alt" style="font-size: 1.2em;"></i>
					<div class="media-body">
						<h5 class="mt-0 mb-1 text-dark" style="font-size: 1.1em;" >Ver detalle</h5>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>