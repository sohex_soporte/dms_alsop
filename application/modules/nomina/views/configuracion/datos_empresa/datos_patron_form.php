<script>
    var  id_Empresa = "{id_Empresa}";
    var  id_Patron = "{id_Patron}";
    var  salario_minimo = "{salario_minimo}";
</script>

<div class="card mt-4">
	<div class="card-body ">

		<form id="general_form">

            <div class="row">
                
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="" class="">Clave</label>
                        <input type="" class="form-control-plaintext" value="{clave}">
                    </div>   
                </div>

            </div>
            <div class="row mt-3">
            
                <div class="col-sm-6">
                    {grupo_2}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    </div>
                    {/grupo_2}
                </div>

                <div class="col-sm-6">
                    {grupo_3}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    </div>
                    {/grupo_3}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-6">
                    {grupo_4}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    </div>
                    {/grupo_4}
                </div>

                <div class="col-sm-6">
                    {grupo_5}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_5}
                </div>

            </div>
            <div class="row mt-3">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="" class="">Salario minimo (SM)</label>
                        <input type="" class="form-control-plaintext" value="$ {salario_minimo}">
                    </div>   
                </div>

                <div class="col-sm-6">
                    {grupo_7}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_7}
                </div>
            </div>
            <div class="row mt-3">

                <div class="col-sm-6">
                    {grupo_8}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_8}
                </div>

                <div class="col-sm-6">
                    {grupo_9}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_9}
                </div>
            </div>
		</form>

	</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group mt-3">
            <div class="row">
                <div class="col-sm-3">&nbsp;</div>
                <div class="col-sm-9">
                    <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>