<?php defined('BASEPATH') or exit('No direct script access allowed');

class Vacaciones extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/reportes/vacaciones/');
        $this->breadcrumb = array(
            'Reportes',
            'Movimientos de nómina',
            array('name'=>'Vacaciones','url'=>site_url('nomina/reportes/vacaciones'))
        );
    }

    public function index($id_trabajador = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $trabajadores = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',array(),'get');
        $trabajadores_list = (is_array($trabajadores['data']))? $trabajadores['data'] : array();

        $dataContent = array(
            'trabajadores' => $trabajadores_list
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/reportes/vacaciones/index', $dataContent,true);
        
        $this->output($html);
    }

    public function index_busqueda(){
        $data_search = $this->input->post();
        $this->load->model('General_model');
        $trabajadores = $this->General_model->call_api('reportes/trabajador/reporte_vacaciones',$data_search,'post');
        $this->response($trabajadores);
    }


    public function index_pdf(){

        $dataContent = $this->input->post();
      
        $this->load->library('Pdf',array(
            'titulo' => 'Reporte de vacaciones',
            'position' => 'P',
            'size' => 'A4'
        ));

        $filterData = array(
            // 'jornada' => $this->input->post('jornada_desc'),
            'fecha_inicio' => $this->input->post('fecha_inicio_desc'),
            'fecha_fin' => $this->input->post('fecha_fin_desc')
        );

        $this->load->library('parser');
        $view = $this->parser->parse('/reportes/vacaciones/index_pdf',$filterData,true);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);


        $this->load->library('table');
        $template = array(    
            'table_open'  => '<table border="0" cellpadding="4" cellspacing="" class="mytable" style="background-color: #f3f4f5;border-color:#c3c3c3;">',
            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th style="font-weight:bold; text-align:left;background-color: #c3c3c3;" >',
            'heading_cell_end'      => '</th>',
            'row_start'             => '<tr style="border-bottom: 1px solid #ddd;background-color: #ffffff;" >',
            'row_end'               => '</tr>',
            'cell_start'            => '<td style="padding: 5px;" >',
            'cell_end'              => '</td>',
        );        
        $this->table->set_template($template);
        $this->table->set_heading(array(
            'Clave', 
            'Nombre del trabajador',
            'J. Completa',
            'F. Alta',
            'Antigüedad',
            'Días disf',
            'Fecha disf.',
            'Estatus',
            'Días prima vac.',
            'Fecha de pago',
            'Estatus'
        ));

        $this->load->model('General_model');
        $reporte = $this->General_model->call_api('reportes/trabajador/reporte_vacaciones',$dataContent,'post');
        
        foreach ($reporte['data'] as $key => $value) {
            $this->table->add_row(
                $value['Clave'], 
                trim($value['Nombre'].' '.$value['Apellido_1'].' '.$value['Apellido_2']), 
                $value['JornadaCompleta'], 
                utils::aFecha($value['FechaAlta'],true), 
                0,
                $value['DiasDisfrute'],
                $value['FechaDisfrute'],
                $value['Descripcion'],
                $value['DiasPrimaVac'],
                $value['FechaPagoPV'],
                ($value['Procesado'] == 1)? 'Por pagar' : 'Por pagar',
            );
        }

        $table = array(
            'table' => $this->table->generate()
        );
        $view = $this->parser->parse('/reportes/vacaciones/index_pdf_table',$table,true);
        $this->tcpdf->SetFont('', '', 9);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);

        $this->tcpdf->Output('Reporte vacaciones.pdf', 'D');
    }
}