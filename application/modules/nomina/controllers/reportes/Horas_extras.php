<?php defined('BASEPATH') or exit('No direct script access allowed');

class Horas_extras extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/reportes/horas_extras/');
        $this->breadcrumb = array(
            'Reportes',
            'Movimientos de nómina',
            array('name'=>'Horas extras','url'=>site_url('nomina/reportes/horas_extras'))
        );
    }

    public function index($id_trabajador = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $departamentos = $this->General_model->call_api('catalogos/departamentos/store',array(),'get');
        $departamentos_list = (is_array($departamentos['data']))? $departamentos['data'] : array();

        $puestos = $this->General_model->call_api('catalogos/puestos/store',array(),'get');
        $puestos_list = (is_array($puestos['data']))? $puestos['data'] : array();

        $trabajadores = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',array(),'get');
        $trabajadores_list = (is_array($trabajadores['data']))? $trabajadores['data'] : array();

        $dataContent = array(
            'departamentos' => $departamentos_list,
            'puestos' => $puestos_list,
            'trabajadores' => $trabajadores_list
        );
        // utils::pre($dataContent);
        

        $this->load->library('parser');
        $html = $this->parser->parse('/reportes/horas_extras/index', $dataContent,true);
        
        $this->output($html);
    }

    public function index_trabajadores(){
        $data_search = array();
        if($this->input->post('id_departamento') != false){
            $data_search['id_Departamento'] = $this->input->post('id_departamento');
        }

        if($this->input->post('id_puesto') != false){
            $data_search['id_Puesto'] = $this->input->post('id_puesto');
        }

        $this->load->model('General_model');
        if(count($data_search)>0){
            $trabajadores = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_findAll',$data_search,'get');
        }else{
            $trabajadores = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',array(),'get');
        }
        
        $this->response($trabajadores);
    }

    public function index_busqueda(){
        $data_search = $this->input->post();

        $this->load->model('General_model');
        $trabajadores = $this->General_model->call_api('reportes/trabajador/reporte_horas_extras',$data_search,'post');
        
        $this->response($trabajadores);
    }


    public function index_pdf(){

        $dataContent = $this->input->post();
      
        $this->load->library('Pdf',array(
            'titulo' => 'Reporte de horas extras',
            'position' => 'P',
            'size' => 'A4'
        ));

        $filterData = array(
            'departamentos' => $this->input->post('departamento_desc'),
            'puesto' => $this->input->post('puesto_desc'),
            'jornada' => $this->input->post('jornada_desc'),
            'fecha_inicio' => $this->input->post('fecha_inicio_desc'),
            'fecha_fin' => $this->input->post('fecha_fin_desc')
        );

        $this->load->library('parser');
        $view = $this->parser->parse('/reportes/horas_extras/index_pdf',$filterData,true);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);


        $this->load->library('table');
        $template = array(    
            'table_open'  => '<table border="0" cellpadding="4" cellspacing="" class="mytable" style="background-color: #f3f4f5;border-color:#c3c3c3;">',
            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th style="font-weight:bold; text-align:left;background-color: #c3c3c3;" >',
            'heading_cell_end'      => '</th>',
            'row_start'             => '<tr style="border-bottom: 1px solid #ddd;background-color: #ffffff;" >',
            'row_end'               => '</tr>',
            'cell_start'            => '<td style="padding: 5px;" >',
            'cell_end'              => '</td>',
        );        
        $this->table->set_template($template);
        $this->table->set_heading(array(
            'Clave', 
            'Nombre del trabajador',
            'J. Completa',
            'Fecha',
            'Horas'
        ));

        $this->load->model('General_model');
        $reporte = $this->General_model->call_api('reportes/trabajador/reporte_horas_extras',$dataContent,'post');
        
        foreach ($reporte['data'] as $key => $value) {
            $this->table->add_row(
                $value['Clave'], 
                trim($value['Nombre'].' '.$value['Apellido_1'].' '.$value['Apellido_2']), 
                $value['JornadaCompleta'], 
                utils::aFecha($value['Fecha'],true), 
                $value['Horas']       
            );
        }

        $table = array(
            'table' => $this->table->generate()
        );
        $view = $this->parser->parse('/inicio/reportenomina/index_pdf_table',$table,true);
        $this->tcpdf->SetFont('', '', 9);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);

        $this->tcpdf->Output('Reporte horas extras.pdf', 'D');
    }
}