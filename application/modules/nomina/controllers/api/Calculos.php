<?php defined('BASEPATH') or exit('No direct script access allowed');

class Calculos extends MY_Controller
{
    public $api_path = '';
    public $results = array('status' => 'success', 'data' => false, 'message' => false);
    public $httpStatus = 200;

    public function __construct()
    {
        parent::__construct();
        if($_SERVER['HTTP_HOST'] == 'localhost'){
            $this->api_path = 'http://localhost/sistema_nomina/';
        }else{
            $this->api_path = 'https://www.sohex.net/nomina_dms/';
        }
    }

    public function factor_integracion(){

        $catalogo = $this->input->post('catalogo');
        $fecha_alta = $this->input->post('fecha_alta');
        $monto = $this->input->post('monto');
        
        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('configuracion/transacciones/calculo_factor_integracion',array(
            'catalogo' => $catalogo,
            'fecha_alta' => $fecha_alta,
            'monto' => $monto
        ),'post');
        $this->response($dataContent);
    }
}