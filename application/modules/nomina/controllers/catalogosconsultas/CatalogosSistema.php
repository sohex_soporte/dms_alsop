<?php defined('BASEPATH') or exit('No direct script access allowed');

class CatalogosSistema extends CI_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->title = 'Nomina';
        $this->breadcrumb = array(
            'Catalogos y consultas',
            array('name'=>'Catálogos del Sistema','url'=>site_url('nomina/catalogosconsultas/catalogossistema')),
        );
    }

    public function index()
    {
        $this->breadcrumb[] = 'Menú';
        $this->title = 'Catálogos del Sistema';
        $html = $this->load->view('/catalogosconsultas/catalogossistema/index',false,true);
        $this->output($html);
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'titulo_modulo' => $this->title,
            'modulo' => 'Nomina',
            'breadcrumb' => $this->breadcrumb,
            'content'=>$html
        ));
    }

    
}
