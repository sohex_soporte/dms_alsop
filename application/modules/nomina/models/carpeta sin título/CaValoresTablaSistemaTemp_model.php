<?php defined('BASEPATH') or exit('No direct script access allowed');

class CaValoresTablaSistemaTemp_model  extends CI_Model  {

    public $db_nomina;
	function __construct()
    {
        parent::__construct();
        $this->db_nomina = $this->load->database('nomina', TRUE);
    }

    public function countDataTemp($id_tabla,$id_usuario){
        $this->db_nomina
            ->select('id,id_Usuario,id_TablaSistema,Valor_1,Valor_2,Valor_3')
            ->from('ca_valorestablasistemaTemp')
            ->where('id_TablaSistema',$id_tabla)
            ->where('id_Usuario',$id_usuario);
        return $this->db_nomina->count_all_results();
    }

    public function delete($id){
        $this->db_nomina->where('id',$id);
        $this->db_nomina->limit(1);
        return $this->db_nomina->delete('ca_valorestablasistemaTemp');
    }
}