<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Recepcion extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Recepción de Unidades";
        $data['subtitulo'] = "Listado";
        $this->blade->render('recepcion/listado', $data);
    }

    public function ajax_recepcion()
    {
        $this->load->library('curl');
        $request = $this->curl->curlGet('api/unidades');
        $data_reporte = procesarResponseApiJsonToArray($request);
        $data['data'] = isset($data_reporte) ? $data_reporte : [];
        echo json_encode($data);
    }


    public function alta_nojala()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        // esto puede ir en el constructor y reutilizar codigo
        $usuario_recibe = !empty($this->session->userdata('nombre')) ?
            $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno')
            : "Visitante";

        $data['usuario'] = $usuario_recibe;
        $data['id_vendedor'] = $this->session->userdata('id');

        //Cargamos los catalogos
        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

        // catalogo-modelos
        $modelo = $this->curl->curlGet('api/cat-lineas');
        $data['cat_modelo'] = procesarResponseApiJsonToArray($modelo);

        $anio = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

        $color = $this->curl->curlGet('api/catalogo-colores');
        $data['cat_color'] = procesarResponseApiJsonToArray($color);

        $interiores = $this->curl->curlGet('api/catalogo-vestiduras');
        $data['cat_interiores'] = procesarResponseApiJsonToArray($interiores);

        $ubicacion = $this->curl->curlGet('api/catalogo-ubicacion');
        $data['cat_ubicacion'] = procesarResponseApiJsonToArray($ubicacion);

        $llaves = $this->curl->curlGet('api/catalogo-ubicacion-llaves');
        $data['cat_llaves'] = procesarResponseApiJsonToArray($llaves);

        $autos = $this->curl->curlGet('api/catalogo-autos');
        $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

        $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/ultimo-registro');
        $dataregistro = json_decode($dataFromApi, TRUE);

        if (isset($dataregistro["id"])) {
            $data['consecutivo'] = $dataregistro["id"];
        }

        $catalogos = $this->pseudo_catalogos();
        $data['cat_combustible'] = $catalogos['cat_combustible'];

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Recepción de Unidades";
        $data['subtitulo'] = "Registro";

        $this->blade->render('recepcion/alta', $data);
    }

    public function pseudo_catalogos()
    {
        $data_combustible =  [
            ['id' => 1, 'nombre' => 'Gasolina'],
            ['id' => 2, 'nombre' => 'Diesel'],
            ['id' => 3, 'nombre' => 'Híbrido'],
            ['id' => 4, 'nombre' => 'Eléctrico']
        ];
        $new_combustible = [];
        foreach ($data_combustible as $key => $value) {
            $item = new \stdClass;
            $item->id = $value['id'];
            $item->nombre = $value['nombre'];
            array_push($new_combustible, $item);
        }

        return [
            'cat_combustible' => $new_combustible
        ];
    }

    public function editar($id = '')
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Recuperamos la informacion del registro
            $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro[0] : [];

            //Cargamos los catalogos
            $marcas = $this->curl->curlGet('api/catalogo-marcas');
            $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

            $anio = $this->curl->curlGet('api/catalogo-anio');
            $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

            // catalogo-modelos
            $modelo = $this->curl->curlGet('api/cat-lineas');
            $data['cat_modelo'] = procesarResponseApiJsonToArray($modelo);

            $color = $this->curl->curlGet('api/catalogo-colores');
            $data['cat_color'] = procesarResponseApiJsonToArray($color);

            $interiores = $this->curl->curlGet('api/catalogo-vestiduras');
            $data['cat_interiores'] = procesarResponseApiJsonToArray($interiores);

            $ubicacion = $this->curl->curlGet('api/catalogo-ubicacion');
            $data['cat_ubicacion'] = procesarResponseApiJsonToArray($ubicacion);

            $llaves = $this->curl->curlGet('api/catalogo-ubicacion-llaves');
            $data['cat_llaves'] = procesarResponseApiJsonToArray($llaves);

            $autos = $this->curl->curlGet('api/catalogo-autos');
            $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

            if (!isset($data['data']->usuario)) {
                if ($this->session->userdata('id_sesion_dms')) {
                    $data['usuario'] = $this->session->userdata('user_sesion_dms');
                } else {
                    $data['usuario'] = "Visitante";
                }
            }
            $catalogos = $this->pseudo_catalogos();
            // dd($data);
            $data['cat_combustible'] = $catalogos['cat_combustible'];
            $data['modulo'] = "Autos";
            $data['submodulo'] = "Inventario";
            $data['titulo'] = "Recepción de Unidades";
            $data['subtitulo'] = "Edición";

            $this->blade->render('recepcion/alta', $data);
        }
    }

    //Recupéramos el ultimo id registrado
    public function ultimo_id()
    {
        $respuesta = "0";
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/ultimo-registro');
        $dataregistro = json_decode($dataFromApi, TRUE);

        if (isset($dataregistro["id"])) {
            $respuesta = $dataregistro["id"];
        }

        echo $respuesta;
    }
}
