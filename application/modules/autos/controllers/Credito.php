<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Credito extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Creditos";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "Solicitudes";
        $data['id_tab'] = 2;
        $this->blade->render('credito/solicitudes_credito', $data);
    }

    public function ajax_listado_solicitudes_credito()
    {
        $responseData = $this->curl->curlGet('api/venta-unidades/solicitudes-credito');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = isset($response) ? $response : [];
        echo json_encode($data);
    }

    public function aprobarcredito($id_preventa = '')
    {
        if ($id_preventa == '') {
            return $this->index();
        }

        $responseData = $this->curl->curlGet('api/venta-unidades/solicitudes-credito/' . $id_preventa);
        $response = procesarResponseApiJsonToArray($responseData);
        $data['detalle_preventa'] = isset($response[0]) ? $response[0] : [];
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Creditos";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "Solicitudes";
        $data['id_tab'] = 2;
        // dd($data);
        $this->blade->render('credito/aprobar_denegar', $data);
    }

    public function autorizacion_credito()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Creditos";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "Solicitudes";
        $data['id_tab'] = 2;
        $this->blade->render('credito/gerente_ventas_autorizar_credito', $data);
    }

    public function transferencias()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Creditos";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "Solicitudes";
        $data['id_tab'] = 2;
        $this->blade->render('credito/solicitudes_transferencias', $data);
    }

    public function ajax_listado_transferencias()
    {
        $responseData = $this->curl->curlGet('api/venta-unidades/listado-solicitudes-transferencias');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = isset($response) ? $response : [];
        echo json_encode($data);
    }

    public function autorizatransferencia($id_cxc = '')
    {
        $responseData = $this->curl->curlGet('api/venta-unidades/solicitudes-transferencias/' . $id_cxc);
        $response = procesarResponseApiJsonToArray($responseData);
        $data['detalle_preventa'] = isset($response[0]) ? $response[0] : [];
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Creditos";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "Solicitudes";
        $data['id_tab'] = 2;
        $this->blade->render('credito/autorizar_transferencias', $data);
    }

    public function facturacion_documentacion()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Facturacion";
        $data['subtitulo'] = "solicitudes";

        $this->blade->render('credito/solicita_facturacion_listado', $data);
    }

    public function documentacionfacturacion($id_venta_unidad = '')
    {
        $data['id_tab'] = 5;
        $data['modulo'] = "Documentación";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre venta";
        $data['subtitulo'] = "Facturacion";
        $data['id_venta_unidad'] = $id_venta_unidad;

        $dataInfoVentaDocs = $this->curl->curlGet('api/informaciondocumentosventa/byidventaunidad/' . $id_venta_unidad);
        $datainfoVentas = procesarResponseApiJsonToArray($dataInfoVentaDocs);
        $data['info_ventas'] = isset($datainfoVentas) ? $datainfoVentas : [];
        $data['nombre_cliente'] = $datainfoVentas->cliente_apellido_paterno . ' ' . $datainfoVentas->cliente_apellido_materno . ' ' . $datainfoVentas->nombre_cliente;

        $dataFromApi = $this->curl->curlGet('api/documentosventa/byidventaunidad/' . $id_venta_unidad);
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['archivos_subidos'] = isset($dataregistros) ? (array)$dataregistros : [];
        $this->blade->render('credito/documentacion_facturacion', $data);
    }

    public function ajax_solicitud_facturacion()
    {
        $query = [];
        $query['solicita_facturacion'] = 1;

        $responseData = $this->curl->curlGet('api/autos/pre-pedidos?' . http_build_query($query));
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = isset($response) ? $response : [];
        echo json_encode($data);
    }
}
