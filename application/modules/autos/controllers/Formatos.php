<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Formatos extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }
    
    public function formatosalida($id='')
    {
        //Si se recupera un id en el alta, cargamos la informacion de la unidad
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general'); 

            //Recuperamos la informacion del registro
            $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/' . $id);
            $valores = json_decode($dataFromApi,TRUE);            

            $anio = $this->curl->curlGet('api/catalogo-anio');
            $cat_anio = procesarResponseApiJsonToArray($anio);

            $color = $this->curl->curlGet('api/catalogo-colores');
            $cat_color = procesarResponseApiJsonToArray($color); 

            $autos = $this->curl->curlGet('api/catalogo-autos');
            $cat_unidades = procesarResponseApiJsonToArray($autos);

            if (isset($valores[0]["id"])) {
                echo "Aqui";
                $data["economico"] = $valores[0]['n_economico'];
                $data["serie"] = $valores[0]['vin'];
                $data["vehiculo"] = $valores[0]['unidad_descripcion'];

                if ($valores[0]['color_id'] != "0") {
                    foreach ($cat_color as $color) {
                        if ($valores[0]['color_id'] == $color->id) {
                            $data["color"] = $color->nombre;
                        } 
                    }
                } else {
                    $data["color"] = "OTRO";
                }
                
                if ($valores[0]['anio_id'] != "0") {
                    foreach ($cat_anio as $anio) {
                        if ($valores[0]['anio_id'] == $anio->id) {
                            $data["modelo"] = $anio->nombre;
                        } 
                    }
                } else {
                    $data["modelo"] = "OTRO";
                }

                if ($valores[0]['catalogo_id'] != "0") {
                    foreach ($cat_unidades as $unidad) {
                        if ($valores[0]['catalogo_id'] == $unidad->id) {
                            $data["catalogo"] = $unidad->clave;
                        } 
                    }
                } else {
                    $data["catalogo"] = "OTRO";
                }
            }
        }

        if($this->session->userdata('nombre')){
            $data['usuario'] = $this->session->userdata('nombre')." ".$this->session->userdata('apellido_paterno')." ".$this->session->userdata('apellido_materno');
        }else{
            $data['usuario'] = "Visitante"; 
        }
        
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Checklist Entrada";
        $data['subtitulo'] = "Registro";
        $this->blade->render('formatos/formulario', $data);
    }
    
    public function editar($id='')
    {
        $data['data'] = [];
        $data['titulo'] = "Clientes";
        $this->blade->render('formatos/formulario', $data);
    }

    //Convertir canvas base64 a imagen (firmas)
    public function firmas()
    {
        $imgBase64 = $_POST['base_64'];
        $destino = $_POST['destino'];
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=3; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'img/'.$destino.'';

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='img/'.$destino.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,4) == "img/" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        echo $base;
    }
}
