@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    @include('ventas/partial_venta_steps')
    <h3>Datos del cliente</h3>
    <div class="row">
        <div class="col-md-4 mt-4">
            <?php renderInputText("text", "cliente_nombre", "Cliente",  isset($detalle_preventa->nombre) ? $detalle_preventa->nombre: ''); ?>
        </div>
        <div class="col-md-4 mt-4">
            <?php renderInputText("text", "cliente_nombre", "Apellido",  isset($detalle_preventa->apellido_paterno) ? $detalle_preventa->apellido_paterno: ''); ?>
        </div>
        <div class="col-md-4 mt-4">
            <?php renderInputText("text", "rfc", "RFC",  isset($detalle_preventa->rfc) ? $detalle_preventa->rfc: ''); ?>
        </div>
    </div>
    
    <h3>Unidad</h3>
    <div class="row">
        <div class="col-md-4">
        <input type="hidden" id="id_preventa" name="id_preventa" value="{{$detalle_preventa->id}}">
            <?php renderInputText("text", "unidad_descripcion", "Descripcion unidad",  isset($detalle_preventa->unidad_descripcion) ? $detalle_preventa->unidad_descripcion: ''); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "economico", "No. economico",  isset($detalle_preventa->economico) ? $detalle_preventa->economico: ''); ?>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="aprobado">¿El credito es aprobado?</label>
                <select class="form-control" name="aprobado" id="aprobado">
                    <option value=""> Seleccionar ..</option>
                    <option value="1">Aprobado</option>
                    <option value="0">Denegado</option>
                </select>
                <div id="aprobado_error" class="invalid-feedback"></div></div>            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right mt-3">
            <button id="btn_credito" class="btn btn-primary">
                Confirmar estatus
            </button>
        </div>
    </div>


</div>
@endsection

@section('scripts')
    <script type="text/javascript">
       $("#btn_credito").on('click', function() {
            $(".invalid-feedback").html("");
            if(document.getElementById('aprobado').value == ''){
                return toastr.error("Indicar el estatus del credito");
            }
            
            const id_estatus = document.getElementById('aprobado').value == 1 ? 2 : 3;
            var id = $("#id_preventa").val();
            // return console.log(estatus);
            ajax.put('api/venta-unidades/'+id, {
                "credito_aprovado":document.getElementById('aprobado').value,
                "id_estatus":id_estatus,
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                utils.displayWarningDialog("Estatus de credito actualizado", "success", function(data) {
                    return window.location.href = base_url + 'autos/credito/index';
                })
            })
        });
    </script>
@endsection
