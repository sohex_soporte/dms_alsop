@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    @include('ventas/partial_venta_steps')
    <h3>Datos de transferencia</h3>
    <div class="row">
        <div class="col-md-4 mt-4">
            <?php renderInputText("text", "concepto", "Concepto",  isset($detalle_preventa->concepto) ? $detalle_preventa->concepto: '', true); ?>
        </div>
        <div class="col-md-4 mt-4">
            <?php renderInputText("text", "total", "Total",  isset($detalle_preventa->total) ? $detalle_preventa->total: '', true); ?>
        </div>
        <div class="col-md-4 mt-4">
            <?php renderInputText("text", "unidad_descripcion", "Descripción de la unidad",  isset($detalle_preventa->unidad_descripcion) ? $detalle_preventa->unidad_descripcion: '',true); ?>
            <input type="text" name="id_cxc" id="id_cxc" value="{{$detalle_preventa->id_cxc}}">
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="aprobado">¿Se autoriza la transferencia?</label>
                <select class="form-control" name="aprobado" id="aprobado">
                    <option value=""> Seleccionar ..</option>
                    <option value="2">Aprobado</option>
                    <option value="3">Denegado</option>
                </select>
                <div id="aprobado_error" class="invalid-feedback"></div></div>            
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 text-right mt-3">
            <button id="btn_credito" class="btn btn-primary">
                Confirmar Traspaso
            </button>
        </div>
    </div>

</div>
@endsection

@section('scripts')
    <script type="text/javascript">
       $("#btn_credito").on('click', function() {
            $(".invalid-feedback").html("");
            if(document.getElementById('aprobado').value == ''){
                return toastr.error("Indicar el estatus del traspaso");
            }
            
            const id_estatus = document.getElementById('aprobado').value;
            var id = $("#id_cxc").val();

            ajax.put('api/venta-unidades/autorizar-transferencias/'+id, {
                "id_estatus_autorizado":id_estatus
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                utils.displayWarningDialog("Transferencia Autorizada", "success", function(data) {
                    return window.location.href = base_url + 'autos/credito/index';
                })
            })
        });
    </script>
@endsection
