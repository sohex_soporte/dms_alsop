<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Check List</title>
	<style>
		.contenedor {
			width: 100%;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		td {
			font-size: 11px !important;
			text-align: right !important;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		.text-center {
			text-align: center;
		}

		table,
		th,
		td {
			border: 1px solid #323232;
			margin-bottom: 12px;
			text-align: left;
		}

		.sep10 {
			width: 100%;
			height: 10px;
			clear: both;
		}
	</style>
</head>

<body>
	<header>
		<table border="0" width="100%" style="border:0px !important" cellspacing="0" cellpadding="2" width="100%">
			<tr VALIGN=middle style="border:0px !important">
				<td style="width:35%; border:0px !important">
					<img style="height:50px;" src="<?php echo base_url('img/logo_queretaro_2.png'); ?>" class="img-fluid" />
				</td>
				<td style="width:35%; border:0px !important">
					<b style="font-size:14px"><?php echo NOMBRE_SUCURSAL;?>  S.A. de C.V.</b>
				</td>
				<td style="width:30%; border:0px !important; font-weight:bold; text-align:right">
					<img style="height:30px;" src="<?php echo base_url('img/logo.png'); ?>" class="img-fluid" />
				</td>
			</tr>
			<tr VALIGN=middle style="border:0px !important">

				<td style="width:35%; border:0px !important">
				</td>
				<td style="width:35%; border:0px !important">
					<h2>Check List</h2>
				</td>
				<td style="width:30%; border:0px !important; font-weight:bold; text-align:right">
					<table class="table" cellpadding="3" style="width:80%;" align="right">
						<tr>
							<td colspan="3"><b>PEDIDO NO.</b></td>
						</tr>
						<tr>
							<td colspan="3" style="font-size: 11px; background-color:#ddd">
								FECHA
							</td>
						</tr>
						<tr>
							<td>DD</td>
							<td>MM</td>
							<td>YYYY</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</header>
	<div class="contenedor">
		<div class="col-12 text-center">
		</div>
	</div>
	<div class="contenedor">
		<?php
		// dd($info_ventas);
		?>
		<div class="col-12">
			<table class="table" cellpadding="5">
				<tbody>
					<tr>
						<th colspan="4" style="font-size: 11px; background-color:#ddd" scope="col">A. COMPRADOR</th>
					</tr>

					<tr>
						<th colspan="1" style="font-size: 11px; width:25%" scope="col">INVENTARIO:</th>
						<td colspan="3" style="font-size: 11px" scope="col"><?php echo isset($inventario) ? $inventario :  ''; ?></td>
					</tr>
					<tr>
						<th colspan="1" style="font-size: 11px" scope="col">COMPRADOR:</th>
						<td colspan="1" style="font-size: 11px" scope="col"><?php echo isset($info_ventas->nombre_cliente) ? $info_ventas->nombre_cliente :  ''; ?></td>
						<th colspan="1" style="font-size: 11px" scope="col">CLT:</th>
						<td colspan="1" style="font-size: 11px" scope="col"><?php echo isset($clt) ? $clt :  ''; ?></td>
					</tr>
					<tr>
						<th colspan="1" style="font-size: 11px" scope="col">SERIE:</th>
						<td colspan="3" style="font-size: 11px" scope="col"><?php echo isset($serie) ? $serie :  ''; ?></td>
					</tr>
					<tr>
						<th colspan="1" style="font-size: 11px" scope="col">CATALOGO:</th>
						<td colspan="3" style="font-size: 11px" scope="col"><?php echo !empty($info_ventas->descripcion_linea) ? $info_ventas->descripcion_linea :  ''; ?></td>
					</tr>
					</tr>
				</tbody>
			</table>
			<div class="sep10"></div>
			<table style="border:0px !important">
				<tr style="border:0px !important">
					<td style="border:0px !important">
						<table class="table" cellpadding="5" style="width:99%">
							<thead>
								<tr>
									<th colspan="2" style="font-size: 11px; background-color:#ddd" scope="col">C.: FACTURACION TRADICIONAL</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="1" style="font-size: 11px; width:90%" scope="col">PEDIDO</td>
									<td colspan="1" style="font-size: 11px; width:10%" scope="col"><?php echo array_key_exists('1', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">REMISION</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('2', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">IFE</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('3', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CURP</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('4', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">COMPROBANTE DE DOMICILIO</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('5', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">FORMATO ID DEL CLT</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('6', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">RECIBO DE CAJA DEL ANTICIPO</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('7', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">FORMATO DE BONO FORD</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('8', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CONSTANCIA DE SITUACIÓN FISCAL</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('9', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">HOJA SICOP "FICHA DE SEGUIMIENTO"</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('10', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CORREO DE CONTACTO DEL CLIENTE</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('11', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">VoBo AREA DE PROCESOS (MAIL)</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('12', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">ACTA CONSTITUTIVA PERSONA MORAL</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('13', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CONSTANCIA DE SITUACIÓN FISCAL</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('14', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">COMPROBANTE DE DOMICILIO</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('15', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">PODER REPESENTANTE LEGAL</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('16', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">IFE REPRESENTANTE LEGAL</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('17', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CURP REPRESENTANTE LEGAL</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('18', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CONSULTA DE LISTAS NEGRAS</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('19', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CARTA RENUNCIA EXT. GARANTIA</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('20', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<th colspan="2" style="font-size: 11px; background-color:#ddd" scope="col">F. FLOTILLAS</th>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">ORDEN DE COMPRA</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('21', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">COTIZACION FORD</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('22', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">BOLETIN STREET PROGRAM</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('23', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="border:0px !important; width:50%">
						<table class="table" cellpadding="5" style="width:99%">
							<thead>
								<tr>
									<th colspan="2" style="font-size: 11px; background-color:#ddd" scope="col">D.: FORD CREDIT, BANCOS Y AUTOPCION</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">APROBACION FORD CREDIT</td>
									<td colspan="1" style="font-size: 11px; width:10%" scope="col"><?php echo array_key_exists('24', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">RECIBO DE CAJA DEL ANTICIPO o ENG</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('25', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CARATULA DE FIRMA OTROS BANCOS</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('26', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">FORMATO DE BONO FORD CREDIT</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('27', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">FORMATO DE BONO FORD</td>
									<td colspan="1" style="font-size: 11px" scope="col"><?php echo array_key_exists('28', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<th colspan="2" style="font-size: 11px; background-color:#ddd" scope="col">E.- CONAUTO</th>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">CARTA CREDITO AUTORIZADA</td>
									<td colspan="1" style="font-size: 11px; width:10%" scope="col"><?php echo array_key_exists('29', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">APROBACION DE COND. AUTO OPCION</td>
									<td colspan="1" style="font-size: 11px; width:10%" scope="col"><?php echo array_key_exists('30', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">RECIBO DE CAJA DE PAGO CLIENTE</td>
									<td colspan="1" style="font-size: 11px; width:10%" scope="col"><?php echo array_key_exists('31', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td colspan="1" style="font-size: 11px" scope="col">FORMATO DE BONO FORD</td>
									<td colspan="1" style="font-size: 11px; width:10%" scope="col"><?php echo array_key_exists('32', $archivos_subidos) ? 'X' :  ''; ?></td>
								</tr>
								<tr>
									<td style="height:340px; " colspan="2"></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
			<div style="page-break-after:always;"></div>
			<div class="sep10"></div>
			<table class="table" cellpadding="5" style="border:1 !important">
				<tr style="border:0px !important;">
					<td style="border:0px !important; padding-bottom:50px" colspan="1">
						<img style="height:30px;" src="<?php echo base_url('img/logo.png'); ?>" class="img-fluid" />
					</td>
					<td style="border:0px !important; padding:10px !important; font-weight:bold; font-size:14px" colspan="3">UNIDAD LISTA PARA REPORTAR EN SMARTVINCENT</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center; font-weight:bold">VIABLE PARA REPORTE</td>
					<td> SI <?php echo isset($info_ventas->viable) && $info_ventas->viable == 1 ? 'X' :  ''; ?></td>
					<td> NO <?php echo isset($info_ventas->viable) && $info_ventas->viable == 1 ? 'X' :  ''; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="width:30%; background-color:#ddd ">TIPO DE VENTA</td>
					<td colspan="3"><?php echo $info_ventas->tipo_venta == 1 ? "CONTADO" : 'CREDITO'; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="background-color:#ddd">FECHA DE FACTURA</td>
					<td colspan="3"><?php echo isset($info_ventas->fecha_factura) ? utils::aFecha($info_ventas->fecha_factura, true) : ''; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="background-color:#ddd">FECHA DE ULT. INGRESO</td>
					<td colspan="3"><?php echo isset($info_ventas->fecha_ultimo_ingreso) ? utils::aFecha($info_ventas->fecha_ultimo_ingreso, true) : ''; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="background-color:#ddd">FECHA DE CONTRATO F.C.</td>
					<td colspan="3"><?php echo isset($info_ventas->fecha_contrato) ? utils::aFecha($info_ventas->fecha_contrato, true) : ''; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="background-color:#ddd">FECHA CARTA CONAUTO</td>
					<td colspan="3"><?php echo isset($info_ventas->fecha_carta_conauto) ? utils::aFecha($info_ventas->fecha_carta_conauto, true) : ''; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="background-color:#ddd">FECHA ORDEN COMPRA</td>
					<td colspan="3"><?php echo isset($info_ventas->fecha_orden_compra) ? utils::aFecha($info_ventas->fecha_orden_compra, true) : ''; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="background-color:#ddd">FECHA DE VENTA PARA SMART</td>
					<td colspan="3"><?php echo isset($info_ventas->fecha_venta_smart) ? utils::aFecha($info_ventas->fecha_venta_smart, true) : ''; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="background-color:#ddd">FECHA DE INGRESO EN SMART</td>
					<td colspan="3"><?php echo isset($info_ventas->fecha_ingreso_smart) ? utils::aFecha($info_ventas->fecha_ingreso_smart, true) : ''; ?></td>
				</tr>
			</table>
			<div class="sep10"></div>
			<table class="table" cellpadding="5" style="border:0px !important">
				<tr>
					<td style="padding:50px !important; border:0px !important; border-bottom:1px solid #323232;">
						<img style="height:120px;width: 100%;" class="img-fluid img-firma-big" src="<?php echo isset($info_ventas->firma_gerente) ?  $info_ventas->firma_gerente : ''; ?>">
					</td>
					<td style="border:0px !important; border-bottom:1px solid #323232;">
						<img style="height:120px;width: 100%;" class="img-fluid img-firma-big" src="<?php echo isset($info_ventas->firma_otros) ?  $info_ventas->firma_otros : ''; ?>">
					</td>
				</tr>
				<tr style="border:0px !important">
					<td style="text-align:center; width:48%; font-size:16px">GERENCIA</td>
					<td style="text-align:center; font-size:16px">FIRMA</td>
				</tr>
			</table>
		</div>
	</div>
</body>

</html>