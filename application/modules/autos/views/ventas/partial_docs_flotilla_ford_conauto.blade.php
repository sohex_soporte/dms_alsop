<div class="row">
    <div class="col-md-12 mb-3">
        <h3>F.- FLOTILLAS</h3>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('orden_compra', 'ORDEN DE COMPRA'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('21', $archivos_subidos))
                    <button onclick='handleUpload(21, "orden_compra")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('21', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[21]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(21, "orden_compra")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('cotizacion_ford', 'COTIZACION FORD'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('22', $archivos_subidos))
                    <button onclick='handleUpload(22, "cotizacion_ford")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('22', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[22]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(22, "cotizacion_ford")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('boletin_street_program', 'BOLETIN STREET PROGRAM'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('23', $archivos_subidos))
                    <button onclick='handleUpload(24, "boletin_street_program")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('23', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[23]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(23, "boletin_street_program")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-12 mb-3">
        <h3>D.: FORD CREDIT, BANCOS Y AUTOPCION</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('aprobacion_ford_credit', 'APROBACION FORD CREDIT'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('24', $archivos_subidos))
                    <button onclick='handleUpload(24, "aprobacion_ford_credit")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('24', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[24]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(24, "aprobacion_ford_credit")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('recibo_caja_anticipo', 'RECIBO DE CAJA DEL ANTICIPO o ENG'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('25', $archivos_subidos))
                    <button onclick='handleUpload(25, "recibo_caja_anticipo")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('25', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[25]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(25, "recibo_caja_anticipo")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('caratula_firma_otros_bancos', 'CARATULA DE FIRMA OTROS BANCOS'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('26', $archivos_subidos))
                    <button onclick='handleUpload(26, "caratula_firma_otros_bancos")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('26', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[26]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(28, "caratula_firma_otros_bancos")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('formato_abono_ford_credit', 'FORMATO DE BONO FORD CREDIT'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('27', $archivos_subidos))
                    <button onclick='handleUpload(27, "formato_abono_ford_credit")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('27', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[27]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(27, "formato_abono_ford_credit")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('formato_abono_ford_ford_credit', 'FORMATO DE BONO FORD'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('28', $archivos_subidos))
                    <button onclick='handleUpload(28, "formato_abono_ford_ford_credit")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('28', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[28]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(28, "formato_abono_ford_ford_credit")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-12 mb-3">
        <h3>E.-      CONAUTO</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('carta_credito_autorizada', 'CARTA CREDITO AUTORIZADA'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('30', $archivos_subidos))
                    <button onclick='handleUpload(29, "carta_credito_autorizada")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif

                @if (array_key_exists('29', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[29]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(29, "aprov_cond_auto_opcion")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('aprov_cond_auto_opcion', 'APROBACION DE COND. AUTO OPCION'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('30', $archivos_subidos))
                    <button onclick='handleUpload(30, "aprov_cond_auto_opcion")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('30', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[30]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(30, "aprov_cond_auto_opcion")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('recibo_caja_pago_cliente', 'RECIBO DE CAJA DE PAGO CLIENTE'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('32', $archivos_subidos))
                    <button onclick='handleUpload(32, "recibo_caja_pago_cliente")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('31', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[31]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(31, "recibo_caja_pago_cliente")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <?php renderFileUpload('formato_abono_ford', 'FORMATO DE BONO FORD'); ?>
            </div>
            <div class="col-md-2 mt-3">
                @if (!array_key_exists('32', $archivos_subidos))
                    <button onclick='handleUpload(32, "formato_abono_ford")' class="btn btn-primary">
                        <i class="fas fa-cloud-upload-alt"></i>
                    </button>
                @endif
                @if (array_key_exists('32', $archivos_subidos))
                    <a target="_blank"
                        href="{{ API_URL_DEV . 'api/documentosventa/descargar/' . $archivos_subidos[32]->nombre_archivo }}"
                        class="btn btn-primary">
                        <i class="fas fa-download"></i>
                    </a>
                    <button onclick='handleUploadUpdate(32, "formato_abono_ford")' class="btn btn-primary"> <i class="fas fa-angle-double-up"></i></button>
                @endif
            </div>
        </div>
    </div>
</div>