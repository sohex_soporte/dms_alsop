@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="checklist_entrada" data-id="{{ isset($data->id) ? $data->id : '' }}"> <!-- enctype="multipart/form-data"  -->
				<div class="row">
                    <div class="col-md-3">
                        <img src="<?php echo base_url().'img/logo_queretaro_2.png' ?>" alt="" style="width: 100%;">
                    </div>
                    <div class="col-md-6">
                        <h3 style="text-align: center; color: darkblue;">Checklist de Entrega de Vehículos</h3> 
                    </div>
                    <div class="col-md-3">
                        <img src="<?php echo base_url().'img/ford_v2.png' ?>" alt="" style="width: 90%;height: 70%;">
                    </div>
                </div> 

                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Usuario de registra:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->usuario_recibe)){print_r($data->usuario_recibe);}else {echo $usuario;} ?>" id="usuario_recibe" name="usuario_recibe" placeholder="">
                            <div id="usuario_recibe_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha de registro:</label>
                            <input type="date" class="form-control" value="<?php if(isset($data->fecha_apertura)){print_r($data->fecha_apertura);}else {echo date('Y-m-d');} ?>" id="fecha_apertura" name="fecha_apertura" placeholder="">
                            <div id="fecha_apertura_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">                        
                        <div class="form-group">
                            <label for=""># Económico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data[0]->no_economico)) {print_r($data[0]->no_economico);} else {if(isset($economico)) echo $economico;} ?>" id="no_economico" name="no_economico">
                            <div id="no_economico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row"> 
                    <div class="col-md-3">
                        <label for="" style="font-size: 12px;">PEDIDO : </label>&nbsp;
                        <input type="text" class="form-control" name="pedido" id="pedido" value="<?php if(isset($data->pedido)) {print_r($data->pedido);} ?>" style="width:100%;">
                        <div id="pedido_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-3">
                        <label for="" style="font-size: 12px;">SERIE : </label>&nbsp;
                        <input type="text" class="form-control" value="<?php if(isset($data[0]->no_serie)) {print_r($data[0]->no_serie);} else {if(isset($serie)) echo $serie;} ?>" id="no_serie" name="no_serie">
                        <div id="no_serie_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-3">
                        <label for="" style="font-size: 12px;">VEHÍCULO :</label>&nbsp;
                        <input type="text" class="form-control" value="<?php if(isset($data[0]->vehiculo)) {print_r($data[0]->vehiculo);} else {if(isset($vehiculo)) echo $vehiculo;} ?>" id="vehiculo" name="vehiculo">
                        <div id="vehiculo_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-3">
                        <label for="" style="font-size: 12px;">COLOR :</label>&nbsp;
                        <input type="text" class="form-control" value="<?php if(isset($data[0]->color)) {print_r($data[0]->color);}else {if(isset($color)) echo $color;} ?>" id="color" name="color">
                        <div id="color_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-4">
                        <label for="" style="font-size: 13px;">Fecha de previa: </label>&nbsp;
                        <input class="form-control" type="date"  name="fecha_previa" id="fecha_previa" value="<?php if(isset($data->fecha_previa)) {print_r($data->fecha_previa);} else{ echo '0000-00-00';} ?>" style="width:100%;padding-top: 0px;">
                        <div id="fecha_previa_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <label for="" style="font-size: 13px;">Fecha de Elaboración de la Comanda: </label>&nbsp;
                        <input class="form-control" type="date"  name="fecha_comanda" id="fecha_comanda" value="<?php if(isset($data->fecha_comanda)) {print_r($data->fecha_comanda);} else{ echo '0000-00-00';} ?>" style="width:100%;padding-top: 0px;">
                        <div id="fecha_comanda_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12" style="background-color: #666699;">
                        <br>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-6" align="center">
                        <label style="font-weight: bold;text-align: center;font-size: 17px;">
                            COMPROMISO DE ENTREGA <br> A CLIENTE
                        </label>

                        <br>
                        <div class="row">
                            <div class="col-md-5" align="right" style="padding-top: 2%;">
                                <label for="" style="font-size: 12px;font-weight:bold;">FECHA DE ENTREGA</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" type="date" name="fecha_entrada" id="fecha_entrada" value="<?php if(isset($data->fecha_entrada)) {print_r($data->fecha_entrada);} else{ echo '0000-00-00';} ?>" style="width:100%;padding-top: 0px;" >
                                <div id="fecha_entrada_error" class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                        <div class="row" style="margin-top: 8px;">
                            <div class="col-md-5" align="right" style="padding-top: 2%;">
                                <label for="" style="font-size: 12px;font-weight:bold;">HORA DE ENTREGA</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" type="time" name="hora_entrada" id="hora_entrada" value="<?php if(isset($data->hora_entrada)) {print_r($data->hora_entrada);} else{ echo '00:00';} ?>" style="width:100%;padding-top: 0px;" >
                                <div id="hora_entrada_error" class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                        <br><br>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <label for="" style="font-size: 12px;font-weight:bold;"> FIRMA DE GERENTE DE EXPERIENCIA</label>
                                <br>
                                
                                <img class="marcoImg" src="<?php if(isset($data[0]->url_firma_gerente)) echo base_url().$data[0]->url_firma_gerente;?>" id="firma_gerente_img" alt="" style="height:2cm;width:4cm;">
                                <input type="hidden" id="ruta_firma_gerente" name="ruta_firma_gerente" value="<?php if(isset($data[0]->firma_gerente)) echo $firma_gerente;?>" >
                                <input type="hidden" id="url_firma_gerente" name="url_firma_gerente" value="<?php if(isset($data[0]->url_firma_gerente)) echo $data[0]->url_firma_gerente;?>" >

                                <br>

                                <input type="text" id="nombre_gerente" class="form-control" name="nombre_gerente" style="width:100%;" value="<?php if(isset($data[0]->nombre_gerente)) {print_r($data[0]->nombre_gerente);} ?>">
                                <div id="hora_entrada_error" class="invalid-feedback"></div>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4" align="center">
                                <?php if (isset($data[0]->url_firma_gerente)): ?>
                                    <?php if ($data[0]->url_firma_gerente == ""): ?>
                                        <a id="firma_gerente" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_gerente" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a id="firma_gerente" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_gerente" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                        Firmar
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>

                    <div class="col-md-6" align="center">
                        <label style="font-weight: bold;text-align: center;font-size: 17px;">
                            COMPROMISO DE ENTREGA <br> GERENTE DE EXPERIENCIA
                        </label>

                        <br>
                        <div class="row">
                            <div class="col-md-5" align="right" style="padding-top: 2%;">
                                <label for="" style="font-size: 12px;font-weight:bold;">FECHA DE ENTREGA</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" type="date" name="fecha_entrada_logistica" id="fecha_entrada_logistica" value="<?php if(isset($data->fecha_entrada_logistica)) {print_r($data->fecha_entrada_logistica);} else{ echo '0000-00-00';} ?>" style="width:100%;padding-top: 0px;" >
                                <div id="fecha_entrada_logistica_error" class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                        <div class="row" style="margin-top: 8px;">
                            <div class="col-md-5" align="right" style="padding-top: 2%;">
                                <label for="" style="font-size: 12px;font-weight:bold;">HORA DE ENTREGA</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" type="time" name="hora_entrada_logistica" id="hora_entrada_logistica" value="<?php if(isset($data->hora_entrada_logistica)) {print_r($data->hora_entrada_logistica);} else{ echo '00:00';} ?>" style="width:100%;padding-top: 0px;" >
                                <div id="hora_entrada_logistica_error" class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                        <br><br>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <label for="" style="font-size: 12px;font-weight:bold;"> FIRMA DE LOGÍSTICA </label>
                                <br>
                                
                                <img class="marcoImg" src="<?php if(isset($data[0]->url_firma_logistica_1)) echo base_url().$data[0]->url_firma_logistica_1;?>" id="firma_logistica_1_img" alt="" style="height:2cm;width:4cm;">
                                <input type="hidden" id="ruta_firma_logistica_1" name="ruta_firma_logistica_1" value="<?php if(isset($data[0]->firma_logistica_1)) echo $firma_logistica_1;?>" >
                                <input type="hidden" id="url_firma_logistica_1" name="url_firma_logistica_1" value="<?php if(isset($data[0]->url_firma_logistica_1)) echo $data[0]->url_firma_logistica_1;?>" >

                                <br>

                                <input type="text" id="nombre_logistica_1" class="form-control" name="nombre_logistica_1" style="width:100%;" value="<?php if(isset($data[0]->nombre_logistica_1)) {print_r($data[0]->nombre_logistica_1);} ?>">
                                <div id="hora_entrada_error" class="invalid-feedback"></div>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4" align="center">
                                <?php if (isset($data[0]->url_firma_logistica_1)): ?>
                                    <?php if ($data[0]->url_firma_logistica_1 == ""): ?>
                                        <a id="firma_logistica_1" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_logistica_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a id="firma_logistica_1" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_logistica_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                        Firmar
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12" style="background-color: #666699;">
                        <br>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-4 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Accesorios
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Extintor
                                    <input type="hidden" name="extintor" id="extintor" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->extintor : ''?>">
                                    <div id="extintor_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck1" name="extintor_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->extintor == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck1" name="extintor_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->extintor == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck1" name="extintor_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->extintor == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Gato
                                    <input type="hidden" name="gato" id="gato" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->gato : ''?>">
                                    <div id="gato_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck2" name="gato_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->gato == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck2" name="gato_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->gato == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck2" name="gato_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->gato == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Llanta de Refacción
                                    <input type="hidden" name="llanta" id="llanta" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->llanta : ''?>">
                                    <div id="llanta_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck3" name="llanta_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llanta == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck3" name="llanta_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llanta == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck3" name="llanta_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llanta == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Reflejantes
                                    <input type="hidden" name="reflejantes" id="reflejantes" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->reflejantes : ''?>">
                                    <div id="reflejantes_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck4" name="reflejantes_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->reflejantes == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck4" name="reflejantes_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->reflejantes == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck4" name="reflejantes_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->reflejantes == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Llave "L"
                                    <input type="hidden" name="llave_l" id="llave_l" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->llave_l : ''?>">
                                    <div id="llave_l_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck5" name="llave_l_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llave_l == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck5" name="llave_l_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llave_l == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck5" name="llave_l_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llave_l == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Cables pasa corriente
                                    <input type="hidden" name="cables_corriente" id="cables_corriente" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->cables_corriente : ''?>">
                                    <div id="cables_corriente_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck6" name="cables_corriente_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->cables_corriente == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck6" name="cables_corriente_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->cables_corriente == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck6" name="cables_corriente_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->cables_corriente == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Controles
                                    <input type="hidden" name="controles" id="controles" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->controles : ''?>">
                                    <div id="controles_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck7" name="controles_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->controles == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck7" name="controles_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->controles == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck7" name="controles_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->controles == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Seguro de ruedas
                                    <input type="hidden" name="seguro_ruedas" id="seguro_ruedas" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->seguro_ruedas : ''?>">
                                    <div id="seguro_ruedas_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck8" name="seguro_ruedas_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->seguro_ruedas == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck8" name="seguro_ruedas_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->seguro_ruedas == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck8" name="seguro_ruedas_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->seguro_ruedas == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-md-4 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Accesorios
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Tapones (si aplica)
                                    <input type="hidden" name="tapones" id="tapones" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->tapones : ''?>">
                                    <div id="tapones_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck9" name="tapones_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapones == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck9" name="tapones_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapones == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck9" name="tapones_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapones == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Rines
                                    <input type="hidden" name="rines" id="rines" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->rines : ''?>">
                                    <div id="rines_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck10" name="rines_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->rines == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck10" name="rines_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->rines == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck10" name="rines_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->rines == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Antena
                                    <input type="hidden" name="antena" id="antena" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->antena : ''?>">
                                    <div id="antena_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck11" name="antena_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->antena == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck11" name="antena_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->antena == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck11" name="antena_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->antena == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Tapón de Gasolina
                                    <input type="hidden" name="tapon_gasolina" id="tapon_gasolina" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->tapon_gasolina : ''?>">
                                    <div id="tapon_gasolina_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck12" name="tapon_gasolina_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapon_gasolina == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck12" name="tapon_gasolina_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapon_gasolina == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck12" name="tapon_gasolina_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapon_gasolina == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Encendedor
                                    <input type="hidden" name="encendendor" id="encendendor" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->encendendor : ''?>">
                                    <div id="encendendor_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck13" name="encendendor_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->encendendor == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck13" name="encendendor_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->encendendor == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck13" name="encendendor_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->encendendor == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Cenicero
                                    <input type="hidden" name="cenicero" id="cenicero" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->cenicero : ''?>">
                                    <div id="cenicero_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck14" name="cenicero_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->cenicero == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck14" name="cenicero_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->cenicero == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck14" name="cenicero_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->cenicero == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Tapetes (si aplica)
                                    <input type="hidden" name="tapetes" id="tapetes" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->tapetes : ''?>">
                                    <div id="tapetes_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck15" name="tapetes_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapetes == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck15" name="tapetes_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapetes == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck15" name="tapetes_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->tapetes == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Embudo
                                    <input type="hidden" name="embudo" id="embudo" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->embudo : ''?>">
                                    <div id="embudo_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck16" name="embudo_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->embudo == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck16" name="embudo_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->embudo == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck16" name="embudo_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->embudo == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-md-4 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Accesorios
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Llaves 2
                                    <input type="hidden" name="llaves_2" id="llaves_2" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->llaves_2 : ''?>">
                                    <div id="llaves_2_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck17" name="llaves_2_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llaves_2 == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck17" name="llaves_2_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llaves_2 == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck17" name="llaves_2_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->llaves_2 == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Luces
                                    <input type="hidden" name="luces" id="luces" value="<?php echo isset($data[0]->accesorios) ? $data[0]->accesorios->luces : ''?>">
                                    <div id="luces_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck18" name="luces_ck" value="SI" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->luces == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck18" name="luces_ck" value="NO" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->luces == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb1_ck18" name="luces_ck" value="NA" <?php if(isset($data[0]->accesorios)) {if( $data[0]->accesorios->luces == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    DOCUMENTOS
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Tarjeta de acceso
                                    <input type="hidden" name="tarjeta_acceso" id="tarjeta_acceso" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->tarjeta_acceso : ''?>">
                                    <div id="tarjeta_acceso_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck1" name="tarjeta_acceso_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->tarjeta_acceso == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck1" name="tarjeta_acceso_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->tarjeta_acceso == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck1" name="tarjeta_acceso_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->tarjeta_acceso == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Póliza de garantía
                                    <input type="hidden" name="poliza_garantia" id="poliza_garantia" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->poliza_garantia : ''?>">
                                    <div id="poliza_garantia_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck2" name="poliza_garantia_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->poliza_garantia == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck2" name="poliza_garantia_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->poliza_garantia == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck2" name="poliza_garantia_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->poliza_garantia == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Tarjeta de asistencia
                                    <input type="hidden" name="tarjeta_asistencia" id="tarjeta_asistencia" value="<?php echo isset($data[0]->documentacion) ? $data[0]->documentacion->tarjeta_asistencia : ''?>">
                                    <div id="tarjeta_asistencia_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck3" name="tarjeta_asistencia_ck" value="SI" <?php if(isset($data[0]->documentacion)) {if( $data[0]->documentacion->tarjeta_asistencia == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck3" name="tarjeta_asistencia_ck" value="NO" <?php if(isset($data[0]->documentacion)) {if( $data[0]->documentacion->tarjeta_asistencia == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck3" name="tarjeta_asistencia_ck" value="NA" <?php if(isset($data[0]->documentacion)) {if( $data[0]->documentacion->tarjeta_asistencia == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Manual de propietario
                                    <input type="hidden" name="manual" id="manual" value="<?php echo isset($data[0]->documentacion) ? $data[0]->documentacion->manual : ''?>">
                                    <div id="manual_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck4" name="manual_ck" value="SI" <?php if(isset($data[0]->documentacion)) {if( $data[0]->documentacion->manual == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck4" name="manual_ck" value="NO" <?php if(isset($data[0]->documentacion)) {if( $data[0]->documentacion->manual == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb2_ck4" name="manual_ck" value="NA" <?php if(isset($data[0]->documentacion)) {if( $data[0]->documentacion->manual == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-5 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    LIMPIEZA
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Motor limpio
                                    <input type="hidden" name="motor" id="motor" value="<?php echo isset($data[0]->limpieza ) ? $data[0]->limpieza->motor : ''?>">
                                    <div id="motor_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="motor_ck" value="SI" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->motor == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="motor_ck" value="NO" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->motor == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="motor_ck" value="NA" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->motor == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Carrocería limpia
                                    <input type="hidden" name="carroceria" id="carroceria" value="<?php echo isset($data[0]->limpieza ) ? $data[0]->limpieza->carroceria : ''?>">
                                    <div id="carroceria_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck2" name="carroceria_ck" value="SI" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->carroceria == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck2" name="carroceria_ck" value="NO" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->carroceria == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck2" name="carroceria_ck" value="NA" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->carroceria == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Golpes
                                    <input type="hidden" name="golpes" id="golpes" value="<?php echo isset($data[0]->limpieza ) ? $data[0]->limpieza->golpes : ''?>">
                                    <div id="golpes_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck3" name="golpes_ck" value="SI" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->golpes == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck3" name="golpes_ck" value="NO" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->golpes == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck3" name="golpes_ck" value="NA" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->golpes == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Rayones
                                    <input type="hidden" name="rayones" id="rayones" value="<?php echo isset($data[0]->limpieza ) ? $data[0]->limpieza->rayones : ''?>">
                                    <div id="rayones_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck4" name="rayones_ck" value="SI" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->rayones == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck4" name="rayones_ck" value="NO" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->rayones == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck4" name="rayones_ck" value="NA" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->rayones == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Interiores limpios
                                    <input type="hidden" name="interiores" id="interiores" value="<?php echo isset($data[0]->limpieza ) ? $data[0]->limpieza->interiores : ''?>">
                                    <div id="interiores_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck5" name="interiores_ck" value="SI" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->interiores == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck5" name="interiores_ck" value="NO" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->interiores == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck5" name="interiores_ck" value="NA" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->interiores == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Niveles
                                    <input type="hidden" name="niveles" id="niveles" value="<?php echo isset($data[0]->limpieza ) ? $data[0]->limpieza->niveles : ''?>">
                                    <div id="niveles_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck6" name="niveles_ck" value="SI" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->niveles == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck6" name="niveles_ck" value="NO" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->niveles == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck6" name="niveles_ck" value="NA" <?php if(isset($data[0]->limpieza )) {if( $data[0]->limpieza->niveles == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-md-7 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    ADICIONALES
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Accesorios
                                    <input type="hidden" name="accesorios" id="accesorios" value="<?php echo isset($data[0]->adicionales ) ? $data[0]->adicionales->accesorios : ''?>">
                                    <div id="accesorios_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="accesorios_ck" value="SI" <?php if(isset($data[0]->adicionales )) {if( $data[0]->adicionales->accesorios == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="accesorios_ck" value="NO" <?php if(isset($data[0]->adicionales )) {if( $data[0]->adicionales->accesorios == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb3_ck1" name="accesorios_ck" value="NA" <?php if(isset($data[0]->adicionales )) {if( $data[0]->adicionales->accesorios == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>

                        <br><br>
                        <div class="row">
                            <div class="col-md-7">
                                <img src="<?php echo base_url().'img/indicador_gas.png'; ?>" style="width:100%;" alt="">
                                <span class="error" id="error_gasolina"></span>
                            </div>
                            <div class="col-md-2">
                                <input class="form-control" type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' name="nivel_gasolina" id="nivel_gasolina" value="<?php if(isset($data->nivel_gasolina)) {print_r($data->nivel_gasolina);} ?>" style="width:100%;">
                                <div id="nivel_gasolina_error" class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-2" style="border-left: 1px solid black;">
                                <input class="form-control" type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' name="nivel_gasolina_1" id="nivel_gasolina_1" value="<?php if(isset($data->nivel_gasolina_1)) {print_r($data->nivel_gasolina_1);} ?>" style="width:100%;">
                                <div id="nivel_gasolina_1_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-6 table-responsive">
                        <label style="font-weight: bold;text-align: center;font-size: 17px;">
                            Condiciones de carroceria / daños
                        </label>
                        <div class="row">
                            <div class="col-md-3">
                                SI&nbsp;&nbsp;
                                <input type="radio" style="transform: scale(1.3);" value="1" name="danos" onclick="activar_marcas()" <?php if(isset($danos)) if($danos == "1") echo "checked";?> checked>
                            </div>

                            <div class="col-md-3">
                                NO&nbsp;&nbsp;
                                <input type="radio" style="transform: scale(1.3);" value="0" name="danos" onclick="activar_marcas()" <?php if(isset($danos)) if($danos == "0") echo "checked";?>>
                            </div>
                        </div>

                        <div class="row" style="border: 1px solid black; border-radius: 7px;padding: 10px;margin-top:10px;">
                            <div class="col-md-3">
                                Golpes &nbsp;
                                <input type="radio" style="transform: scale(1.3);" id="golpes" name="marcasRadio" checked value="hit">
                            </div>

                            <div class="col-md-5">
                                Roto / Estrellado &nbsp;
                            <input type="radio" style="transform: scale(1.3);" id="roto" name="marcasRadio" value="broken">
                            </div>

                            <div class="col-md-4">
                                Rayones &nbsp;
                                <input type="radio" style="transform: scale(1.3);" id="rayones" name="marcasRadio" value="scratch">
                            </div>
                        </div>

                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-12">
                                <?php if (isset($data[0]->url_carroceria)): ?>
                                    <?php if ($data[0]->url_carroceria == ""): ?>
                                        <canvas id="canvas_3" width="400" height="300" hidden="">
                                            Su navegador no soporta canvas.
                                        </canvas>
                                        <img src="<?php echo base_url().$data[0]->url_carroceria; ?>" alt="" style="width:400px;">
                                    <?php else: ?>
                                        <canvas id="canvas_3" width="400" height="300">
                                            Su navegador no soporta canvas.
                                        </canvas>
                                    <?php endif ?>
                                <?php else: ?>
                                    <canvas id="canvas_3" width="400" height="300">
                                        Su navegador no soporta canvas.
                                    </canvas>
                                <?php endif; ?>
                                <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'img/car.jpg'; ?>">
                                <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?php if(isset($data[0]->url_carroceria)) echo $data[0]->url_carroceria;?>"> 
                                <input type="hidden" name="" id="validaClick_1" value="0">
                                
                            </div>
                        </div>

                        <br><br>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-6" align="center">
                                <a href="<?php if(isset($data[0]->url_carroceria)) echo base_url().$data[0]->url_carroceria;?>" class="btn btn-info" id="btn-download" download="Diagrama_Diagnostico">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                    Descargar diagrama
                                </a>
                            </div>

                            <div class="col-md-6" align="center">
                                <?php if (!isset($data[0]->url_carroceria)): ?>
                                    <button type="button" class="btn btn-warning" id="resetoeDiagrama" style="margin-left: 20px;">
                                        <i class="fas fa-sync-alt"></i>
                                        Reiniciar diagrama
                                    </button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 table-responsive" align="center">
                        <label style="font-weight: bold;text-align: center;font-size: 14px;">
                            RECIBO DE UNIDAD
                        </label>

                        <br>
                        <div class="row">
                            <div class="col-md-5" align="right" style="padding-top: 2%;">
                                <label for="" style="font-size: 12px;font-weight:bold;">FECHA</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" type="date" name="fecha_recibo" id="fecha_recibo" value="<?php if(isset($data->fecha_recibo)) {print_r($data->fecha_recibo);} else{ echo '0000-00-00';} ?>" style="width:100%;padding-top: 0px;" >
                                <div id="fecha_recibo_error" class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                        <div class="row" style="margin-top: 8px;">
                            <div class="col-md-5" align="right" style="padding-top: 2%;">
                                <label for="" style="font-size: 12px;font-weight:bold;">HORA</label>
                            </div>
                            <div class="col-md-6">
                                <input class="form-control" type="time" name="hora_recibo" id="hora_recibo" value="<?php if(isset($data->hora_recibo)) {print_r($data->hora_recibo);} else{ echo '00:00';} ?>" style="width:100%;padding-top: 0px;" >
                                <div id="hora_recibo_error" class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <label for="" style="font-size: 12px;font-weight:bold;">
                                    NOMBRE Y FIRMA DE GERENTE DE EXPERIENCIA Y ASESOR 
                                </label>
                                <br>
                                
                                <img class="marcoImg" src="<?php if(isset($data[0]->url_firma_asesor)) echo base_url().$data[0]->url_firma_asesor;?>" id="firma_asesor_img" alt="" style="height:2cm;width:4cm;">
                                <input type="hidden" id="ruta_firma_asesor" name="ruta_firma_asesor" value="<?php if(isset($data[0]->firma_asesor)) echo $firma_asesor;?>" >
                                <input type="hidden" id="url_firma_asesor" name="url_firma_asesor" value="<?php if(isset($data[0]->url_firma_asesor)) echo $data[0]->url_firma_asesor;?>" >

                                <br>

                                <input type="text" id="nombre_asesor" class="form-control" name="nombre_asesor" style="width:100%;" value="<?php if(isset($data[0]->nombre_asesor)) {print_r($data[0]->nombre_asesor);} ?>">
                                <div id="hora_entrada_error" class="invalid-feedback"></div>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4" align="center">
                                <?php if (isset($data[0]->url_firma_asesor)): ?>
                                    <?php if ($data[0]->url_firma_asesor == ""): ?>
                                        <a id="firma_asesor" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a id="firma_asesor" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                        Firmar
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4"></div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <label for="" style="font-size: 12px;font-weight:bold;">
                                    NOMBRE Y FIRMA DE ENCARGADO DE PREVIAS
                                </label>
                                <br>
                                
                                <img class="marcoImg" src="<?php if(isset($data[0]->url_firma_previas)) echo base_url().$data[0]->url_firma_previas;?>" id="firma_previas_img" alt="" style="height:2cm;width:4cm;">
                                <input type="hidden" id="ruta_firma_previas" name="ruta_firma_previas" value="<?php if(isset($data[0]->firma_previas)) echo $firma_previas;?>" >
                                <input type="hidden" id="url_firma_previas" name="url_firma_previas" value="<?php if(isset($data[0]->url_firma_previas)) echo $data[0]->url_firma_previas;?>" >

                                <br>

                                <input type="text" id="nombre_previas" class="form-control" name="nombre_previas" style="width:100%;" value="<?php if(isset($data[0]->nombre_previas)) {print_r($data[0]->nombre_previas);} ?>">
                                <div id="hora_entrada_error" class="invalid-feedback"></div>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4" align="center">
                                <?php if (isset($data[0]->url_firma_previas)): ?>
                                    <?php if ($data[0]->url_firma_previas == ""): ?>
                                        <a id="firma_previas" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_previas" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a id="firma_previas" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_previas" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                        Firmar
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <label for="" style="font-size: 13px;">COMENTARIOS:</label>
                        <textarea type="text" class="form-control" id="comentarios" name="comentarios" rows="3"><?php if(isset($data[0]->comentarios)) {print_r($data[0]->comentarios);} ?></textarea>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12" style="background-color: #666699;">
                        <br>
                    </div>
                </div>

                <br>
                <h4 style="text-align: center;font-weight: bold;color:black;">
                    DOCUMENTACIÓN REQUERIDA
                </h4>

                <div class="row">
                    <div class="col-md-5 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Póliza
                                    <input type="hidden" name="poliza" id="poliza" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->poliza : ''?>">
                                    <div id="poliza_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck1" name="poliza_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->poliza == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck1" name="poliza_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->poliza == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <!--<input type="radio" style="transform: scale(1.5);" class="tb5_ck1" name="poliza_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->poliza == "NA") echo "checked"; } ?>>-->
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Carta Factura
                                    <input type="hidden" name="carta_factura" id="carta_factura" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->carta_factura : ''?>">
                                    <div id="carta_factura_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck2" name="carta_factura_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->carta_factura == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck2" name="carta_factura_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->carta_factura == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <!--<input type="radio" style="transform: scale(1.5);" class="tb5_ck2" name="carta_factura_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->carta_factura == "NA") echo "checked"; } ?>>-->
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Seguro
                                    <input type="hidden" name="seguro" id="seguro" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->seguro : ''?>">
                                    <div id="seguro_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck3" name="seguro_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->seguro == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck3" name="seguro_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->seguro == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck3" name="seguro_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->seguro == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Permiso
                                    <input type="hidden" name="permiso" id="permiso" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->permiso : ''?>">
                                    <div id="permiso_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck4" name="permisos_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->permiso == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck4" name="permisos_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->permiso == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck4" name="permisos_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->permiso == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-md-5 table-responsive">
                        <table class="table table-bordered" style="width: 100%;">
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    SI
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NO
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    NA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Placas
                                    <input type="hidden" name="placas" id="placas" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->placas : ''?>">
                                    <div id="placas_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck5" name="placas_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->placas == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck5" name="placas_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->placas == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck5" name="placas_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->placas == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Verificación
                                    <input type="hidden" name="verificacion" id="verificacion" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->verificacion : ''?>">
                                    <div id="verificacion_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck6" name="verificacion_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->verificacion == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck6" name="verificacion_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->verificacion == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck6" name="verificacion_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->verificacion == "NA") echo "checked"; } ?>>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 73%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                    Papelería
                                    <input type="hidden" name="papeleria" id="papeleria" value="<?php echo isset($data[0]->documentacion ) ? $data[0]->documentacion->papeleria : ''?>">
                                    <div id="papeleria_error" class="invalid-feedback"></div>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck7" name="papeleria_ck" value="SI" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->papeleria == "SI") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <input type="radio" style="transform: scale(1.5);" class="tb5_ck7" name="papeleria_ck" value="NO" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->papeleria == "NO") echo "checked"; } ?>>
                                </td>
                                <td style="width: 9%; vertical-align: middle;" align="center">
                                    <!--<input type="radio" style="transform: scale(1.5);" class="tb5_ck7" name="papeleria_ck" value="NA" <?php if(isset($data[0]->documentacion )) {if( $data[0]->documentacion->papeleria == "NA") echo "checked"; } ?>>-->
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12" style="background-color: #666699;">
                        <br>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <h4 style="text-align: center;font-weight: bold;color:black;">
                            AUTORIZACIÓN DE ENTREGA FUERA DE PROCESO
                        </h4>
                        <div class="row">
                            <div class="col-md-12" align="center">
                                <table class="table table-bordered" style="width: 100%;">
                                    <tr>
                                        <td style="width: 70%; vertical-align: middle;font-size:12px;font-weight:bold;">
                                            REQUIERE AUTORIZACIÓN:
                                            <input type="hidden" name="autorizacion" id="autorizacion" value="<?php echo isset($data[0]->autorizacion ) ? $data[0]->autorizacion : ''?>">
                                            <div id="autorizacion_error" class="invalid-feedback"></div>
                                        </td>
                                        <td style="width: 15%; vertical-align: middle;" align="left">
                                            SI&nbsp;
                                            <input type="radio" style="transform: scale(1.5);" class="tb6_ck1" name="autorizacion_ck" value="SI" <?php if(isset($data[0]->autorizacion )) {if( $data[0]->autorizacion == "SI") echo "checked"; } ?>>
                                        </td>
                                        <td style="width: 15%; vertical-align: middle;" align="left">
                                            NO&nbsp;
                                            <input type="radio" style="transform: scale(1.5);" class="tb6_ck1" name="autorizacion_ck" value="NO" <?php if(isset($data[0]->autorizacion )) {if( $data[0]->autorizacion == "NO") echo "checked"; } ?>>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <br>
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <label for="" style="font-size: 12px;font-weight:bold;"> GERENTE GENERAL</label>
                                        <br>
                                        
                                        <img class="marcoImg" src="<?php if(isset($data[0]->url_firma_gerente_general)) echo base_url().$data[0]->url_firma_gerente_general;?>" id="firma_gerente_general_img" alt="" style="height:2cm;width:4cm;">
                                        <input type="hidden" id="ruta_firma_gerente_general" name="ruta_firma_gerente_general" value="<?php if(isset($data[0]->firma_gerente_general)) echo $firma_gerente_general;?>" >
                                        <input type="hidden" id="url_firma_gerente_general" name="url_firma_gerente_general" value="<?php if(isset($data[0]->url_firma_gerente_general)) echo $data[0]->url_firma_gerente_general;?>" >

                                        <br>

                                        <input type="text" id="nombre_gerente_general" class="form-control" name="nombre_gerente_general" style="width:100%;" value="<?php if(isset($data[0]->nombre_gerente_general)) {print_r($data[0]->nombre_gerente_general);} ?>">
                                        <div id="hora_entrada_error" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <?php if (isset($data[0]->url_firma_gerente_general)): ?>
                                            <?php if ($data[0]->url_firma_gerente_general == ""): ?>
                                                <a id="firma_gerente_general" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_gerente_general" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                                    Firmar
                                                </a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <a id="firma_gerente_general" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_gerente_general" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                                Firmar
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <br>
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <label for="" style="font-size: 12px;font-weight:bold;"> LOGÍSTICA</label>
                                        <br>
                                        
                                        <img class="marcoImg" src="<?php if(isset($data[0]->url_firma_logistica_2)) echo base_url().$data[0]->url_firma_logistica_2;?>" id="firma_logistica_2_img" alt="" style="height:2cm;width:4cm;">
                                        <input type="hidden" id="ruta_firma_logistica_2" name="ruta_firma_logistica_2" value="<?php if(isset($data[0]->firma_logistica_2)) echo $firma_logistica_2;?>" >
                                        <input type="hidden" id="url_firma_logistica_2" name="url_firma_logistica_2" value="<?php if(isset($data[0]->url_firma_logistica_2)) echo $data[0]->url_firma_logistica_2;?>" >

                                        <br>

                                        <input type="text" id="nombre_logistica_2" class="form-control" name="nombre_logistica_2" style="width:100%;" value="<?php if(isset($data[0]->nombre_logistica_2)) {print_r($data[0]->nombre_logistica_2);} ?>">
                                        <div id="hora_entrada_error" class="invalid-feedback"></div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <?php if (isset($data[0]->url_firma_logistica_2)): ?>
                                            <?php if ($data[0]->url_firma_logistica_2 == ""): ?>
                                                <a id="firma_logistica_2" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_logistica_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                                    Firmar
                                                </a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <a id="firma_logistica_2" class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_logistica_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                                Firmar
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <br><br>
                        <label for="" style="font-size: 15px;font-weight:bold;">MOTIVO:</label>
                        <textarea type="text" class="form-control" id="motivo" name="motivo" rows="3"><?php if(isset($data[0]->motivo)) {print_r($data[0]->motivo);} ?></textarea>
                    </div>
                </div>

            	<div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button type="button" id="editar_registro" class="btn btn-success"> Actualizar </button>
                        @else
                            <button type="button" id="guardar_registro" class="btn btn-success"> Guardar </button>
                        @endif

                        <input type="hidden" id="consecutivo" value="<?php if(isset($consecutivo->id)) {echo $consecutivo->id;}else{echo '0';} ?>">
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title" id="firmaDigitalLabel"></h4>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="firma_actual" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script src="{{ base_url('js/autos/diagrama_vehiculo.js') }}"></script>
    <script src="{{ base_url('js/autos/checks_checklist.js') }}"></script>
    <script src="{{ base_url('js/autos/firmas_checklist.js') }}"></script>

    <script>
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");

            //Guardamos la imagen del diagrama
            guardar_diagrama();

            ajax.post('api/seminuevos/control-documentos/documentacion', procesarRegistro(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                //var alerta = (headers.status != 200) ? "warning" : "success";

                utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                    return window.location.href = base_url + 'autos/Recepcion/index';
                })
            })
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            var id = $("#recepcion_unidad").data('id');

            ajax.put('api/seminuevos/control-documentos/documentacion/'+id, procesarRegistro(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                    return window.location.href = base_url + 'autos/Recepcion/index';
                })
            })
        });

        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let form = $('#recepcion_unidad').serializeArray();
            let newArray = {
                recepcion_unidades_id : $("#recepcion_unidad").data('id'),

                usuario_recibe : document.getElementById("usuario_recibe").value,
                fecha_recepcion : document.getElementById("fecha_recepcion").value,
                estado : document.getElementById("estado").value,
                marca_id : document.getElementById("marca_id").value,
                modelo_id : document.getElementById("modelo_id").value,
            };

            return newArray;
        }

        //Enviamos la imagen para generarla y guardarla
function guardar_diagrama() {
    //Validamso que no se haya guardado ya una imagen
    if ($("#danosMarcas").val() == "") {
        var canvas = document.getElementById('canvas_3');
        var dataURL = canvas.toDataURL('image/png');
        //$("#danosMarcas").val(dataURL);

        $.ajax({
            url: base_url+"autos/Checklist/firmas",
            method: 'post',
            data: {
                base_64: dataURL,
                destino: "diagramas",
            },
            success:function(resp){
                console.log("url diagrama: "+resp);

                if (resp.indexOf("handler           </p>")<1) {
                    $("#danosMarcas").val(resp);
                }

            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}

    </script>
@endsection