@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm" action="">
            <div class="row">
                <div class="col-sm-3">
                    <label for="">Usuario</label>
                    {{ $id_usuario }}
                </div>
                <div class="col-sm-3">
                    <label for="">Fecha inicio</label>
                    {{ $fecha_inicio }}
                    <span class="error error_fecha_inicio"></span>
                </div>
                <div class="col-sm-3">
                    <label for="">Fecha fin</label>
                    {{ $fecha_fin }}
                    <span class="error error_fecha_fin"></span>
                </div>
                <div class="col-sm-3">
                    <label for="">Estatus</label>
                    {{ $estatus }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3" style="margin-top: 30px;">
                    <button type="button" id="buscar" name="buscar" class="btn btn-default">Buscar</button>
                </div>
            </div>
        </form>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl" width="100%" class="table table-bordered" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Fecha creación</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>Serie</th>
                                <th>Cliente</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Línea</th>
                                <th>Tipo vehículo</th>
                                <th>Año</th>
                                <th>Comentario</th>
                                <th width="30%">Estatus</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($historial) > 0)
                                @foreach ($historial as $c => $value)
                                    <tr>
                                        <td>{{ formatDate($value->created_at) }}</td>
                                        <td>{{ $value->fecha }}</td>
                                        <td>{{ $value->hora }}</td>
                                        <td>{{ $value->vin }}</td>
                                        <td>{{ $value->nombre.' '.$value->apellido }}</td>
                                        <td>{{ $value->marca }}</td>
                                        <td>{{ $value->modelo }}</td>
                                        <td>{{ $value->linea }}</td>
                                        <td>{{ $value->tipo_vehiculo }}</td>
                                        <td>{{ $value->anio }}</td>
                                        <td>{{ $value->observaciones }}</td>
                                        <td>{{ $value->estatus }}</td>
                                        <td>{{ $value->nombre_usuario . ' ' . $value->ap_usuario . ' ', $value->am_usuario }}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="13">Aún no se han registrado comentarios... </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
        inicializar_tabla_local();

        function inicializar_tabla_local() {
            $('#tbl').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
        }
        $("#buscar").on('click', function() {
            buscarInformacion()
        })

        function buscarInformacion() {
            var url = site_url + "/proactivo/buscar_historial/";
            $('.error').empty();
            if ($("#fecha_inicio").val() != '' && $("#fecha_fin").val() == '') {
                $(".error_fecha_fin").empty().append('El campo fecha fin es requerido').css('color', 'red');
                return;
            }
            if ($("#fecha_fin").val() != '' && $("#fecha_inicio").val() == '') {
                $(".error_fecha_inicio").empty().append('El campo fecha inicio es requerido').css('color', 'red');
                return;
            }
            ajaxLoad(url, {
                "id_usuario": $("#id_usuario").val(),
                "estatus_id": $("#estatus_id").val(),
                "fecha_inicio": $("#fecha_inicio").val(),
                "fecha_fin": $("#fecha_fin").val(),
            }, "tabla-items", "POST", function() {
                inicializar_tabla_local();
            });
        }
    </script>
@endsection
