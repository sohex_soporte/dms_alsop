@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : "" }}</li>
    </ol>
    <div class="row">
        <div class="col-md-12 text-right">
        <a class="btn btn-primary" href="{{ base_url('refacciones/salidas/ventasMostrador') }}">
            <i class="fa fa-list"></i> Regresar</a>
        </div>
        <div class="col-md-12">
            <form id="frm-producto">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "descripcion", "Descripcion", isset($producto->descripcion) ? $producto->descripcion : '', true); ?>
                        <input type="hidden" name="producto_id" id="producto_id" value="{{ $producto->id}}">
                        <input type="hidden" name="no_identificacion" id="no_identificacion" value="{{ $producto->no_identificacion}}">
                    </div>
                    <div class="col-md-3">
                        <?php echo renderInputText("text", "unidad", "Unidad", isset($producto->unidad) ? $producto->unidad : '', true); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo renderInputText("text", "no_identificacion", "No de producto", isset($producto->no_identificacion) ? $producto->no_identificacion : '',true); ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <?php echo renderInputText("number", "existencia", "Existencia total", isset($producto->cantidad) ? $producto->cantidad : '',true); ?>
                        <input type="hidden" id="status" name="status" value="2">
                        <input type="hidden" id="almacen_id" name="almacen_id" value="{{ isset($producto->almacen_id) ? $producto->almacen_id : '' }}">
                    </div>
                    @foreach ($producto->producto_almacen as $item)
                        <div class="col-md-4">
                            <?php echo renderInputText("number", "existencia", "Existencia ". $item->almacenes->nombre, isset( $item->cantidad) ?  $item->cantidad : '',true); ?>
                        </div>
                    @endforeach
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "nombre_taller", "Taller", isset($producto->taller) ? $producto->taller->nombre : '', true); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "ubicacion", "Ubicacion", isset($producto->taller) ? $producto->taller->ubicacion : '', true); ?>
                        <input type="hidden" value="2" name="tipo_venta_id" id="tipo_venta_id">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="select">Tipo cliente</label>
                            <select name="tipo_cliente_id" class="form-control" id="tipo_cliente_id">
                                <option value=""> Seleccionar</option>
                                @foreach ($cat_tipo_clientes as $tipo)
                                    <option value="{{ $tipo->id}}"> {{ $tipo->nombre}} </option>
                                @endforeach
                            </select>
                            <div id='tipo_cliente_id_error' class='invalid-feedback'></div>
                        </div>
                    </div>
                </div>
                <div class="row form-registrar-cliente-esporadico">
                    <div class="col-md-8">
                        <?php echo renderInputText("text", "nombre", "Nombre de cliente", '', false); ?>
                        <input type="hidden" id="hidden-dinamic-client-id" class="hidden-dinamic-client-id">
                    </div>
                    <div class="col-md-4 pt-4">
                        <button type="button" id="guardar_cliente" class="btn btn-primary">
                            <i class="fas fa-plus"></i> Guardar cliente
                        </button>
                    </div>
                    <hr>
                </div>
                <div class="row cliente-registrado">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="select">Cliente</label>
                            <select name="cliente_id" class="form-control" id="cliente_id">
                                <option value=""> Seleccionar precio</option>
                                @foreach ($cat_clientes as $cliente)
                                    <option value="{{ $cliente->id}}"> {{ $cliente->nombre}} </option>
                                @endforeach
                            </select>

                            <div id='cliente_id_error' class='invalid-feedback'></div>
                        </div>
                    </div>
                    <hr>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("number", "precio_factura", "Valor unitario", isset($producto->valor_unitario) ? $producto->valor_unitario : '', true); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("number", "cantidad", "Cantidad",0); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="select">Aplicar precio</label>
                    <select name="precio_id" class="form-control" id="precio_id">
                        <option value=""> Seleccionar precio</option>
                        @foreach ($cat_precios as $precio)
                            @if (isset($producto->precio_id) && $producto->precio_id == $precio->id)
                                <option selected value="{{ $precio->id}}"> {{ $precio->precio_publico}} %</option>
                            @else
                                <option value="{{ $precio->id}}"> {{ $precio->precio_publico}} %</option>
                            @endif
                        @endforeach
                    </select>
                    <div id='precio_id_error' class='invalid-feedback'></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "precio_publico", "Precio al publico", isset($producto->rel_precio->precio_publico) ? '$ '. porcentajeProducto($producto->rel_precio->precio_publico, $producto->valor_unitario)  : '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "precio_mayoreo", "Precio mayoreo", isset($producto->rel_precio->precio_mayoreo) ?  '$ '. porcentajeProducto($producto->rel_precio->precio_mayoreo, $producto->valor_unitario) : '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "precio_interno", "Precio interno", isset($producto->rel_precio->precio_interno) ? '$ '. porcentajeProducto($producto->rel_precio->precio_interno, $producto->valor_unitario) : '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "precio_taller", "Precio taller", isset($producto->rel_precio->precio_taller) ? '$ '. porcentajeProducto($producto->rel_precio->precio_taller, $producto->valor_unitario) : '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "precio_otras_distribuidoras", "Precio otras distribuidoras", isset($producto->rel_precio->precio_otras_distribuidoras) ? '$ '. porcentajeProducto($producto->rel_precio->precio_otras_distribuidoras, $producto->valor_unitario) : '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "impuesto", "impuesto", isset($producto->rel_precio->impuesto) ? '$ '. porcentajeProducto($producto->rel_precio->impuesto, $producto->valor_unitario) : '', true); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button class="btn btn-primary col-md-12" id="agregar_vender" type="button">  <i class="fas fa-shopping-cart"></i>  Continuar </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        
        
        $(".form-registrar-cliente-esporadico").hide();
        $(".cliente-registrado").hide();
        $("#tipo_cliente_id").on("change", function() {
            if($("#tipo_cliente_id").val() == ''){
                $(".form-registrar-cliente-esporadico").hide();
                $(".cliente-registrado").hide();
            }else if($("#tipo_cliente_id").val() == 1){
                utils.displayWarningDialog("¿Desea ir a Registrar el cliente?", null, function(data) {
                    if(data.value){
                        return window.location.href = base_url + 'catalogos/clientesController/crear';
                    }
                }, true);
            }else if($("#tipo_cliente_id").val() == 2){
                $(".form-registrar-cliente-esporadico").hide();
                $(".cliente-registrado").show();
            }else{
                $(".form-registrar-cliente-esporadico").hide();
                $(".cliente-registrado").hide();
            }
        });

        $("#guardar_cliente").on("click", function() {
            $(".invalid-feedback").html("");
            ajax.post(`api/clientes`, {
                nombre:document.getElementById("nombre").value,
            }, function(response, headers){
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                utils.displayWarningDialog("Cliente registrado", "success", function(data) {
                    $('.hidden-dinamic-client-id').val(response.id);
                    $("#guardar_cliente").attr("disabled",true);
                    $("#nombre").attr("disabled",true);
                })
            });
        });



        $("#agregar_vender").on("click", function() {
            if($("#cantidad").val() == 0 ){
                return utils.displayWarningDialog("Indicar numero de piezas", "warning", function(data) {})
            }
            if(document.getElementById("cliente_id").value == '' && $('.hidden-dinamic-client-id').val() == ''){
                $("#tipo_cliente_id_error").text('seleccionar tipo de cliente').css('display','block');
                $('html,body').animate({
                    scrollTop: $('#tipo_cliente_id_error').offset().top
                }, 1000);
                return false;
            }

            let form =  $("#frm-producto").serializeArray();
            $(".invalid-feedback").html("");
            let dinamic_client_id = document.getElementById("cliente_id").value !== '' ? document.getElementById("cliente_id").value : $('.hidden-dinamic-client-id').val();
            ajax.post(`api/ventas`, {
                cliente_id:dinamic_client_id,
                estatus_id: document.getElementById("status").value,
                venta_total:0,
                valor_unitario:$("#precio_factura").val(),
                tipo_venta_id:document.getElementById("tipo_venta_id").value,
                precio_id:document.getElementById("precio_id").value,
                producto_id:document.getElementById("producto_id").value,
                cantidad:document.getElementById("cantidad").value,
                almacen_id:document.getElementById("almacen_id").value
            }, function(response, headers){
                
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                utils.displayWarningDialog("Procesando compra", "success", function(data) {
                    if(response.folio_id){
                        return window.location.href = base_url + `refacciones/salidas/detalleVenta/`+response.folio_id;
                    }
                })
            });
        });

        $("#cantidad").on('keyup', function() {
            let cantidad = $("#cantidad").val();
            let almacen_id = $("#almacen_id").val();
            let producto_id = $("#producto_id").val();
            validar_cantidad_productos(cantidad, almacen_id,producto_id)
            
        });

        function validar_cantidad_productos(cantidad, almacen_id,producto_id) {
            ajax.post(`api/productos/validar-descuento-producto`, {
                cantidad,
                 almacen_id,
                 producto_id
            }, function(response, headers) {
                if (headers.status == 200) {
                    $(".validation_error").html('');
                    $("#agregar_vender").attr('disabled', false);
                }

                if (headers.status == 400) {
                    $("#agregar_vender").attr('disabled', true);
                }
            })
        }


        $("#precio_id").on('change', function() {
            let id_precio = $("#precio_id").val();
            let precio_factura = $("#precio_factura").val();
            $.ajax({
                type: 'POST',
                url: base_url+"refacciones/productos/ajax_aplicar_precio",
                data:{
                precio_factura,
                id_precio
                },
                dataType: "json",
                success: function (response) {
                    $("#precio_publico").val("$ "+response.precio_publico);
                    $("#precio_mayoreo").val("$ "+response.precio_mayoreo);
                    $("#precio_interno").val("$ "+response.precio_interno);
                    $("#precio_taller").val("$ "+response.precio_taller);
                    $("#precio_otras_distribuidoras").val("$ "+response.precio_otras_distribuidoras);
                    $("#impuesto").val("$ "+response.impuesto);
                }
            });
        });
    });


</script>
@endsection