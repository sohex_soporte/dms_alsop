@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>
        <div class="row mb-4">
            <div class="col-md-10"></div>
            <div class="col-md-2 text-right">
                <a href="{{ base_url('refacciones/factura/subirFactura') }}" class="btn btn-primary">
                    <i class="fa fa-plus"></i> Subir factura
                </a>
                <input type="hidden" name="user_id" id="user_id"
                    value="{{ !empty($this->session->userdata('id')) ? $this->session->userdata('id') : '' }}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered" id="tabla_facturas" width="100%" cellspacing="0">
                </table>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // $("#tabla-facturas").DataTable({});
        var tabla_facturas = $('#tabla_facturas').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: PATH_API + "api/orden-compra/facturas-listado",
                type: 'GET',
            },
            columns: [{
                    title: "#",
                    data: 'id',
                },
                {
                    title: "Folio",
                    data: 'folio',
                },
                {
                    title: "Fecha",
                    data: 'fecha',
                },
                {
                    title: "Total",
                    data: 'total',
                },
                {
                    title: "Sub total",
                    data: 'subtotal',
                },
                {
                    title: "-",
                    render: function(data, type, row) {
                        btn_cancelar = (row.estatus_id && row.estatus_id !== 2) ?
                            '<button  class="btn btn-danger btn_cancelar mr-2" onclick="cancelar(' + row
                            .id + ')"><i class="fa fa-trash"></i> </button>' : '';
                        btn_descargar = '<a target="_blank" class="btn btn-success mr-2" href="' +
                            PATH_API + 'api/facturas/descargar/' + row.file_name +
                            '"><i class="fas fa-download"></i> </a>';
                        btn_detalle = '<a class="btn btn-success mr-2" href="' + base_url +
                            'refacciones/factura/vistafactura/' + row.id +
                            '"><i class="fa fa-list"></i> </a>';

                        return btn_descargar + btn_detalle + btn_cancelar;
                    }
                }


            ]
        });

        const cancelar = (factura_id) => {
            utils.displayWarningDialog("Desea cancelar la factura??", "warning", function(data) {
                if (data.value) {
                    ajax.post(`api/orden-compra/cancelar-factura`, {
                        factura_id,
                        user_id: $("#user_id").val()
                    }, function(response, headers) {
                        if (headers.status == 400) {
                            return utils.displayWarningDialog("Esta factura ya cuenta con ventas!!",
                                "warning",
                                function(data) {});
                        }

                        if (headers.status == 200) {
                            return utils.displayWarningDialog("Factura cancelada", "warning",
                                function(data) {
                                    location.reload(true);
                                });
                        }
                    })

                }
            }, true)
        }
    </script>

@endsection
