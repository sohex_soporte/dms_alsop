@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h2 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h2>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item"> Rerporte de ventas </li>
        </ol>

        <div class="row">
            <div class="col-md-12 text-right">
                <a class="btn btn-primary mb-4" href="{{ base_url('refacciones/salidas/listadoVentas') }}">Regresar</a>
            </div>
            
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tbl" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No. de pieza</th>
                                <th>Producto </th>
                                <th>Venta</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reporte as $item)
                                <tr>
                                    <td>{{ $item->no_identificacion }}</td>
                                    <td>{{ $item->descripcion }}</td>
                                    <td>$ {{ $item->venta_total }}</td>
                                    <td> {{ $item->cantidad }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No. de pieza</th>
                                <th>Producto </th>
                                <th>Venta</th>
                                <th>Cantidad</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#tbl').DataTable({
            language: {
                url: PATH_LANGUAGE
            }
        });
    </script>
@endsection
