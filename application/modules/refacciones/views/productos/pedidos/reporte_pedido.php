<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Reporte de piezas pedidas</title>
	<style>
		.contenedor {
			width: 100%;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}
		td{
			font-size:11px !important;
			text-align: right !important;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		table,
		th,
		td {
			border: 1px solid #233a74;
			margin-bottom: 12px
		}

	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12">
			Mexico D.F. <?php echo obtenerFechaEnLetra(date('yy-m-d')); ?>
		</div>
		<div class="col-12">
			<h2>Reporte de piezas pedidas</h2>
		</div>
	</div>
	<div class="contenedor">
		<div class="col-12">
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">#</th>
						<th style="font-size: 11px" scope="col">Producto</th>
						<th style="font-size: 11px" scope="col">No. dentificacion</th>
						<th style="font-size: 11px" scope="col">Unidad</th>
						<th style="font-size: 11px" scope="col">Valor unitario</th>
                        <th style="font-size: 11px" scope="col">Pedidas</th>
                        <th style="font-size: 11px" scope="col">Total</th>
                        <th style="font-size: 11px" scope="col">Fecha</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($listado as $key => $item) { ?>
					<tr>
						<td><?php echo  $item->id; ?></td>
						<td><?php echo  $item->descripcion; ?></td>
						<td><?php echo  $item->no_identificacion; ?></td>
						<td><?php echo  $item->unidad; ?></td>
						<td><?php echo  '$'. number_format($item->valor_unitario); ?></td>
                        <td><?php echo  $item->cantidad_solicitada; ?></td>
                        <td><?php echo  $item->cantidad_solicitada * $item->valor_unitario; ?></td>
						<td>
							<?php echo date('d-m-yy', strtotime($item->created_at)) ?>
						</td>
					</tr>
					<?php  } ?>
				</tbody>
			</table>
		</div>
	</div>
</body>

</html>
