@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : '' }}</li>
    </ol>
    <h4>Estatus de venta : <?php echo isset($folio) && isset($folio[0]->re_ventas_estatus) ? $folio[0]->re_ventas_estatus->estatus_venta->nombre : ''; ?></h4>
    <div class="row mt-2">
        <div class="col-md-4">
            <?php echo renderInputText('text', 'folio', 'Folio', isset($folio) ? $folio[0]->folio->folio  : '', true); ?>
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->folio_id : '' }}" id="folio_id">
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->id : '' }}" id="venta_id">
            <input type="hidden" value="{{ isset($folio[0]->re_ventas_estatus) ? $folio[0]->re_ventas_estatus->estatus_venta->id : '' }}" id="estatus_venta_id">
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->almacen_id : '' }}" name="almacen_id" id="almacen_id">
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'venta_total', 'Total', isset($folio[0]->venta_total) ? $folio[0]->venta_total  : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'cliente', 'Cliente', isset($folio) && isset($folio[0]->cliente) ? $folio[0]->cliente->nombre : '', true); ?>
            <input type="hidden" value="{{ isset($folio) && $folio[0]->cliente ? $folio[0]->cliente->id : '' }}" id="cliente_id" name="cliente_id">
        </div>
    </div>
    <h4>Vehiculo</h4>
    <div class="row">
        @foreach ($info_vehiculo as $inputKey => $itemValue)
            <div class="col-md-4">
                <?php echo renderInputText('text', $inputKey, $inputKey, isset($itemValue) ? $itemValue  : '', true); ?>
            </div>
        @endforeach
    </div>
    <hr />
    <div  class="row mb-4 mt-4 ">
        <div class="col-md-12 text-right">
            <input type="hidden"value="{{$folio[0]->re_ventas_estatus->estatus_ventas_id}}" id="statusorden">
            <a class="btn btn-primary" href="{{ site_url('refacciones/salidas/otrasSalidas') }}"> <i class="fas fa-long-arrow-alt-left"></i> Regresar </a>      
        </div>
    </div>
    <br>
    <h3>Productos seleccionados</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_carrito" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No Pieza</th>
                            <th>Descripcion</th>
                            <th>Total</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>No Pieza</th>
                            <th>Descripcion</th>
                            <th>Total</th>
                            <th>Valor unitario</th>
                            <th>Cantidad</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <h3>Productos disponibles</h3>
	<hr>
    <div class="row">
        <div class="col-md-3">
            <?php echo renderInputText("text", "no_identificacion", "No. de pieza", '', false); ?>
        </div>
        <div class="col-md-7">
            <?php echo renderInputText("text", "descripcion", "Descripción", '', false); ?>
        </div>
        <div class="col-md-2 mt-4">
            <button onclick="buscarproducto()" class="btn btn-primary"> 
                <i class="fa fa-search"></i> Buscar
            </button>
        </div>
    </div>
    <br>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>No Pieza</th>
							<th>Descripcion</th>
							<th>Valor unitario</th>
							<th>Ubicación producto</th>
							<th>Existencia <br />total producto</th>
							<th>Existencia <br />almacen principal</th>
							<th>-</th>
						</tr>
					</thead>
					
					<tfoot>
						<tr>
						<tr>
							<th>#</th>
							<th>No Pieza</th>
							<th>Descripcion</th>
							<th>Valor unitario</th>
							<th>Ubicación producto</th>
							<th>Existencia <br />total producto</th>
							<th>Existencia <br />almacen principal</th>
							<th>-</th>
						</tr>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
   
    let tbl_productos=$('#tbl_productos').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: PATH_API + "api/productos/listadoStock",
            type: 'GET',
        },
        columns: [{
                title: "#",
                data: 'id',
            },
            {
                title: "No. de pieza",
                data: 'no_identificacion',
            },
            {
                title: "Descripcion",
                data: 'descripcion',
            },
            {
                title: "Precio unitario",
                data: 'valor_unitario',
            },
            {
                title: "Unidad",
                data: 'unidad',
            },
            {
                title: "Cantidad actual",
                data: 'cantidad_actual',
            },
            {
                title: "Almacen primario",
                data: 'cantidad_almacen_primario'
            },
            {
                'data': function(data) {
                    if(utils.isDefined(data.unidad) && data.cantidad_almacen_primario > 0){
                        return "<button onclick='modalDetalle(this)' data-toggle='modal' data-product_name="+data.descripcion+" data-product_id="+data.id+" data-no_identificacion="+data.no_identificacion+" data-cantidad_almacen_primario="+data.cantidad_almacen_primario+" data-valor_unitario="+data.valor_unitario+" class='btn btn-success'><i class='fas fa-shopping-cart'></i></button>";
                    }else{
                        return '--';

                    }
                }
            }
        ]
    })

    let estatus_venta_id = $('#estatus_venta_id').val();
    var tabla_carrito = $('#tbl_carrito').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/salidas/ajax_detalle_venta_mpm",
            type: 'POST',
            data: {
                id: function() {
                    return $('#venta_id').val()
                }
            }
        },
        columns: [{
                'data': 'id'
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.no_identificacion) && data.no_identificacion ? data
                        .no_identificacion : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.descripcion_producto) && data.descripcion_producto ?
                        data
                        .descripcion_producto : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.venta_total) ? "$ " + data.venta_total : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.valor_unitario) ? "$ " + data.valor_unitario : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.cantidad) && data.cantidad ? data.cantidad : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.unidad) && data.unidad ? data.unidad : '--'
                }
            },
            {
                'data': function(data) {
                    let id_index_requisicion = data.id_index_requisicion ? data.id_index_requisicion : 0;
                    if(utils.isDefined(data.unidad) && data.unidad){
                        return "<button type='button' class='btn-borrar btn btn-primary' data-id_index_requisicion=" + data.id_index_requisicion + " data-id=" + data.id + "><i class='fas fa-trash'></i></button>";
                    }else{
                        return '--';

                    }
                }
            }
        ]
    });

    var tbl_reporte = $('#tbl_reporte').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/salidas/refacciones_operacion",
            type: 'POST',
            data: {
                venta_id: function() {
                    return $('#venta_id').val()
                }
            }
        },
        columns: [{
                'data': 'id'
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.no_identificacion) && data.no_identificacion ? data
                        .no_identificacion : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.descripcion) && data.descripcion ? data.descripcion : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.valor_unitario) && data.valor_unitario ? data.valor_unitario : null
                }
            }
        ]
    });
    
    $("#tbl_carrito").on("click", ".btn-borrar", function() {
		var id = $(this).data('id')
		var id_index_requisicion = $(this).data('id_index_requisicion')
		borrar(id, id_index_requisicion)
	});
    const obtenerpiezas = (no_identificacion = '')=>{
            console.log(no_identificacion);
    }


    realizarVenta();

    function realizarVenta() {
        if(document.getElementById('statusorden').value == 2){
            return false;
        }
        // return false;
        $.isLoading({
            text: "Realizando proceso de venta ...."
        });
        let id_venta = $('#venta_id').val();
        let concepto = 'Service excelent - Piezas';
        
        ajax.put(`api/ventas/mpmdescontar/${id_venta}`, {
            folio_id: $('#folio_id').val(),
            venta_total: $('#venta_total').val(),
            tipo_forma_pago_id: 1,
            concepto: concepto,
            tipo_pago_id: 1,
            cliente_id: $('#cliente_id').val(),
            plazo_credito_id: 14,
            enganche: 0,
            tasa_interes: 0,//$('#tasa_interes').val(),
            usuario_gestor_id: $('#usuario_gestor_id').val()
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                // toastr.info('Procesando')
                return this.addReVentasEstatus(id_venta); //Cambia estatus a vendido
            } else {
                $.isLoading("hide");

            }
        })

    }

    function modalDetalle(_this) {
		$("#producto_id").val($(_this).data('product_id'));
		$("#valor_unitario").val($(_this).data('valor_unitario'));
		$("#valor_unitario_nuevo").val($(_this).data('valor_unitario'));
		$("#no_identificacion").val($(_this).data('no_identificacion'));
		$("#cantidad_almacen_primario").val($(_this).data('cantidad_almacen_primario'));
		var producto_name = $(_this).data('product_name');
		$("#title_modal").text(producto_name);
		$("#modal-producto-detalle").modal('show');
	}

    function agregarproducto(id) {
		toastr.info("Añadiendo producto al carro de compras");
		$.isLoading({
			text: "Añadiendo producto al carro de compras...."
		});

		let folio_id = $('#folio_id').val();
		let producto_id = $("#producto_id").val();
		let cliente_id = $("#cliente_id").val();
		let cantidad = $("#cantidad").val();
		let venta_id = $("#venta_id").val();
        
        let precio_id = $("#precio_id").val();
        let valor_unitario = $("#valor_unitario").val();
        
        let valor_unitario_nuevo = $("#valor_unitario_nuevo").val();
        if(valor_unitario !== valor_unitario_nuevo){
            valor_unitario = valor_unitario_nuevo;
            // precio_id = 1;
        }

        let precio_manual = $("#precio_manual").val();
        if(precio_manual !== ''){
            precio_id = 1; //default value
        }
        
        
		let data = {
			folio_id,
			producto_id,
			valor_unitario,
			precio_id,
			cliente_id,
			cantidad,
			venta_id,
            precio_manual,
            'estatus_producto_orden':'A'
		}
        

		ajax.post(`api/venta-producto`, data, function(response, headers) {
			if (headers.status == 201 || headers.status == 200) {
				$("#modal-producto-detalle").modal('hide');
				tabla_carrito.ajax.reload();
				$.ajax({
					type: "GET",
					url: base_url + "refacciones/salidas/ajax_calcular_venta_total/" + $('#folio_id').val(),
					dataType: "json",
					success: function(response) {
						let venta = parseInt(response.venta_total);
						let titulo = "Producto añadido al carro de compras correctamente"
						$('#venta_total').val(venta);
						utils.displayWarningDialog(titulo, 'success', function(result) {
							// window.location.reload();
                            tbl_productos.ajax.reload();
						});
                        descontarRegresarStock();
					},
					complete: function() {
						$.isLoading("hide");
                      
					}
				});
			} else {
				$.isLoading("hide");
			}
		})
	}

	function borrar(id, id_index_requisicion) {
        // return console.log(id,id_index_requisicion);
		utils.displayWarningDialog("Desea quitar el producto?", "warning", function(data) {
			if (data.value) {
				ajax.post(`api/venta-producto/remove-mpm-item/${id}`, {
                    "id_cita": "<?php echo $numero_orden; ?>",
                    "id_indice": id_index_requisicion
                }, function(response, headers) {
					tabla_carrito.ajax.reload();                   
                    descontarRegresarStock();
				})
			}
		}, true)
	}


    function addReVentasEstatus(id_venta) {
        ajax.post('api/re-estatus-venta', {
            ventas_id: id_venta,
            estatus_ventas_id: 2,
        }, function(response, headers) {
            if (headers.status == 201) {
                toastr.info("Cargando venta");
                $.isLoading("hide");
            }
        })
    }

    const descontarRegresarStock =()=>{
        var productosAgregados = $('#tbl_carrito').DataTable();
        toastr.info("Actualizando inventario espere un momento....");
        let total = productosAgregados.data().count();
        let count = 0;
        $(productosAgregados.data()).each(function(index, product) {
            count++;
            if (product.producto_id) {
                ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + product
                    .producto_id, {},
                    function(response, headers) {
                        if (headers.status == 200) {
                            toastr.info("Inventario actualizado para el producto " + product.descripcion_producto);
                            $.isLoading("hide");
                        }
                    })
            }

            if (total == count) {
                $.isLoading("hide");
                utils.displayWarningDialog("Inventario actualizado", 'success', function(result) {
                    // window.location.reload();
                    tbl_productos.ajax.reload();
                });
            }
        });
    }

    const buscarproducto = () => {
        let object = {};
        if (document.getElementById('descripcion').value !== '') {
            object['descripcion'] = document.getElementById('descripcion').value;
        } else if (document.getElementById('no_identificacion').value !== '') {
            object['no_identificacion'] = document.getElementById('no_identificacion').value;
        }

        let parametros = $.param(object);

        $('#tbl_productos').DataTable().ajax.url(PATH_API + 'api/productos/listadoStock?' + parametros).load()
    }
</script>
@endsection



@section('modal')

    <div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
                        </div>
                        <div class="col-md-12">
                            <input type="hidden" name="producto_id" id="producto_id">
                           
                            <div class="form-group">
                                <label for="select">Aplicar precio</label>
                                <select name="precio_id" class="form-control" id="precio_id">
                                    <option value=""> Seleccionar precio</option>
                                    @foreach ($cat_precios as $precio)
                                    <option value="{{ $precio->id}}"> {{ $precio->descripcion}} {{$precio->precio_publico}} %</option>
                                    @endforeach
                                </select>

                                <input type="hidden" name="valor_unitario" id="valor_unitario">
                                <input type="hidden" name="no_identificacion" id="no_identificacion">
                                <input type="hidden" name="cantidad_almacen_primario" id="cantidad_almacen_primario">
                                <div id='precio_id_error' class='invalid-feedback'></div>
                            </div>
                            <div class="col-md-12">
                                <?php echo renderInputText("text", "valor_unitario_nuevo", "Valor unitario", ''); ?>
                            </div>
                            <div class="col-md-12">
                                <?php echo renderInputText("number", "precio_manual", "Precio Manual %", ''); ?>
                            </div>
                        </div>
    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button onclick="agregarproducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
                </div>
            </div>
        </div>
    </div>
@endsection


