@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : "" }}</li>
	</ol>
	<h4>Estatus de venta : </h4>
	<?php if($folio[0]->re_ventas_estatus->estatus_ventas_id == 6):?>
		<div class="alert alert-warning">
			<?php elseif($folio[0]->re_ventas_estatus->estatus_ventas_id == 2): ?>
		<div class="alert alert-success">
			<?php else: ?>
		<div class="alert alert-warning">
	<?php endif ?>
		<?php echo isset($folio) && isset($folio[0]->re_ventas_estatus) ? $folio[0]->re_ventas_estatus->estatus_venta->nombre : ''; ?>
		</div>
	<div class="row mt-2">
		<div class="col-md-4">
			<?php echo renderInputText("text", "folio", "Folio", isset($folio) ? $folio[0]->folio->folio : '', true); ?>
			<input type="hidden" value="{{ isset($folio) ? $folio[0]->folio_id : '' }}" id="folio_id">
			<input type="hidden" value="{{ isset($folio) ? $folio[0]->id : '' }}" id="venta_id">
			<input type="hidden" value="{{ isset($folio[0]->re_ventas_estatus) ? $folio[0]->re_ventas_estatus->estatus_venta->id : '' }}" id="estatus_venta_id">
			<input type="hidden" value="{{ isset($folio) ? $folio[0]->almacen_id : '' }}" name="almacen_id" id="almacen_id">
			<input type="hidden" value="{{ $folio[0]->re_ventas_estatus->estatus_ventas_id }}" name="estatus_ventas_id" id="estatus_ventas_id">
		</div>
		<div class="col-md-4">
			<?php echo renderInputText("text", "venta_total", "Total", isset($venta_total) ? $venta_total : '', true); ?>
		</div>
		<div class="col-md-4">
			<?php echo renderInputText("text", "cliente", "Cliente", isset($folio) && isset($folio[0]->cliente) ? $folio[0]->cliente->nombre : '', true); ?>
			<input type="hidden" value="{{ isset($folio) && $folio[0]->cliente ? $folio[0]->cliente->id : '' }}" id="cliente_id" name="cliente_id">
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-3">
			<?php echo renderInputText("text", "concepto", "Concepto", isset($cxc->concepto) ? $cxc->concepto : ''); ?>
		</div>
	
		<?php if (!empty($folio) && $folio[0]->re_ventas_estatus->estatus_ventas_id == 1) : ?>
			<div class="col-md-3 mt-4">
				<div class="custom-control custom-checkbox">
					<input type="checkbox" class="custom-control-input check_apartar" onchange="apartarbutton()" id="check_apartar">
					<label class="custom-control-label" for="check_apartar">Confirmar apartar piezas</label>
				</div>
			</div>
		<?php endif ?>
		<div class="col-md-4 container-forma-pago">
			<div class="form-group">
				<label for="select">Plazo de credito</label>
				<select name="plazo_credito_id" class="form-control " id="plazo_credito_id">
					<option value=""> Seleccionar</option>
					@foreach ($plazo_credito as $plazo)
					@if(isset($cxc->plazo_credito_id) && $cxc->plazo_credito_id == $plazo->id)
					<option data-cantidad_mes="{{$plazo->cantidad_mes}}" value="{{ $plazo->id}}" selected="selected"> {{ $plazo->nombre}} </option>
					@else
					<option data-cantidad_mes="{{$plazo->cantidad_mes}}" value="{{ $plazo->id}}"> {{ $plazo->nombre}} </option>
					@endif
					@endforeach
				</select>
				<div id='plazo_credito_id_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-4 container-forma-pago">
			<div class="form-group">
				<label for="select">Enganche</label>
				<input type="text" name="enganche" id="enganche" class="form-control" value="<?php echo isset($cxc->enganche) ? $cxc->enganche : ''; ?>" />
				<div id='enganche_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-4 container-forma-pago">
			<div class="form-group">
				<label for="select">Tasa de interes %</label>
				<input type="text" name="tasa_interes" id="tasa_interes" class="form-control" value="<?php echo isset($cxc->tasa_interes) ? $cxc->tasa_interes : ''; ?>" />
				<div id='tasa_interes_error' class='invalid-feedback'></div>
			</div>
		</div>
		<div class="col-md-4 container-forma-pago">
			<div class="form-group">
				<label for="select">Cobrador asignar</label>
				<select name="usuario_gestor_id" class="form-control " id="usuario_gestor_id">
					<option value=""> Seleccionar</option>
					@foreach ($gestores as $gestor)
					@if(isset($cxc->usuario_gestor_id) && $cxc->usuario_gestor_id == $gestor->id)
					<option value="{{ $gestor->id}}" selected="selected"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
					@else
					<option value="{{ $gestor->id}}"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
					@endif
					@endforeach
				</select>
				<div id='usuario_gestor_id_error' class='invalid-feedback'></div>
			</div>
		</div>
	</div>
	<div class="row mb-4 mt-4">
		<div class="col-md-12 text-right">
			<?php if (isset($folio) && $folio[0]->re_ventas_estatus->estatus_ventas_id == 2) { ?>
				<a target="_blank" class="btn btn-primary col-md-3" id="btn-poliza" href="{{ base_url('refacciones/polizas/generarPolizaVentasPDF?folio_id='.$folio[0]->folio_id) }}">
					<i class="fas fa-print"></i> Poliza
				</a>
			<?php }
			if (isset($folio) && ($folio[0]->re_ventas_estatus->estatus_ventas_id == 1 || $folio[0]->re_ventas_estatus->estatus_ventas_id == 6)) { ?>
				<button class="btn btn-primary col-md-4" id="btn-finalizar">
					<i class="fas fa-shopping-cart"></i>
					Procesar
				</button>

				<button onclick="apartarVenta()" class="btn btn-primary col-md-4 d-none" id="btn-apartar">
					<i class="fas fa-handshake"></i>
					Apartar 
				</button>
			<?php } ?>
		</div>
	</div>
	<br>
	<h3>Productos seleccionados</h3>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="tbl_carrito" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>No identificación</th>
							<th>Descripcion</th>
							<th>Total</th>
							<th>Precio</th>
							<th>Cantidad</th>
							<th>Unidad</th>
							<th>Ubicación producto</th>
							<th>-</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>#</th>
							<th>No identificación</th>
							<th>Descripcion</th>
							<th>Total</th>
							<th>Valor unitario</th>
							<th>Cantidad</th>
							<th>Unidad</th>
							<th>Ubicación producto</th>
							<th>-</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
	<hr>
	<h3>Productos disponibles</h3>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="tbl_productos" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>#</th>
							<th>No identificación</th>
							<th>Descripcion</th>
							<th>Valor unitario</th>
							<th>Ubicación producto</th>
							<th>Existencia <br />total producto</th>
							<th>Existencia <br />almacen principal</th>
							<th>Existencia <br />almacen secundario</th>
							<th>-</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($listado_productos as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->no_identificacion }}</td>
							<td>{{ $item->descripcion }}</td>
							<td>
								$ {{$item->valor_unitario}}
							</td>
							<td>{{ $item->ubicacionProducto }}</td>
							<td>{{ $item->cantidad_actual }}</td>
							<td>{{ $item->cantidad_almacen_primario }}</td>
							<td>{{ $item->cantidad_almacen_secundario }}</td>
							<td>
								@if ($item->cantidad_almacen_primario == 0 || $folio[0]->re_ventas_estatus->estatus_ventas_id > 1)
								- -
								@else
								<button onclick="modalDetalle(this)" data-toggle="modal" data-product_name='{{ $item->descripcion}}' data-product_id='{{ $item->id}}' data-no_identificacion='{{ $item->no_identificacion}}' data-cantidad_almacen_primario='{{ $item->cantidad_almacen_primario}}' data-valor_unitario='{{ $item->valor_unitario}}' class="btn btn-success">
									<i class="fas fa-shopping-cart"></i>
								</button>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
						<tr>
							<th>#</th>
							<th>No identificación</th>
							<th>Descripcion</th>
							<th>Valor unitario</th>
							<th>Ubicación producto</th>
							<th>Existencia <br />total producto</th>
							<th>Existencia <br />almacen principal</th>
							<th>Existencia <br />almacen secundario</th>
							<th>-</th>
						</tr>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>

</div>
@endsection

@section('scripts')
<script>
	$(".container-forma-pago").hide();
	
	$('#tbl_productos').DataTable({
		language: {
			url: PATH_LANGUAGE
		}
	})
	let estatus_venta_id = $('#estatus_venta_id').val();
	var tabla_carrito = $('#tbl_carrito').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: base_url + "refacciones/salidas/ajax_detalle_venta_carrito",
			type: 'POST',
			data: {
				id: function() {
					return $('#venta_id').val()
				}
			}
		},
		columns: [{
				'data': 'id'
			},
			{
				'data': function(data) {
					return utils.isDefined(data.no_identificacion) && data.no_identificacion ? data
						.no_identificacion : null
				}
			},
			{
				'data': function(data) {
					return utils.isDefined(data.descripcion_producto) && data.descripcion_producto ? data
						.descripcion_producto : null
				}
			},
			{
				'data': function(data) {
					return utils.isDefined(data.venta_total) ? "$ " + data.venta_total : null
				}
			},
			{
				'data': function(data) {
					return utils.isDefined(data.valor_unitario) ? "$ " + data.valor_unitario : null
				}
			},
			{
				'data': function(data) {
					return utils.isDefined(data.cantidad) && data.cantidad ? data.cantidad : null
				}
			},
			{
				'data': function(data) {
					return utils.isDefined(data.unidad) && data.unidad ? data.unidad : null
				}
			},
			{
				'data': function(data) {
					return utils.isDefined(data.ubicacion_producto) && data.ubicacion_producto ? data.ubicacion_producto : null
				}
			},
			{
				'data': function(data) {
					if (utils.isDefined(estatus_venta_id) && estatus_venta_id != 1) {
						return '--';
					} else {
						return "<button type='button' class='btn-borrar btn btn-primary' data-id=" + data
							.id + "><i class='fas fa-trash'></i></button>";
					}
				}
			}
		]
	});

	$("#btn-finalizar").on("click", function() {
		$.isLoading({
			text: "Realizando proceso de venta ...."
		});
		let continuar = false;
		let table_carrito_length = $("#tbl_carrito").dataTable().fnSettings().aoData.length
		if (table_carrito_length == 0) {
			utils.displayWarningDialog("Seleccionar elementos para comprar");
			return false;
		}
		
		ajax.post(`api/venta-producto/validarprecioapartado`, { venta_id:$('#venta_id').val()  }, function(response, headers) {
			if (headers.status == 201 || headers.status == 200) {
				response.map(item =>{
					toastr.info(`La pieza: ${item.no_identificacion} tiene un nuevo precio de $${item.valor_actualizado}`)
				})
			}
		});

		realizarVenta();

	});

	function realizarVenta() {
		$.isLoading({
			text: "Realizando proceso de venta ...."
		});
		let id_venta = $('#venta_id').val();
		let concepto = '';
		if ($("#concepto").val()) {
			concepto = 'Mostrador - ' + $('#concepto').val();
		}
		ajax.put(`api/ventas/finalizar/${id_venta}`, {
			folio_id: $('#folio_id').val(),
			venta_total: $('#venta_total').val(),
			tipo_forma_pago_id: 1,//default contado
			concepto: concepto,
			tipo_pago_id: 1,//default efectivo
			cliente_id: $('#cliente_id').val(),
			plazo_credito_id: 14,// una exhibicion
			enganche: 0, //$('#enganche').val(),
			tasa_interes: $('#tasa_interes').val(),
			usuario_gestor_id: $('#usuario_gestor_id').val()
		}, function(response, headers) {
			if (headers.status == 201 || headers.status == 200) {
				toastr.info('Realizando proceso de venta')
				return this.addReVentasEstatus(id_venta); //Cambia estatus a vendido
			} else {
				$.isLoading("hide");

			}
		})

	}

	function modalDetalle(_this) {
		$("#producto_id").val($(_this).data('product_id'));
		$("#valor_unitario").val($(_this).data('valor_unitario'));
		$("#no_identificacion").val($(_this).data('no_identificacion'));
		$("#cantidad_almacen_primario").val($(_this).data('cantidad_almacen_primario'));
		var producto_name = $(_this).data('product_name');
		$("#title_modal").text(producto_name);
		$("#modal-producto-detalle").modal('show');
	}

	$("#cantidad").on('blur', function() {
		let cantidad = $("#cantidad").val();
		let almacen_id = $("#almacen_id").val();
		let producto_id = $("#producto_id").val();
		let cantidad_almacen_primario = $("#cantidad_almacen_primario").val();

		if (parseInt(cantidad_almacen_primario) >= parseInt(cantidad)) {
			$(".validation_error").html('');
			$("#btn-modal-agregar").attr('disabled', false);
		} else {
			toastr.error("No existe cantidad sufiente en el stock " + cantidad_almacen_primario);
			$("#btn-modal-agregar").attr('disabled', true);
			$("#cantidad").val(0);
			$("#cantidad").focus();
		}

	});

	$("#tbl_carrito").on("click", ".btn-borrar", function() {
		var id = $(this).data('id')
		borrar(id)
	});

	function agregarproducto(id) {
		toastr.info("Añadiendo producto al carro de compras");
		$.isLoading({
			text: "Añadiendo producto al carro de compras...."
		});

		let folio_id = $('#folio_id').val();
		let producto_id = $("#producto_id").val();
		let cliente_id = $("#cliente_id").val();
		let precio_id = $("#precio_id").val();
		let cantidad = $("#cantidad").val();
		let valor_unitario = $("#valor_unitario").val();
		let venta_id = $("#venta_id").val();
		let data = {
			folio_id,
			producto_id,
			valor_unitario,
			precio_id,
			cliente_id,
			cantidad,
			venta_id
		}

		ajax.post(`api/venta-producto`, data, function(response, headers) {
			if (headers.status == 201 || headers.status == 200) {
				$("#modal-producto-detalle").modal('hide');
				tabla_carrito.ajax.reload();
				$.ajax({
					type: "GET",
					url: base_url + "refacciones/salidas/ajax_calcular_venta_total/" + $('#folio_id')
						.val(),
					dataType: "json",
					success: function(response) {
						let venta = parseInt(response.venta_total);
						let titulo = "Producto añadido al carro de compras correctamente"
						$('#venta_total').val(venta);
						utils.displayWarningDialog(titulo, 'success', function(result) {
							window.location.reload();
						});
					},
					complete: function() {
						$.isLoading("hide");
					}
				});
			} else {
				$.isLoading("hide");
			}
		})
	}

	function borrar(id) {
		utils.displayWarningDialog("Desea quitar el producto?", "warning", function(data) {
			if (data.value) {
				ajax.delete(`api/venta-producto/${id}`, null, function(response, headers) {
					//tabla_carrito.ajax.reload();
					window.location.reload();
				})
			}
		}, true)
	}

	function addReVentasEstatus(id_venta) {
		ajax.post('api/re-estatus-venta', {
			ventas_id: id_venta,
			estatus_ventas_id: 2,
		}, function(response, headers) {
			if (headers.status == 201) {
				let titulo = "El proceso se efectuo correctamente!"
				var productosAgregados = $('#tbl_carrito').DataTable();
				toastr.info("Actualizando inventario espere un momento....");
				let total = productosAgregados.data().count();
				let count = 0;
				$(productosAgregados.data()).each(function(index, product) {
					ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + product.producto_id, {},
						function(response, headers) {
							if (headers.status == 200) {
								count++;
								toastr.info("Inventario actualizado para el producto " + product.descripcion_producto);

								if (total == count) {
									$.isLoading("hide");
									utils.displayWarningDialog(titulo, 'success', function(result) {
										window.location.reload();
									});
								}
							}
						})
				});
			}
		})
	}

	const apartarbutton = ()=>{
		const check = document.querySelector('.check_apartar').checked;
		if(check){			
			Swal.fire({
				title: 'Alerta',
				text: "¿Esta seguro de apartar las piezas ?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Confirmar!'
			}).then((result) => {
				if (result.value) {
					$("#btn-apartar").removeClass('d-none');
					$("#btn-finalizar").addClass('d-none');
					document.getElementById("concepto").readOnly = true;
					$('#tipo_forma_pago_id').attr("disabled", true);
					$('#tipo_pago_id').attr("disabled", true);
					$(".container-forma-pago").hide();
					$("#plazo_credito_id option[value=14]").attr('selected', true);
				}
			})
		}else{
			$("#btn-apartar").addClass('d-none');
			$("#btn-finalizar").removeClass('d-none');
			document.getElementById("concepto").readOnly = false;
			$('#tipo_forma_pago_id').attr("disabled", false);
			$('#tipo_pago_id').attr("disabled", false);
						
			$(".container-forma-pago").hide();
			$("#plazo_credito_id option[value=14]").attr('selected', true);
			
		}

	}

	const apartarVenta = () =>{
		ajax.post('api/ventas/apartar', {
			venta_id: $('#venta_id').val()
		}, function(response, header) {
			
			if (header.status == 200 || header.status == 204) {
				let titulo = "Piezas apartadas"
				utils.displayWarningDialog(titulo, 'success', function(result) {
				    return window.location.href = base_url + "refacciones/salidas/ventasMostrador";
				});
			}
		})
	}

</script>
@endsection

@section('modal')
<div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal"></h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
					</div>
					<div class="col-md-12">
						<input type="hidden" name="producto_id" id="producto_id">
						<div class="form-group">
							<label for="select">Aplicar precio</label>
							<select name="precio_id" class="form-control" id="precio_id">
								<option value=""> Seleccionar precio</option>
								@foreach ($cat_precios as $precio)
								<option value="{{ $precio->id}}"> {{ $precio->descripcion}} </option>
								@endforeach
							</select>
							<input type="hidden" name="valor_unitario" id="valor_unitario">
							<input type="hidden" name="no_identificacion" id="no_identificacion">
							<input type="hidden" name="cantidad_almacen_primario" id="cantidad_almacen_primario">
							<div id='precio_id_error' class='invalid-feedback'></div>
						</div>
					</div>

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button onclick="agregarproducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
			</div>
		</div>
	</div>
</div>
@endsection