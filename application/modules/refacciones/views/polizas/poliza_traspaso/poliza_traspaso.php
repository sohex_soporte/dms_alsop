<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Traspaso</title>
    <style>
        .contenedor {
            width: 100%;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        .col-6 {
            float: left;
            width: 49%;
            padding: 3px;
        }

        .col-5 {
            float: left;
            width: 40%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid #233a74;
            margin-bottom: 12px
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <div class="col-12">
            Mexico D.F. <?php echo obtenerFechaEnLetra($traspaso->created_at) ?>
        </div>
        <div class="col-12">
            <h2>Poliza de traspasos</h2>
        </div>
        <div class="col-12">
            Folio <?php echo isset($traspaso->folios) ? $traspaso->folios->folio : '--' ?>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-12">
            <table class="table">
                <thead class="">
                    <tr>
                        <th scope="col">
                            Afectación
                        </th>
                        <th scope="col">
                            Descripción
                        </th>
                        <th scope="col" style="width: 30%">
                            Deber
                        </th>
                        <th scope="col" style="width: 30%">
                            Haber
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align:center"> </td>
                        <td style="text-align:center">
                            <?php echo isset($traspaso->almacen_origen) ? $traspaso->almacen_origen->nombre : '--' ?>
                        </td>
                        <td style="text-align:center">
                            $ <?php echo isset($total_traspaso->total) ? $total_traspaso->total : '--' ?>
                        </td>
                        <td style="text-align:center">
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align:center"> </td>
                        <td style="text-align:center">
                             <?php echo isset($traspaso->almacen_destino) ? $traspaso->almacen_destino->nombre : '--' ?>
                        </td>
                        <td style="text-align:center"></td>
                        <td style="text-align:center">
                           $ <?php echo isset($total_traspaso->total) ? $total_traspaso->total : '--' ?>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th scope="col">
                            Afectación
                        </th>
                        <th scope="col">
                            Descripción
                        </th>
                        <th scope="col" style="width: 30%">
                            Deber
                        </th>
                        <th scope="col" style="width: 30%">
                            Haber
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-6">
            SUMAS IGUALES :
        </div>
        <div class="col-6">
           <div class="contenedor">
               <div style="text-align:center" class="col-5">
                $ <?php echo isset($total_traspaso->total) ? $total_traspaso->total : '--' ?>
               </div>
               <div style="text-align:center"  class="col-5">
                $ <?php echo isset($total_traspaso->total) ? $total_traspaso->total : '--' ?>
               </div>
           </div>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-12">
           CONCEPTO: Traspaso de <?php echo obtenerFechaEnLetra($traspaso->created_at) ?>
        </div>
    </div>
</body>

</html>