@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Folio</label>
                    {{ $folio }}
                </div>
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
                <div class="col-sm-4">
                    <label for="">Unidad</label>
                    {{ $id_unidad }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Color</label>
                    {{ $id_color }}
                </div>
                <div class="col-sm-4">
                    <label for="">Solicitud</label>
                    {{ $solicitud }}
                </div>
                <div class="col-sm-4">
                    <label for="">Recibos</label>
                    {{ $recibos }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha entrega</label>
                    {{ $fecha_entrega }}
                </div>
                <div class="col-sm-4">
                    <label for="">Observaciones</label>
                    {{ $observaciones }}
                </div>
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $(".busqueda").select2();
        const url_api = "{{ API_URL_DEV }}";
        $('.numeric').numeric();
        $('.positive').numeric();
        $("#guardar").on('click', function() {
            const data = {
                folio: $("#folio").val(),
                id_unidad: $("#id_unidad").val(),
                id_color: $("#id_color").val(),
                serie: $("#serie").val(),
                solicitud: $("#solicitud").val(),
                recibos: $("#recibos").val(),
                fecha_entrega: $("#fecha_entrega").val(),
                observaciones: $("#observaciones").val()
            };

            if ($("#id").val() != 0) {
                ajax.put('api/logistica/faltantes/' + $("#id").val(), data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información actualizada con éxito";
                        utils.displayWarningDialog("Información actualizada con éxito", "success", function(
                            data) {
                            return window.location.href = base_url +
                                'logistica/faltantes/listado';
                        })
                    })
            } else {
                ajax.post('api/logistica/faltantes', data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información guardada con éxito";
                        utils.displayWarningDialog("Información guardada con éxito", "success", function(
                            data) {
                            return window.location.href = base_url +
                                'logistica/faltantes/listado';
                        })
                    })
            }
        })
        const buscarRemisionBySerie = (serie) => {
            let url = url_api + 'api/unidades/get-all?serie=' + serie;
            ajaxJson(url, {}, "GET", "", function(result) {
                result = result.data;
                console.log('data',result);
                $(".busqueda").select2('destroy');
                if (result.length > 0) {
                    result = result[0];
                    $("#id_unidad").val(result.id);
                    $("#id_color").val(result.id_color_exterior);
                } else {
                }
                $(".busqueda").select2();
            });
        }
        $("#serie").on('change', () => buscarRemisionBySerie($("#serie").val()))
    </script>
@endsection
