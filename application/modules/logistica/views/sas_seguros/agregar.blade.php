@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Reclamo</label>
                    {{ $reclamo }}
                </div>
                <div class="col-sm-4">
                    <label for="">Tipo</label>
                    {{ $tipo }}
                </div>
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Unidad</label>
                    {{ $id_unidad }}
                </div>
                <div class="col-sm-4">
                    <label for="">Color</label>
                    {{ $id_color }}
                </div>
                <div class="col-sm-4">
                    <label for="">Año</label>
                    {{ $id_anio }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Daño</label>
                    {{ $danio }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha alta</label>
                    {{ $fecha_alta }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha entrega asesor</label>
                    {{ $fecha_entrega_asesor }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Estatus</label>
                    {{ $id_estatus }}
                </div>
                <div class="col-sm-4">
                    <label for="">Asesor</label>
                    {{ $id_asesor_venta }}
                </div>
                <div class="col-sm-4">
                    <label for="">Finalizado</label>
                    {{ $finalizado }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fotos</label>
                    {{ $fotos }}
                </div>
                <div class="col-sm-4">
                    <label for="">Documentos</label>
                    {{ $documentos }}
                </div>
                <div class="col-sm-4">
                    <label for="">Presupuestos</label>
                    {{ $presupuestos }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Recuperado</label>
                    {{ $recuperado }}
                </div>
                <div class="col-sm-4">
                    <label for="">Cobrado</label>
                    {{ $cobrado }}
                </div>
                <div class="col-sm-4">
                    <label for="">Observaciones</label>
                    {{ $observaciones }}
                </div>
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
        @if ($id != 0)
            <hr>
            <form id="frm-orden-compra" enctype="multipart/form-data" id="subir_archivos">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="factura">Evidencia</label>
                            <input type="file" class="form-control-file" id="archivo_sas_seguro" name="archivo_sas_seguro">
                            <div id="archivo_sas_seguro_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-right">
                            <button id="add-file" class="btn btn-primary col-md-3" type="button"> Guardar archivo </button>
                        </div>
                    </div>
                </div>
            </form>
            <h3>Lista de evidencia</h3>
            <table class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Archivo</th>
                        <th>Fecha creación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($files as $file)
                        <tr>
                            <td>
                                <?php
                                $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/danios-bodyshop/' . $id));
                                ?>
                                <img src="data:image/png;base64," alt="Red dot" />
                            </td>
                            <td>{{ $file->created_at }}</td>
                            <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $(".busqueda").select2();
        const url_api = "{{ API_URL_DEV }}";
        $("#add-file").on('click', function() {
            $.isLoading({
                text: "Procesando orden compra...."
            });
            let file = $('#archivo_sas_seguro')[0].files[0];
            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('seguro_id', $("#id").val());
            paqueteDeDatos.append('file', file);
            console.log(paqueteDeDatos);
            ajax.postFile(`api/logistica/archivos-sas-seguros/upload`, paqueteDeDatos, function(response, header) {
                return location.reload();
            });

        })
        $('.numeric').numeric();
        $('.positive').numeric();
        $("#guardar").on('click', function() {
            const data = {
                reclamo: $("#reclamo").val(),
                tipo: $("#tipo").val(),
                id_unidad: $("#id_unidad").val(),
                serie: $("#serie").val(),
                id_color: $("#id_color").val(),
                serie: $("#serie").val(),
                id_anio: $("#id_anio").val(),
                observaciones: $("#observaciones").val(),
                danio: $("#danio").val(),
                fecha_alta: $("#fecha_alta").val(),
                fecha_entrega_asesor: $("#fecha_entrega_asesor").val(),
                id_estatus: $("#id_estatus").val(),
                id_asesor_venta: $("#id_asesor_venta").val(),
                finalizado: $("#finalizado").val(),
                fotos: $("#fotos").val(),
                documentos: $("#documentos").val(),
                presupuestos: $("#presupuestos").val(),
                recuperado: $("#recuperado").val(),
                cobrado: $("#cobrado").val()
            };

            if ($("#id").val() != 0) {
                ajax.put('api/logistica/sas-seguros/' + $("#id").val(), data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información actualizada con éxito";
                        utils.displayWarningDialog("Información actualizada con éxito", "success", function(
                            data) {
                            return window.location.href = base_url +
                                'logistica/sasseguros/listado';
                        })
                    })
            } else {
                ajax.post('api/logistica/sas-seguros', data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información guardada con éxito";
                        utils.displayWarningDialog("Información guardada con éxito", "success", function(
                            data) {
                            return window.location.href = base_url +
                                'logistica/sasseguros/listado';
                        })
                    })
            }
        })
        const buscarRemisionBySerie = (serie) => {
            let url = url_api + 'api/unidades/get-all?serie=' + serie;
            ajaxJson(url, {}, "GET", "", function(result) {
                result = result.data;
                console.log('data', result);
                $(".busqueda").select2('destroy');
                if (result.length > 0) {
                    result = result[0];
                    $("#id_unidad").val(result.id);
                    $("#id_color").val(result.id_color_exterior);
                    $("#id_anio option:contains(" + result.modelo + ")").attr('selected', 'selected');

                } else {}
                $(".busqueda").select2();
            });
        }
        $("#serie").on('change', () => buscarRemisionBySerie($("#serie").val()))
    </script>
@endsection
