<div class="row">
    <div class="col-sm-6">
        <strong>Cliente</strong>
        <span>{{ $registro[0]->nombre_cliente . ' ' . $registro[0]->ap_cliente . ' ' . $registro[0]->am_cliente }}</span>
    </div>
    <div class="col-sm-6">
        <strong>Vendedor</strong>
        <span>{{ $registro[0]->nombre_vendedor . ' ' . $registro[0]->ap_vendedor . ' ' . $registro[0]->am_vendedor }}</span>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <strong>Serie</strong> <span>{{ $registro[0]->serie }}</span>
    </div>
    <div class="col-sm-4">
        <strong>Fecha entrega cliente</strong> <span>{{ $registro[0]->fecha_entrega_cliente }}</span>
    </div>
    <div class="col-sm-4">
        <strong>Hora entrega cliente</strong> <span>{{ $registro[0]->hora_entrega_cliente }}</span>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <strong>Eco</strong> <span>{{ $registro[0]->eco }}</span>
    </div>
    <div class="col-sm-4">
        <strong>Fecha promesa vendedor</strong> <span>{{ $registro[0]->fecha_promesa_vendedor }}</span>
    </div>
    <div class="col-sm-4">
        <strong>Hora promesa vendedor</strong> <span>{{ $registro[0]->hora_promesa_vendedor }}</span>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <strong>Ubicación</strong> <span>{{ $registro[0]->ubicacion }}</span>
    </div>
    <div class="col-sm-4">
        <strong>Color</strong> <span>{{ $registro[0]->color }}</span>
    </div>
    <div class="col-sm-4">
        <strong>Unidad</strong> <span>{{ $registro[0]->unidad }}</span>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <strong>Modo</strong> <span>{{ $registro[0]->modo }}</span>
    </div>
</div>
