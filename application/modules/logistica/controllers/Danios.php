<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Danios extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function index()
    {
        echo 1;
    }
    public function listado()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/logistica/danios-bodyshop/get-all', []));
        $data['titulo'] = "Listado Daños Bodyshop";
        $data['bitacora'] = "inicial";
        $this->blade->render('danios/listado', $data);
    }
    public function agregar($id = 0)
    {
        if ($id == 0) {
            $info = new Stdclass();
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/danios-bodyshop/' . $id));
        }

        $data['fecha_arribo'] = form_input('fecha_arribo', set_value('fecha_arribo', exist_obj($info, 'fecha_arribo')), 'class="form-control" id="fecha_arribo"', 'date');
        $estatus_admvo = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/estatus-administrativo'));
        $data['id_estatus_admvo'] = form_dropdown('id_estatus_admvo', array_combos($estatus_admvo, 'id', 'estatus', TRUE), set_value('id_estatus_admvo', exist_obj($info, 'id_estatus_admvo')), 'class="form-control busqueda" id="id_estatus_admvo"');
        $data['fecha_ingreso'] = form_input('fecha_ingreso', set_value('fecha_ingreso', exist_obj($info, 'fecha_ingreso')), 'class="form-control" id="fecha_ingreso"', 'date');
        $asesores = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 13
        ]));
        $data['id_asesor'] = form_dropdown('id_asesor', array_combos($asesores, 'id', 'nombre', TRUE), set_value('id_asesor', exist_obj($info, 'id_asesor')), 'class="form-control busqueda" id="id_asesor"');
        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');
        $colores = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-colores'));
        $data['id_color'] = form_dropdown('id_color', array_combos($colores, 'id', 'nombre', TRUE), set_value('id_color', exist_obj($info, 'id_color')), 'class="form-control busqueda" id="id_color"');
        $data['danios'] = form_input('danios', set_value('danios', exist_obj($info, 'danios')), 'class="form-control" id="danios"');

        $ubicacion_llaves = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-ubicacion-llaves'));
        $data['id_ubicacion_llaves'] = form_dropdown('id_ubicacion_llaves', array_combos($ubicacion_llaves, 'id', 'nombre', TRUE), set_value('id_ubicacion_llaves', exist_obj($info, 'id_ubicacion_llaves')), 'class="form-control busqueda" id="id_ubicacion_llaves"');
        $data['descripcion'] = form_textarea('descripcion', set_value('descripcion', exist_obj($info, 'descripcion')), 'class="form-control" id="descripcion"');
        $data['faltantes'] = form_textarea('faltantes', set_value('faltantes', exist_obj($info, 'faltantes')), 'class="form-control" id="faltantes"');

        $estatus = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/estatus-danios-bodyshop'));
        $data['id_estatus'] = form_dropdown('id_estatus', array_combos($estatus, 'id', 'estatus', TRUE), set_value('id_estatus', exist_obj($info, 'id_estatus')), 'class="form-control busqueda" id="id_estatus"');

        $data['fecha_ingreso'] = form_input('fecha_ingreso', set_value('fecha_ingreso', exist_obj($info, 'fecha_ingreso')), 'class="form-control" id="fecha_ingreso"', 'date');
        $data['fecha_entrega'] = form_input('fecha_entrega', set_value('fecha_entrega', exist_obj($info, 'fecha_entrega')), 'class="form-control" id="fecha_entrega"', 'date');
        $data['observaciones'] = form_textarea('observaciones', set_value('observaciones', exist_obj($info, 'observaciones')), 'class="form-control" id="observaciones"');

        
        $agencia = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/agencias'));
        $data['id_agencia'] = form_dropdown('id_agencia', array_combos($agencia, 'id', 'agencia', TRUE), set_value('id_agencia', exist_obj($info, 'id_agencia')), 'class="form-control busqueda" id="id_agencia"');
        $origenes = procesarResponseApiJsonToArray($this->curl->curlGet('api/telemarketing/catalogo-origenes'));
        $data['id_origen'] = form_dropdown('id_origen', array_combos($origenes, 'id', 'origen', TRUE), set_value('id_origen', exist_obj($info, 'id_origen')), 'class="form-control busqueda" id="id_origen"');
        $data['hora_recepcion'] = form_input('hora_recepcion', set_value('hora_recepcion', exist_obj($info, 'hora_recepcion')), 'class="form-control" id="hora_recepcion"', 'time');

        $data['cargo_a'] = form_input('cargo_a', set_value('cargo_a', exist_obj($info, 'cargo_a')), 'class="form-control" id="cargo_a"');
        $data['cobro_proveedores'] = form_input('cobro_proveedores', set_value('cobro_proveedores', exist_obj($info, 'cobro_proveedores')), 'class="form-control" id="cobro_proveedores"');
        $data['medio_transportacion'] = form_input('medio_transportacion', set_value('medio_transportacion', exist_obj($info, 'medio_transportacion')), 'class="form-control" id="medio_transportacion"');
        $area_reparacion = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/areas-reparacion'));
        $data['id_area_reparacion'] = form_dropdown('id_area_reparacion', array_combos($area_reparacion, 'id', 'area_reparacion', TRUE), set_value('id_area_reparacion', exist_obj($info, 'id_area_reparacion')), 'class="form-control busqueda" id="id_area_reparacion"');
        $data['id'] = $id;
        $data['titulo'] = 'Daños Bodyshop';
        $this->blade->render('danios/agregar', $data);
    }
}
