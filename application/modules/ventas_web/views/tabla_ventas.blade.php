<table id="tbl-ventas" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Folio</th>
            <th>Nombre</th>
            <th>Teléfono casa</th>
            <th>Teléfono celular</th>
            <th>Email</th>
            <th>Unidad</th>
            @if ($vista == 'telemarketing')
                <th>Asesor</th>
                <th>Cita generada</th>
            @elseif ($vista=='prospectos')
                <th>Usuario telemarketing</th>
            @else
                <th>Asesor</th>
                <th>Estatus cita</th>
                <th>fecha cita</th>
                <th>Vehículo vendido</th>
                <th>comentario venta</th>
            @endif
            <th>Origen</th>
            <th>Fecha de creación</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php $clase_erronea = ''; ?>
        @foreach ($ventas as $v => $venta)
            @if ($venta->informacion_erronea && $vista == 'prospectos')
                <?php $clase_erronea = 'observaciones'; ?>
            @endif
            <tr class="{{ $clase_erronea }}">
                <td>{{ $venta->id }}</td>
                <td>{{ $venta->nombre . ' ' . $venta->apellido_paterno . ' ' . $venta->apellido_materno }}
                </td>
                <td>{{ $venta->telefono_secundario }}</td>
                <td>{{ $venta->telefono }}</td>
                <td>{{ $venta->correo_electronico }}</td>
                <td>{{ $venta->unidad}}</td>
                @if ($vista == 'telemarketing')
                    <td>{{ $venta->nombre_usuario_asesor . ' ' . $venta->apellido_paterno_asesor . ' ' . $venta->apellido_materno_asesor }}
                    </td>
                    <td>{{ $venta->id_cita != '' ? 'Si' : 'No' }}</td>
                @elseif ($vista=='prospectos')
                    <td>{{ $venta->nombre_usuario_telemarketing . ' ' . $venta->apellido_paterno_telemarketing . ' ' . $venta->apellido_materno_telemarketing }}
                    </td>
                @else
                    <td>{{ $venta->nombre_usuario_asesor . ' ' . $venta->apellido_paterno_asesor . ' ' . $venta->apellido_materno_asesor }}
                    </td>
                    @if ($venta->id_status == 1 || $venta->id_status == '')
                        <td>Sin estatus</td>
                    @elseif($venta->id_status==2)
                        <td>Cliente llegó</td>
                    @else
                        <td>Cliente no llegó</td>
                    @endif
                    <td>{{ $venta->fecha_cita }}</td>
                    @if ($venta->vendido == 0)
                        <td>Sin estatus</td>

                    @elseif($venta->vendido==1)
                        <td>Vendido</td>
                    @else
                        <td>NO vendido</td>
                    @endif
                    <td>{{ $venta->comentario_venta }}</td>

                @endif
                <td>{{ $venta->origen }}</td>
                <td>{{ $venta->created_at }}</td>
                <td>
                    @if ($vista == 'telemarketing')
                        @if ($venta->id_cita == '')
                            <a href="" data-id="{{ $venta->id }}" class="js_comentarios" aria-hidden="true"
                                data-toggle="tooltip" data-placement="top" title="Comentarios"
                                data-fecha="{{ $venta->fecha_cita_programada }}"
                                data-cliente="{{ $venta->nombre . ' ' . $venta->apellido_paterno . ' ' . $venta->apellido_materno }}"
                                data-no_contactar="{{ $venta->no_contactar }}">
                                <i class="fa fa-comments"></i>
                            </a>
                        @endif
                        @if ($venta->id_cita == '')
                            <a href="" data-id="{{ $venta->id }}" class=" js_info_erronea" aria-hidden="true"
                                data-toggle="tooltip" data-placement="top" title="Información errónea">
                                <i class="fa fa-retweet"></i>
                            </a>
                        @endif
                        <a href="" data-id="{{ $venta->id }}" class="js_historial" aria-hidden="true"
                            data-toggle="tooltip" data-placement="top" title="Historial comentarios"
                            data-tipo_comentario="1">
                            <i class="fa fa-info"></i>
                        </a>
                    @elseif ($vista=='prospectos')
                        <a href="{{ site_url('ventas_web/agregar_venta/' . $venta->id) }}" name="Editar" id="Editar"
                            class="" title="Editar">
                            <i class="fa fa-edit"></i>
                        </a>
                        @if ($venta->informacion_erronea && $vista == 'prospectos')
                            <a href="" data-id="{{ $venta->id }}" class="js_historial" aria-hidden="true"
                                data-toggle="tooltip" data-placement="top" title="Historial comentarios"
                                data-tipo_comentario="2">
                                <i class="fa fa-info"></i>
                            </a>

                            <a href="" data-id="{{ $venta->id }}" class=" js_reenviar" aria-hidden="true"
                                data-toggle="tooltip" data-placement="top" title="Enviar información">
                                <i class="fa fa-share-square"></i>
                            </a>
                        @endif

                    @else
                        <a href="" data-id="{{ $venta->id }}" class=" js_comentarios_asesor" aria-hidden="true"
                            data-toggle="tooltip" data-placement="top" title="Comentarios">
                            <i class="fa fa-comments"></i>
                        </a>

                        <a href="" data-id="{{ $venta->id }}" class="js_historial" aria-hidden="true"
                            data-toggle="tooltip" data-placement="top" title="Historial comentarios"
                            data-tipo_comentario="3">
                            <i class="fa fa-info"></i>
                        </a>

                        @if ($venta->vendido == 0)
                            <a href="" data-id="{{ $venta->id }}" class=" js_vendido" aria-hidden="true"
                                data-toggle="tooltip" data-placement="top" title="Estatus venta">
                                <i class="fa fa-car"></i>
                            </a>
                        @endif
                    @endif

                </td>
            </tr>
        @endforeach
    </tbody>
</table>
