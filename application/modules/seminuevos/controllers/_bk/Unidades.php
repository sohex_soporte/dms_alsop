<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Unidades extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
    }

    public function index()
    {
        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Recepción";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Listado";
        $this->blade->render('unidades/documentacion', $data);
    }

    // public function index()
    // {
    //     $this->load->library('curl');
    //     $this->load->helper('general');

    //     $dataFromApi = $this->curl->curlGet('api/seminuevos/');
    //     $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
    //     $data['data'] = isset($dataregistros) ? $dataregistros : [];

    //     $data['modulo'] = "Seminuevos";
    //     $data['submodulo'] = "Recepción";
    //     $data['titulo'] = "Unidades";
    //     $data['subtitulo'] = "Listado";

    //     $this->blade->render('unidades/listado', $data);
    // }

    // public function alta($id = '')
    // {
    //     $this->load->library('curl');
    //     $this->load->helper('general');

    //     // esto puede ir en el constructor y reutilizar codigo
    //     $usuario_recibe = !empty($this->session->userdata('nombre')) ? $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno') : "Visitante";

    //     $data['usuario_recibe'] = $usuario_recibe;
    //     $data['id_vendedor'] = $this->session->userdata('id');

    //     //Cargamos los catalogos
    //     $marcas = $this->curl->curlGet('api/catalogo-marcas');
    //     $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

    //     // catalogo-modelos
    //     //$marcas = $this->curl->curlGet('api/catalogo-modelos');
    //     //$data['cat_modelo'] = procesarResponseApiJsonToArray($marcas);

    //     $anio = $this->curl->curlGet('api/catalogo-anio');
    //     $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

    //     $color = $this->curl->curlGet('api/catalogo-colores');
    //     $data['cat_color'] = procesarResponseApiJsonToArray($color);

    //     $ubicacion = $this->curl->curlGet('api/catalogo-ubicacion');
    //     $data['cat_ubicacion'] = procesarResponseApiJsonToArray($ubicacion);

    //     $llaves = $this->curl->curlGet('api/catalogo-ubicacion-llaves');
    //     $data['cat_llaves'] = procesarResponseApiJsonToArray($llaves);

    //     $autos = $this->curl->curlGet('api/catalogo-autos');
    //     $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

    //     $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/ultimo-registro');
    //     $dataregistro = json_decode($dataFromApi, TRUE);

    //     if (isset($dataregistro["id"])) {
    //         $data['consecutivo'] = $dataregistro["id"];
    //     }

    //     $catalogos = $this->pseudo_catalogos();
    //     $data['cat_combustible'] = $catalogos['cat_combustible'];

    //     $data['modulo'] = "Seminuevos";
    //     $data['submodulo'] = "Recepción";
    //     $data['titulo'] = "Unidades";
    //     $data['subtitulo'] = "Registro";

    //     $this->blade->render('unidades/recepcion', $data);
    // }

    // public function editar($id = '')
    // {
    //     if ($id) {
    //         $this->load->library('curl');
    //         $this->load->helper('general');

    //         //Recuperamos la informacion del registro
    //         $dataFromApi = $this->curl->curlGet('api/seminuevos/' . $id);
    //         $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
    //         $data = isset($dataregistro)  ? $dataregistro[0] : [];

    //         //Cargamos los catalogos
    //         $marcas = $this->curl->curlGet('api/catalogo-marcas');
    //         $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

    //         // catalogo-modelos
    //         //$marcas = $this->curl->curlGet('api/catalogo-modelos');
    //         //$data['cat_modelo'] = procesarResponseApiJsonToArray($marcas);

    //         $anio = $this->curl->curlGet('api/catalogo-anio');
    //         $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

    //         $color = $this->curl->curlGet('api/catalogo-colores');
    //         $data['cat_color'] = procesarResponseApiJsonToArray($color);

    //         $ubicacion = $this->curl->curlGet('api/catalogo-ubicacion');
    //         $data['cat_ubicacion'] = procesarResponseApiJsonToArray($ubicacion);

    //         $llaves = $this->curl->curlGet('api/catalogo-ubicacion-llaves');
    //         $data['cat_llaves'] = procesarResponseApiJsonToArray($llaves);

    //         $autos = $this->curl->curlGet('api/catalogo-autos');
    //         $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

    //         $catalogos = $this->pseudo_catalogos();
    //         $data['cat_combustible'] = $catalogos['cat_combustible'];

    //         $data['modulo'] = "Seminuevos";
    //         $data['submodulo'] = "Recepción";
    //         $data['titulo'] = "Unidades";
    //         $data['subtitulo'] = "Edición";

    //         $this->blade->render('unidades/recepcion', $data);
    //     }
    // }

    // public function pseudo_catalogos()
    // {
    //     $data_combustible =  [
    //         ['id' => 1, 'nombre' => 'Gasolina'],
    //         ['id' => 2, 'nombre' => 'Diesel'],
    //         ['id' => 3, 'nombre' => 'Híbrido'],
    //         ['id' => 4, 'nombre' => 'Eléctrico']
    //     ];
    //     $new_combustible = [];
    //     foreach ($data_combustible as $key => $value) {
    //         $item = new \stdClass;
    //         $item->id = $value['id'];
    //         $item->nombre = $value['nombre'];
    //         array_push($new_combustible, $item);
    //     }

    //     return [
    //         'cat_combustible' => $new_combustible
    //     ];
    // }
}
