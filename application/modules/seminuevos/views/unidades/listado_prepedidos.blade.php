@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <div class="row mb-3">
            <div class="col-md-8"></div>
            <div class="col-md-4" align="right">
                <a class="btn btn-primary"
                    href="<?php echo base_url('seminuevos/unidades/unidadesventa'); ?>">Nueva Pre-venta</a>
            </div>
        </div>

        <div class="row mb-3">
            <div class="col-md-4">
                <?php renderInputText('date', 'fecha_inicio', 'Fecha inicio:', date('Y-m-d')); ?>
            </div>
            <div class="col-md-4">
                <?php renderInputText('date', 'fecha_fin', 'Fecha fin:', null); ?>
            </div>
            <div class="col-md-4 mt-4">
                <button class="btn btn-primary btn-block" id="btn-filtrar"> Filtrar</button>
            </div>
            <div class="col-md-12  mt-4">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tbl_ventas"></table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var tabla_pre_ventas = $('#tbl_ventas').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "seminuevos/unidades/ajax_preventas",
                type: 'POST',
                data: {
                    fecha_inicio: () => $('#fecha_inicio').val(),
                    fecha_fin: () => $('#fecha_fin').val()
                }
            },
            columns: [{
                    title: "#",
                    data: function(data) {
                        return data.id
                    }
                },
                {
                    title: "Fecha solicitud",
                    data: function(data) {
                        return utils.dateToLetras(data.created_at);
                    }
                },
                {
                    title: "Cliente",
                    data: 'nombre_cliente'
                },
                {
                    title: "Unidad",
                    data: function(data) {
                        return `${data.nombre_marca} - ${data.nombre_modelo} de color  ${data.nombre_color} `;
                    }
                },
                {
                    title: "Enganche",
                    data: 'enganche'
                },
                {
                    title: "Total",
                    data: 'total'
                },
                {
                    title: "Vendedor",
                    data: 'nombre_vendedor'
                },
                {
                    title: "Estatus de venta",
                    data: 'estatus_venta'
                },
                {
                    title: "-",
                    data: function({id}) {
                        return "<a alt='Editar'  href='" + site_url + '/seminuevos/unidades/detalle_venta/' + id + "' class='btn btn-primary'><i class='fas fa-pen'></i></a>";
                    }
                },
                {
                    title: "-",
                    data: function({id}) {
                        //formato salida
                        return "<a href='" + site_url + '/seminuevos/unidades/formatosalida/' + id + "' alt='Formato de salida' class='btn btn-warning'><i class='fas fa-car'></i></a>";
                    }
                },
                {
                    title: "-",
                    data: function({id}) {
                        return "<a alt='documentacion preventa' href='" + base_url + 'seminuevos/unidades/documentacionpreventaunidad/' + id + "' class='btn btn-primary'><i class='far fa-file-alt'></i></a>";
                    }
                }
            ]
        });

        $("#btn-filtrar").on('click', function() {
            tabla_pre_ventas.ajax.reload();
        });

    </script>
    <script type="text/javascript"></script>
@endsection
