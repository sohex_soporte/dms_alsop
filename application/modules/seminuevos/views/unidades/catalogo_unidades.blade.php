@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-10"></div>
        <div class="col-md-2 text-rigth">
        </div>
    </div>
    <div class="row mb-4">
        
        <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title"></h5>
                  <h6 class="card-subtitle mb-3 text-muted">Costo total unidades </h6>
                    <p class="card-text text-danger"> 
                      precio costo: $ {{ $total_unidades->total_precio_costo }}
                      <br>
                      precio venta: $ {{ $total_unidades->total_precio_venta }}
                    </p>
                  <p class="card-text text-danger"> </p>
                </div>
              </div>
        </div>
    
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_cat_unidades" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Año</th>
                            
                            <th>Combustible</th>
                            <th>Precio costo</th>
                            <th>Precio venta</th>
                            <th>Pre-venta</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Año</th>
                            <th>Combustible</th>
                            <th>Precio costo</th>
                            <th>Precio venta</th>
                            <th>Pre-venta</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		 var tabla_unidades = $('#tbl_cat_unidades').DataTable({
        ajax: base_url + "seminuevos/unidades/ajax_recepcion",
        columns: [
            {
                'data': function(data){
                    return data.id
                }
            },
            {
                'data':function (data){
                    return data.nombre_marca
                }
            },
            {
                'data': 'nombre_modelo'
            },
            {
                'data': 'nombre_anio'
            },
            {
                'data': 'combustible'
            },
            {
                'data':function (data){
                    return '$ '+ data.precio_costo
                }
            },
            {
                'data':function (data){
                    return '$ '+ data.precio_venta
                }
            },
            {
                'data': function(data) {
                    return "<a href='" + site_url + '/seminuevos/unidades/preventa_formulario/' + data.id + "' class='btn btn-warning'><i class='fas fa-car'></i></a>";
                }
            }
        ]
    });
	</script>
@endsection