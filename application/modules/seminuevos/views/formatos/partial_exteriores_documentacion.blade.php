<div>
    <table class="table table-bordered">
        <thead class="titulo_pdf">
            <tr>
                <td>
                    Exteriores
                    <div id="exteriores_error"></div>
                </td>
                <td  class="text-center">
                    Si
                </td>
                <td  class="text-center">
                    No
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tapones rueda.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="TaponesRuedaCheck exteriores"  name="tapones_rueda" value="1" {{ (isset($datos->exteriores) && $datos->exteriores->tapones_rueda == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="TaponesRuedaCheck exteriores" name="tapones_rueda" value="2" {{ (isset($datos->exteriores) && $datos->exteriores->tapones_rueda == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Gomas de limpiadores.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="GotasCheck exteriores"  name="goma_limpiadores" value="1" {{ (isset($datos->exteriores) && $datos->exteriores->goma_limpiadores == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="GotasCheck exteriores" name="goma_limpiadores" value="2" {{ (isset($datos->exteriores) && $datos->exteriores->goma_limpiadores == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Antena.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="AntenaCheck exteriores" name="antena" value="1" {{ (isset($datos->exteriores) && $datos->exteriores->antena == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="AntenaCheck exteriores" name="antena" value="2" {{ (isset($datos->exteriores) && $datos->exteriores->antena == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Tapón de gasolina.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="TaponGasolinaCheck exteriores" name="tapon_gasolina" value="1" {{ (isset($datos->exteriores) && $datos->exteriores->tapon_gasolina == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="TaponGasolinaCheck exteriores" name="tapon_gasolina" value="2" {{ (isset($datos->exteriores) && $datos->exteriores->tapon_gasolina == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div>
    <table class="table table-bordered">
        <thead class="titulo_pdf">
            <tr>
                <td>
                    Documentación
                    <div id="documentacion_error"></div>
                </td>
                <td  class="text-center">
                    Si
                </td>
                <td  class="text-center">
                    No
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Póliza garantía/Manual prop.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="PolizaGarantiaCheck documentacion" name="poliza_garantia" value="1" {{ (isset($datos->documentacion) && $datos->documentacion->poliza_garantia == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="PolizaGarantiaCheck documentacion" name="poliza_garantia" value="2" {{ (isset($datos->documentacion) && $datos->documentacion->poliza_garantia == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Seguro de Rines.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="SeguroRinesDocCheck documentacion" name="seguro_rines_documentacion" value="1" {{ (isset($datos->documentacion) && $datos->documentacion->seguro_rines_documentacion == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="SeguroRinesDocCheck documentacion" name="seguro_rines_documentacion" value="2" {{ (isset($datos->documentacion) && $datos->documentacion->seguro_rines_documentacion == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Certificado verificación.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="cVerificacionCheck documentacion" name="certificado_verificacion" value="1" {{ (isset($datos->documentacion) && $datos->documentacion->certificado_verificacion == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="cVerificacionCheck documentacion" name="certificado_verificacion" value="2" {{ (isset($datos->documentacion) && $datos->documentacion->certificado_verificacion == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Tarjeta de circulación.</td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="tCirculacionCheck documentacion" name="tarjeta_circulacion" value="1" {{ (isset($datos->documentacion) && $datos->documentacion->tarjeta_circulacion == '1')  ? 'checked' : '' }}>
                </td>
                <td  class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="tCirculacionCheck documentacion" name="tarjeta_circulacion" value="2" {{ (isset($datos->documentacion) && $datos->documentacion->tarjeta_circulacion == '2')  ? 'checked' : '' }}>
                </td>
            </tr>
        </tbody>
    </table>
</div>