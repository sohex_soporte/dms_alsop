<table class="table text-center table-bordered">
    <thead>
        <tr>
            <th colspan="4">Interiores</th>
        </tr>
        <tr>
            <th scope="col"> - </th>
            <th scope="col">SI</th>
            <th scope="col">NO</th>
            <th scope="col">No cuenta</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <span> Llavero</span>
            </td>
            <td>
                <input type="radio" style="transform: scale(1.5);" class="" name="llavero" value="1" {{ (isset($datos->interiores) && $datos->interiores->llavero == '1')  ? 'checked' : '' }}>
            </td>
            <td>
                <input type="radio" style="transform: scale(1.5);" class="" name="llavero" value="2" {{ (isset($datos->interiores) && $datos->interiores->llavero == '2')  ? 'checked' : '' }}>
            </td>
            <td>
                <input type="radio" style="transform: scale(1.5);" class="" name="llavero" value="3" {{ (isset($datos->interiores) && $datos->interiores->llavero == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Seguro rines.</td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="seguro_rines" value="1" {{ (isset($datos->interiores) && $datos->interiores->seguro_rines == '1')  ? 'checked' : '' }} >
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="seguro_rines" value="2" {{ (isset($datos->interiores) && $datos->interiores->seguro_rines == '3')  ? 'checked' : '' }} >
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="seguro_rines" value="3" {{ (isset($datos->interiores) && $datos->interiores->seguro_rines == '3')  ? 'checked' : '' }} >
            </td>
        </tr>
        <tr>
            <td class="text-left" colspan="4"><b>Indicadores de falla Activados:</b></td>
        </tr>
        <tr>
            <td  colspan="4">
                <table style="width:100%;">
                    <tr>
                        <td class="text-center">
                            <img src="<?php echo base_url('img/inventario/05.png'); ?>" style="width:1.2cm;">
                        </td>
                        <td class="text-center">
                            <img src="<?php echo base_url('img/inventario/02.png'); ?>" style="width:1.2cm;">
                        </td>
                        <td class="text-center">
                            <img src="<?php echo base_url('img/inventario/07.png'); ?>" style="width:1.2cm;">
                        </td>
                        <td class="text-center">
                            <img src="<?php echo base_url('img/inventario/01.png'); ?>" style="width:1.2cm;">
                        </td>
                        <td class="text-center">
                            <img src="<?php echo base_url('img/inventario/06.png'); ?>" style="width:1.2cm;">
                        </td>
                        <td class="text-center">
                            <img src="<?php echo base_url('img/inventario/04.png'); ?>" style="width:1.2cm;">
                        </td>
                        <td class="text-center">
                            <img src="<?php echo base_url('img/inventario/03.png'); ?>" style="width:1.2cm;">
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <input type="checkbox" class="indicadores_activados" style="margin-right: 6px; transform: scale(1.5);"  name="indicadores_activados[]"  value="1">
                        </td>
                        <td class="text-center">
                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="indicadores_activados"  name="indicadores_activados[]"  value="2">
                        </td>
                        <td class="text-center">
                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="indicadores_activados"  name="indicadores_activados[]"  value="3">
                        </td>
                        <td class="text-center">
                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="indicadores_activados"  name="indicadores_activados[]"  value="4">
                        </td>
                        <td class="text-center">
                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="indicadores_activados" name="indicadores_activados[]"  value="5">
                        </td>
                        <td class="text-center">
                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="indicadores_activados" name="indicadores_activados[]"  value="6">
                        </td>
                        <td class="text-center">
                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="indicadores_activados"  name="indicadores_activados[]"  value="7">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Indicador de falla activados.</td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="indicadores" value="1" {{ (isset($datos->interiores) && $datos->interiores->indicadores == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="indicadores" value="2" {{ (isset($datos->interiores) && $datos->interiores->indicadores == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="indicadores" value="3" {{ (isset($datos->interiores) && $datos->interiores->indicadores == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            
            <td>Rociadores y Limpiaparabrisas.</td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="rociador" value="1" {{ (isset($datos->interiores) && $datos->interiores->rociador == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="rociador" value="2" {{ (isset($datos->interiores) && $datos->interiores->rociador == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="rociador" value="3" {{ (isset($datos->interiores) && $datos->interiores->rociador == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Claxon.</td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="claxon" value="1" {{ (isset($datos->interiores) && $datos->interiores->claxon == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="claxon" value="1" {{ (isset($datos->interiores) && $datos->interiores->claxon == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="td_tablas">
                <input type="radio" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="claxon" value="1" {{ (isset($datos->interiores) && $datos->interiores->claxon == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <b>Luces</b>
            </td>
        </tr>
        <tr>
            <td>
                Delanteras.
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="luces_del" value="1" {{ (isset($datos->interiores) && $datos->interiores->luces_del == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="luces_del" value="2" {{ (isset($datos->interiores) && $datos->interiores->luces_del == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="luces_del" value="3" {{ (isset($datos->interiores) && $datos->interiores->luces_del == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>
                Traseras.
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="luces_tras" value="1" {{ (isset($datos->interiores) && $datos->interiores->luces_tras == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="luces_tras" value="2" {{ (isset($datos->interiores) && $datos->interiores->luces_tras == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="luces_tras" value="3" {{ (isset($datos->interiores) && $datos->interiores->luces_tras == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>

                Stop.
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="luces_stop" value="1" {{ (isset($datos->interiores) && $datos->interiores->luces_stop == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="luces_stop" value="1" {{ (isset($datos->interiores) && $datos->interiores->luces_stop == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="luces_stop" value="1" {{ (isset($datos->interiores) && $datos->interiores->luces_stop == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Radio / Caratulas.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="radio"  value="1" {{ (isset($datos->interiores) && $datos->interiores->radio == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="radio" value="2" {{ (isset($datos->interiores) && $datos->interiores->radio == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="radio"  value="3" {{ (isset($datos->interiores) && $datos->interiores->radio == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Pantallas.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="PantallasCheck interiores" name="pantallas" value="1" {{ (isset($datos->interiores) && $datos->interiores->pantallas == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="PantallasCheck interiores" name="pantallas" value="2" {{ (isset($datos->interiores) && $datos->interiores->pantallas == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="PantallasCheck interiores" name="pantallas" value="3" {{ (isset($datos->interiores) && $datos->interiores->pantallas == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>A/C.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="AACheck interiores" name="ac" value="1" {{ (isset($datos->interiores) && $datos->interiores->ac == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="AACheck interiores" name="ac" value="2" {{ (isset($datos->interiores) && $datos->interiores->ac == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="AACheck interiores" name="ac" value="3" {{ (isset($datos->interiores) && $datos->interiores->ac == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Encendedor.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="encendedor" value="1" {{ (isset($datos->interiores) && $datos->interiores->encendedor == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="encendedor" value="2" {{ (isset($datos->interiores) && $datos->interiores->encendedor == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="encendedor" value="3" {{ (isset($datos->interiores) && $datos->interiores->encendedor == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Vidrios.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="VidriosCheck interiores" name="vidrios" value="1" {{ (isset($datos->interiores) && $datos->interiores->vidrios == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="VidriosCheck interiores" name="vidrios" value="2" {{ (isset($datos->interiores) && $datos->interiores->vidrios == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="VidriosCheck interiores" name="vidrios" value="3" {{ (isset($datos->interiores) && $datos->interiores->vidrios == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Espejos.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="EspejosCheck interiores" name="espejos" value="1" {{ (isset($datos->interiores) && $datos->interiores->espejos == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="EspejosCheck interiores" name="espejos" value="2" {{ (isset($datos->interiores) && $datos->interiores->espejos == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="EspejosCheck interiores" name="espejos" value="3" {{ (isset($datos->interiores) && $datos->interiores->espejos == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Seguros eléctricos.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="seguros_electricos" value="1" {{ (isset($datos->interiores) && $datos->interiores->seguros_electricos == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="seguros_electricos" value="2" {{ (isset($datos->interiores) && $datos->interiores->seguros_electricos == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="seguros_electricos" value="3" {{ (isset($datos->interiores) && $datos->interiores->seguros_electricos == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Disco compacto.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="CDCheck interiores" name="disco_compato"  value="1" {{ (isset($datos->interiores) && $datos->interiores->disco_compato == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="CDCheck interiores" name="disco_compato" value="2" {{ (isset($datos->interiores) && $datos->interiores->disco_compato == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="CDCheck interiores" name="disco_compato"   value="3" {{ (isset($datos->interiores) && $datos->interiores->disco_compato == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Asientos y vestiduras.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="asiento_vestidura" value="1" {{ (isset($datos->interiores) && $datos->interiores->asiento_vestidura == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="asiento_vestidura" value="2" {{ (isset($datos->interiores) && $datos->interiores->asiento_vestidura == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="asiento_vestidura" value="3" {{ (isset($datos->interiores) && $datos->interiores->asiento_vestidura == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
        <tr>
            <td>Tapetes.</td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="" name="tapetes" value="1" {{ (isset($datos->interiores) && $datos->interiores->tapetes == '1')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="" name="tapetes" value="2" {{ (isset($datos->interiores) && $datos->interiores->tapetes == '2')  ? 'checked' : '' }}>
            </td>
            <td  class="text-center">
                <input type="radio" style="transform: scale(1.5);" class="" name="tapetes" value="3" {{ (isset($datos->interiores) && $datos->interiores->tapetes == '3')  ? 'checked' : '' }}>
            </td>
        </tr>
    </tbody>
</table>
