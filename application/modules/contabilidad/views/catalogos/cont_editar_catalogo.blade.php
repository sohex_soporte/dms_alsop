@layout('tema_luna/layout')
@section('contenido')

<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>


    <div class="row">
        <div class="col-md-4">
            <form>
                <div class="form-group">
                    <label for="producto_id_sat">No. de Cuenta</label>
                    <input type="text" class="form-control input-sm" value="" id="no_identificacion" name="no_identificacion" placeholder="No. de Cuenta">
                    <div id="no_identificacion_error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="producto_id_sat">Nombre Cta</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_unidad" name="clave_unidad" placeholder="Nombre Cta">
                    <div id="clave_unidad_error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="producto_id_sat">Cve. Auxiliar</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Cve. Auxiliar">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div>

                <div class="form-group">
                    <label for="producto_id_sat">Tipo cuenta</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Tipo cuenta">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div>

                <div class="form-group">
                    <label for="producto_id_sat">Còdigo agrup.</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Còdigo">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div>

                <div class="form-group">
                    <label for="producto_id_sat">Naturaleza</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Naturaleza">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div>
                <br>
                <button type="button" id="btn-guarda_cuenta" class="btn btn-primary">Editar</button>

                <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $("#btn-guarda_cuenta").on('click', function() {
            let data = {
                no_cuenta: '222',
                nombre_cuenta: '222',
                cve_auxiliar: '222',
                tipo_cuenta: '2333',
                codigo: '222',
                naturaleza: '33e',
            };
            return console.log(data);
            ajax.post('api/catalogo-cuentas', data, function(response, header) {
                return console.log(response, header);
                // if (header.status == 201 || header.status == 200) {
                //     let titulo = ""
                //     utils.displayWarningDialog(titulo, 'success', function(result) {

                //     });
                // }

                // if (header.status == 500) {
                //     let titulo = "Error "
                //     utils.displayWarningDialog(titulo, 'warning', function(result) {});
                // }
            })
        })
    });
</script>

@endsection