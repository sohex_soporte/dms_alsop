<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Anticipos extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['titulo'] = "Anticipos";
        $data['subtitulo'] = "listado";
        $api_clientes = $this->curl->curlGet('api/clientes');
        $api_estatus_anticipos = $this->curl->curlGet('api/estatus-anticipos');
        $api_procesos = $this->curl->curlGet('api/procesos-contabilidad');
        $data['clientes'] = procesarResponseApiJsonToArray($api_clientes);
        $data['estatus_anticipos'] = procesarResponseApiJsonToArray($api_estatus_anticipos);
        $data['tipo_procesos'] = procesarResponseApiJsonToArray($api_procesos);
        $this->blade->render('anticipos/index', $data);
    }

    public function comprobante_anticipo($key)
    {
        $data = [];
        $api_anticipos = $this->curl->curlGet('api/anticipos/' . $key);
        $anticipo = procesarResponseApiJsonToArray($api_anticipos);
        $api_clientes = $this->curl->curlGet('api/clientes/' . $anticipo->cliente_id);
        $cliente = procesarResponseApiJsonToArray($api_clientes);
        $data['anticipo'] = $anticipo;
        $data['cliente'] = current($cliente);
        $view = $this->load->view('anticipos/formato_comprobante_anticipo', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }
}
