@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body pb-4">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5" style="margin-bottom:0px !important">
				<tr>
					<th colspan="4">Datos del cliente</th>
				</tr>
				<tr>
					<th width="10%">Nombre: </th>
					<td width="40%"><?php echo isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : ''; ?></td>
					<th width="10%">Núm. cliente: </th>
					<td width="40%"><?php echo isset($cliente->numero_cliente) ? $cliente->numero_cliente : ''; ?></td>
				</tr>
				<tr>
					<th>Domicilio: </th>
					<td><?php echo isset($cliente->direccion) ? $cliente->direccion . ' ' . $cliente->colonia . ' ' . $cliente->municipio . ' ' . $cliente->estado : ''; ?></td>
					<th>Folio: </th>
					<td><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></td>
				</tr>
				<tr>
					<th>Concepto: </th>
					<td><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></td>
					<th>Total: </th>
					<?php $total = $data_cuentas->total + $data['anticipos']->total; ?>
					<td class=""><?php echo isset($data_cuentas->total) ? '$' . number_format($total, 2) : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Fecha compra: </th>
					<td><?php echo isset($data_cuentas->fecha) ? $data_cuentas->fecha : ''; ?></td>
					<th>Total anticipos aplicados: </th>
					<td><?php echo isset($data['anticipos']->total) ? $data['anticipos']->total : ''; ?></td>
				</tr>
				<tr>
					<th colspan="4">ANTICIPOS A FAVOR</th>
				</tr>
			</table>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped table-bordered" id="table_anticipos" width="100%" cellspacing="0">
					</table>
				</div>
			</div>
		</div>
	</div>
	<hr/>
	<h4>Aplicar Descuentos</h4>
	<?php
	 if (in_array($data_cuentas->tipo_proceso_id, [8, 9])) { ?>
		<div class="row mt-4">
			<div class="col-md-2">
				<div class="form-group">
					<label>Total refacciones:</label>
					<input type="text" id="total_aplicar_1" readonly="readonly" value="<?php echo isset($descuento_refaccion->total_aplicar) ? number_format($descuento_refaccion->total_aplicar, 2) : number_format($total_refacciones, 2); ?>" class="form-control money_format">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>% Descuento:</label>
					<input type="text" name="porcentaje_1" id="porcentaje_1" maxlength="2" value="<?php echo isset($descuento_refaccion->porcentaje) ? ($descuento_refaccion->porcentaje) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Cantidad descontada:</label>
					<input type="text" name="cantidad_descontada_1" readonly="readonly" id="cantidad_descontada_1" value="<?php echo isset($descuento_refaccion->cantidad_descontada) ? ($descuento_refaccion->cantidad_descontada) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Total con descuento:</label>
					<input type="text" name="total_descuento_1" readonly="readonly" id="total_descuento_1" value="<?php echo isset($descuento_refaccion->total_descuento) ? ($descuento_refaccion->total_descuento) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Comentario:</label><br />
					<textarea rows="3" name="comentario_1" class="form-control" id="comentario_1"><?php echo isset($descuento_refaccion->comentario) ? ($descuento_refaccion->comentario) : ''; ?></textarea>
				</div>
			</div>
			<?php if (!$descuento_refaccion->folio_id && $data_cuentas->estatus_cuenta_id == 5) { ?>
				<div class="col-md-1 mt-4">
					<button type="button" class="btn btn-primary text-right" id="btn-aplicar-1">Aplicar</button>
				</div>
			<?php } ?>
		</div>
	<?php }
	if (in_array($data_cuentas->tipo_proceso_id, [8, 9])) { ?>

		<div class="row mt-4">
			<div class="col-md-2">
				<div class="form-group">
					<label>Total mano obra:</label>
					<input type="text" id="total_aplicar_2" readonly="readonly" value="<?php echo isset($descuento_mano_obra->total_aplicar) ? number_format($descuento_mano_obra->total_aplicar, 2) : number_format($total_mano_obra, 2); ?>" class="form-control money_format">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>% Descuento:</label>
					<input type="text" name="porcentaje_2" id="porcentaje_2" maxlength="2" value="<?php echo isset($descuento_mano_obra->porcentaje) ? ($descuento_mano_obra->porcentaje) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Cantidad descontada:</label>
					<input type="text" name="cantidad_descontada_2" readonly="readonly" id="cantidad_descontada_2" value="<?php echo isset($descuento_mano_obra->cantidad_descontada) ? ($descuento_mano_obra->cantidad_descontada) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Total con descuento:</label>
					<input type="text" name="total_descuento_2" readonly="readonly" id="total_descuento_2" value="<?php echo isset($descuento_mano_obra->total_descuento) ? ($descuento_mano_obra->total_descuento) : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Comentario:</label><br />
					<textarea rows="3" name="comentario_2" class="form-control" id="comentario_2"><?php echo isset($descuento_mano_obra->comentario) ? ($descuento_mano_obra->comentario) : ''; ?></textarea>
				</div>
			</div>
			<?php if (!$descuento_mano_obra->folio_id && $data_cuentas->estatus_cuenta_id == 5) { ?>
				<div class="col-md-1 mt-4">
					<button type="button" class="btn btn-primary text-right" id="btn-aplicar-2">Aplicar</button>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<hr />
	<h4>Realizar pago [ <?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?> ]</h4>
	<div class="row mt-4">
		<div class="col-md-4">
			<div class="form-group">
				<label>Total a pagar:</label>
				<input type="text" name="importe" id="importe" readonly="readonly" value="<?php echo isset($data_cuentas->total) ? number_format($data_cuentas->total, 2) : ''; ?>" class="form-control money_format">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Tipo de pago</label>
				<select class="form-control" id="tipo_pago" name="tipo_pago" style="width: 100%;">
					<option value="">Selecionar ...</option>
					@if(!empty($cat_tipo_pago))
					@foreach ($cat_tipo_pago as $pago)
					@if($unico_pago->tipo_pago_id == $pago->id)
					<option value="{{$pago->id}}" selected="selected">[{{$pago->clave}}] {{$pago->nombre}}</option>
					@else
					<option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
					@endif
					@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Tipo de CDFI</label>
				<select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
					<option value="">Selecionar ...</option>
					@if(!empty($cat_cfdi))
					@foreach ($cat_cfdi as $cfdi)
					@if($unico_pago->cfdi_id == $cfdi->id)
					<option value="{{$cfdi->id}}" selected="selected">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
					@else
					<option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
					@endif
					@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Caja</label>
				<select class="form-control" id="caja_id" name="caja_id" style="width: 100%;">
					@if(!empty($catalogo_caja))
					@foreach ($catalogo_caja as $caja)
					@if($unico_pago->caja_id == $caja->id)
					<option value="{{$caja->id}}" selected="selected">{{$caja->nombre}} </option>
					@else
					<option value="{{$caja->id}}">{{$caja->nombre}}</option>
					@endif
					@endforeach
					@endif
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Su pago:</label>
				<input type="text" name="pago" id="pago" value="<?php echo isset($unico_pago->pago) ? $unico_pago->pago : ''; ?>" class="form-control money_format">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Cambio:</label>
				<input type="text" name="cambio" id="cambio" value="<?php echo isset($unico_pago->cambio) ? $unico_pago->cambio : ''; ?>" class="form-control money_format">
			</div>
		</div>
		<?php if ($data_cuentas->estatus_cuenta_id == 5) { ?>
			<div class="col-md-12 mt-4 mb-4">
				<div class="text-right">
					<button id="btn-pagar" type="button" class="btn btn-primary col-md-2"><i class="fas fa-cash-register"></i> Pagar</button>
				</div>
			</div>
		<?php } ?>
	</div>
	<div class="row mt-3">
		<div class="text-left col-md-2">
			<a href="{{ site_url('caja/entradas/pagos') }}" class="btn btn-primary "><i class="fas fa-arrow-left"></i> Regresar</a>
		</div>
		<?php if ($data_cuentas->estatus_cuenta_id > 1 && $data_cuentas->estatus_cuenta_id <= 3) { ?>
			<div class="col-md-10 text-right">
				<button id="btn-imprimir_poliza" onclick="imprimir_estado_cuenta(this)" data-cuenta_por_pagar_id="<?php echo $data_cuentas->id; ?>" type="button" class="btn btn-primary "><i class="fas fa-file-pdf"></i> Imprimir estado cuenta</button>
			</div>
		<?php } ?>
	</div>
	<input type="hidden" name="fecha_pago" id="fecha_pago" value="<?php echo isset($unico_pago->fecha_pago) ? $unico_pago->fecha_pago : date('Y-m-d'); ?>" class="form-control">
	<input type="hidden" id="abono_id" value="<?php echo isset($unico_pago->id) ? $unico_pago->id : ''; ?>" />
	<input type="hidden" id="tipo_proceso_id" value="<?php echo isset($data_cuentas->tipo_proceso_id) ? $data_cuentas->tipo_proceso_id : ''; ?>" />
	<input type="hidden" id="cliente_id" value="<?php echo isset($data_cuentas->cliente_id) ? $data_cuentas->cliente_id : ''; ?>" />
	<input type="hidden" id="folio_id" value="{{ isset($data_cuentas->folio_id) ? $data_cuentas->folio_id : ''; }}" />
	<input type="hidden" id="estatus_cuenta_id" value="{{ isset($data_cuentas->estatus_cuenta_id) ? $data_cuentas->estatus_cuenta_id : ''; }}" />

</div>
@endsection

@section('scripts')
<script type="text/javascript">
	let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	var d = new Date();
	var strDate = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate();
	let orden_entrada_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let dataForm = [];
	$("#btn-pagar").on('click', function() {
		$.isLoading({
			text: "Realizando petición ...."
		});
		dataForm = {
			cuenta_por_cobrar_id: orden_entrada_id,
			total_pago: document.getElementById("importe").value.replace(",", ""),
			tipo_abono_id: 3,
			tipo_pago_id: document.getElementById("tipo_pago").value.replace(",", ""),
			cfdi_id: document.getElementById("cfdi_id").value,
			caja_id: document.getElementById("caja_id").value,
			fecha_pago: strDate,
			estatus_abono_id: 3,
			fecha_vencimiento: strDate
		}
		let pago = $("#pago").val().replace(",", "");;
		if (!parseFloat(pago) && pago < 1) {
			$.isLoading("hide");
			toastr.error("Falta indicar el pago")
			$.isLoading("hide");
			return false;
		}
		let abono_id = $("#abono_id").val();
		ajax.put(`api/abonos-por-cobrar/${abono_id}`, dataForm, function(response, headers) {
			if (headers.status == 200 || headers.status == 201) {
				$.isLoading("hide");
				ajax.put(`api/cuentas-por-cobrar/${orden_entrada_id}`, {
					estatus_cuenta_id: 2
				}, function(response, header) {
					if (header.status == 400) {
						$.isLoading("hide");
						return ajax.showValidations(header);
					}
					utils.displayWarningDialog(header.message, "success", function(data) {
						window.location.reload();
					})
				})
			} else {
				$.isLoading("hide");
				return ajax.showValidations(headers);
			}
		})
	});

	$("#pago").on('blur', function() {
		let pago = $("#pago").val().replace(",", "");
		let importe = $("#importe").val().replace(",", "");
		if (parseFloat(pago) < parseFloat(importe)) {
			toastr.error("El pago es menor al total a pagar")
			$("#btn-pagar").attr('disabled', true);
			$("#pago").val(0);
		} else {
			let cambio = parseFloat(pago) - parseFloat(importe);
			$("#cambio").val(cambio.toFixed(2));
			$("#btn-pagar").attr('disabled', false);
		}
	});

	function imprimir_comprobante(_this) {
		abono_id = $(_this).data('abono_id');
		window.location.href = PATH + '/caja/entradas/imprime_comprobante?abono_id=' + window.btoa(abono_id);
	}

	function imprimir_estado_cuenta(_this) {
		cuenta_id = $(_this).data('cuenta_por_pagar_id');
		window.location.href = PATH + '/caja/entradas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id);
	}

	function aplicarAnticipo(_this) {

		total_pagar = document.getElementById("importe").value.replace(",", "");
		total_anticipo = $(_this).data('total_anticipo');

		if (parseFloat(total_anticipo) > parseFloat(total_pagar)) {
			utils.displayWarningDialog('El anticipo es mayor al total de la compra', "warning", function(data) {
				return false;
			})
		} else {
			Swal.fire({
				icon: 'warning',
				title: '¿Esta seguro de aplicar el anticipo?',
				showCancelButton: true,
				confirmButtonText: "Si",
				cancelButtonText: "No"
			}).then((result) => {
				if (result.value) {
					let params = {
						'folio_id': $("#folio_id").val()
					}
					ajax.put('api/anticipos/aplicar-anticipo/' + $(_this).data('anticipo_id'), params, function(response, headers) {
						if (headers.status == 400) {
							return ajax.showValidations(headers);
						}
						utils.displayWarningDialog('Anticipo aplicado correctamente', "success", function(data) {
							getBusqueda();
						})
					})
				}
			});
		}
	}

	function crearTabla() {
		$('#table_anticipos').dataTable(this.configuracionTabla());
	}

	function configuracionTabla() {
		let estatus_cuenta_id = $("#estatus_cuenta_id").val();

		return {
			language: {
				url: PATH_LANGUAGE
			},
			order: [
				[0, 'asc']
			],
			searching: false,
			bInfo: false,
			searching: false,
			bLengthChange: false,
			columns: [{
					title: "Folio anticipo",
					render: function(data, type, row) {
						return '<small><b>' + ('0000' + row.id).slice(-5) + '</small>';
					},
				},
				{
					title: "Tipo",
					render: function(data, type, row) {
						return '<small>' + row.proceso + '</small>';
					},
				},
				{
					title: "Comentario",
					render: function(data, type, row) {
						return '<small>' + row.comentario + '</small>';
					},
				},
				{
					title: "Total",
					data: 'total',
					render: function(data, type, row) {
						return '<small class="money_format"><b>' + parseFloat(data).toFixed(2) + '</b></small>';
					},
				},
				{
					title: "Estatus",
					render: function(data, type, row) {
						let badge = '';
						switch (parseInt(row.estatus_id)) {
							case 1:
								badge = "badge-info";
								break;
							case 2:
								badge = "badge-success";
								break;
							case 3:
								badge = "badge-danger";
								break;

							default:
								break;
						}
						return '<div class="badge ' + badge + '"><small>' + row.estatus + '</small></div>';
					},
				},
				{
					title: "Fecha creación",
					render: function(data, type, row) {
						return '<small>' + this.obtenerFechaMostrar(row.fecha) + '</small>';
					},
				},
				{
					title: "Acciones",
					render: function(data, type, row) {
						let btn_utilizar = '';
						if (row.estatus_id == 1 && estatus_cuenta_id == 5) {
							btn_utilizar = '<button title="Utilizar anticipo" onclick="aplicarAnticipo(this)" data-total_anticipo="' + row.total + '" data-anticipo_id="' + row.id + '" type="button" class="btn btn-success"><i class="fa fa-cash-register fa-1x"></i></button>';
						}
						return btn_utilizar;
					}
				},
			],

		}
	}

	function getBusqueda() {
		let params = {
			'cliente_id': $("#cliente_id").val(),
			'tipo_proceso_id': $("#tipo_proceso_id").val(),
		}
		ajax.get("api/anticipos/get-filter", params, function(response, header) {
			var listado = $('table#table_anticipos').DataTable();
			listado.clear().draw();
			if (response && response.length > 0) {
				response.forEach(listado.row.add);
				listado.draw();
			}
		});
	}

	function obtenerFechaMostrar(fecha) {
		const dia = 2,
			mes = 1,
			anio = 0;
		fecha = fecha.split(' ');
		fecha = fecha[0].split('-');
		return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
	}

	$(".select2").select2();
	$('.money_format').mask("#,##0.00", {
		reverse: true
	});
	this.crearTabla();
	this.getBusqueda();

	$("#porcentaje_1").on('blur', function() {
		let porcentaje = $("#porcentaje_1").val();

		if (!$.isNumeric(porcentaje)) {
			toastr.error("El campo no es un número")
		}
		let total_aplicar = $("#total_aplicar_1").val().replace(",", "");
		let cantidad_descontada = total_aplicar * porcentaje / 100;
		let total_descuento = total_aplicar - cantidad_descontada;
		$("#total_descuento_1").val(parseFloat(total_descuento).toFixed(2));
		$("#cantidad_descontada_1").val(parseFloat(cantidad_descontada).toFixed(2));
	});

	$("#porcentaje_2").on('blur', function() {
		let porcentaje = $("#porcentaje_2").val();

		if (!$.isNumeric(porcentaje)) {
			toastr.error("El campo no es un número")
		}
		let total_aplicar = $("#total_aplicar_2").val().replace(",", "");
		let cantidad_descontada = total_aplicar * porcentaje / 100;
		let total_descuento = total_aplicar - cantidad_descontada;
		$("#total_descuento_2").val(parseFloat(total_descuento).toFixed(2));
		$("#cantidad_descontada_2").val(parseFloat(cantidad_descontada).toFixed(2));
	});

	$("#btn-aplicar-1").on('click', function() {
		$.isLoading({
			text: "Realizando petición ...."
		});
		dataForm = {
			folio_id: $("#folio_id").val(),
			total_aplicar: document.getElementById("total_aplicar_1").value.replace(",", ""),
			tipo_descuento: 1,
			porcentaje: document.getElementById("porcentaje_1").value,
			cantidad_descontada: document.getElementById("cantidad_descontada_1").value.replace(",", ""),
			total_descuento: document.getElementById("total_descuento_1").value.replace(",", ""),
			comentario: document.getElementById("comentario_1").value,
			fecha: strDate
		}

		ajax.post(`api/descuentos/`, dataForm, function(response, headers) {
			if (headers.status == 200 || headers.status == 201) {
				$.isLoading("hide");
					utils.displayWarningDialog(headers.message, "success", function(data) {
						window.location.reload();
					})
			} else {
				$.isLoading("hide");
				return ajax.showValidations(headers);
			}
		})
	});

	$("#btn-aplicar-2").on('click', function() {
		$.isLoading({
			text: "Realizando petición ...."
		});
		dataForm = {
			folio_id: $("#folio_id").val(),
			total_aplicar: document.getElementById("total_aplicar_2").value.replace(",", ""),
			tipo_descuento: 2,
			porcentaje: document.getElementById("porcentaje_2").value,
			cantidad_descontada: document.getElementById("cantidad_descontada_2").value.replace(",", ""),
			total_descuento: document.getElementById("total_descuento_2").value.replace(",", ""),
			comentario: document.getElementById("comentario_2").value,
			fecha: strDate
		}

		ajax.post(`api/descuentos/`, dataForm, function(response, headers) {
			if (headers.status == 200 || headers.status == 201) {
				$.isLoading("hide");
					utils.displayWarningDialog(headers.message, "success", function(data) {
						window.location.reload();
					})
			} else {
				$.isLoading("hide");
				return ajax.showValidations(headers);
			}
		})
	});
</script>
@endsection