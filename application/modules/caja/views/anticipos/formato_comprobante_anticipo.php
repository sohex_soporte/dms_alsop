<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Comprobante anticipo</title>
	<style>
		.contenedor {
			width: 100%;
		}

		td {
			font-size: 11px !important;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		.text-right {
			text-align: right;
		}

		table#detalle_cuenta,
		td,
		td {
			border-bottom: 0.1px solid #233a74;
			margin-bottom: 12px
		}
	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12 text-right">
			<?php echo obtenerFechaEnLetra(date('yy-m-d')); ?>
		</div>
		<div class="col-12">
			<h2>FORD</h2>
			<h2><?php echo NOMBRE_SUCURSAL; ?></h2>
			<h4>Comprobante de anticipo</h4>
		</div>
	</div>
	<div class="col-6">
		<h4>Datos cliente</h4>
		<table style="width:100%;" class="table" cellpadding="5">
			<tr>
				<td>Nombre cliente:</td>
				<td><?php echo isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : ''; ?></td>
			</tr>
			<tr>
				<td>RFC:</td>
				<td><?php echo isset($cliente->rfc) ? $cliente->rfc : ''; ?></td>
			</tr>
			<tr>
				<td>Dirección :</td>
				<td><?php echo isset($cliente->estado) ? $cliente->estado . ' ' . $cliente->municipio . ' ' . $cliente->colonia . ' ' . $cliente->direccion : ' '; ?></td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
	<div class="contenedor">
		<div class="col-12 mt-4">
			<h4>Detalle comprobante</h4>
			<table id="detalle_cuenta" style="width:100%;" class="table" cellpadding="5">
				<tr>
					<td width="120px">Folio: </td>
					<td><?php echo isset($anticipo->id) ? utils::folio($anticipo->id) : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Tipo anticipo: </td>
					<td><?php echo isset($anticipo->tipo_proceso_id) && $anticipo->tipo_proceso_id == 8 ? 'Servicios' : 'Refacciones'; ?></td>
				</tr>
				<tr>
					<td width="120px">Comentario: </td>
					<td><?php echo isset($anticipo->comentario) ? $anticipo->comentario : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Monto del anticipo: </td>
					<td><?php echo isset($anticipo->total) ? '$' . (number_format($anticipo->total, 2)) : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Fecha anticipo: </td>
					<td><?php echo isset($anticipo->fecha) ? utils::aFecha($anticipo->fecha, true) : ''; ?></td>
				</tr>
			</table>
			<div class="clear"></div>
		</div>
	</div>
</body>

</html>