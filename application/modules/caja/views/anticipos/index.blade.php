@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <hr />
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Cliente:</label>
                <select name="cliente_id" id="cliente_id" class="form-control">
                    <option value="">Selecionar ...</option>
                    @foreach ($clientes as $cliente)
                    <option value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_paterno}} </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Estatus:</label>
                <select name="estatus_id" id="estatus_id" class="form-control">
                    <option value="">Selecionar ...</option>
                    @foreach ($estatus_anticipos as $estatus)
                    <option value="{{ $estatus->id}}"> {{ $estatus->nombre }} </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12 mt-4">
            <div class="text-right">
                <button type="button" id="btn-limpiar" onclick="limpiarfiltro()" class="btn btn-primary">
                    <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
                </button>
                <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary ">
                    <i class="fa fa-search" aria-hidden="true"></i> Filtrar
                </button>
            </div>
        </div>
    </div>
    <hr />
    <div class="row mb-4">
        <div class="col-md-12 text-right">
            <button type="button" onclick="openModal();" class="btn btn-primary" type="button"><i class="fa fa-plus"></i>&nbsp;Crear anticipo</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-bordered" id="table_anticipos" width="100%" cellspacing="0">
            </table>
        </div>
    </div>

</div>
@endsection
@section('modal')
<div class="modal fade" id="modal-anticipo" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Anticipo</h5>
            </div>
            <div class="modal-body" style="padding:20px !important">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>* Cliente</label>
                            <select name="cliente_id_modal" id="cliente_id_modal" class="form-control select2Modal" style="width:100%">
                                <option value="">Selecionar ...</option>
                                @foreach ($clientes as $cliente)
                                <option value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_paterno}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>* Proceso</label>
                            <select class="form-control" id="tipo_proceso_id" name="tipo_proceso_id" style="width:100%">
                                <option value="">Selecionar ...</option>
                                @foreach ($tipo_procesos as $proceso)
                                <option value="{{$proceso->id}}">{{ $proceso->descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>* Total:</label>
                            <input type="text" name="total" id="total" class="form-control money_format">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>* Comentario:</label>
                            <input type="text" name="comentario" id="comentario" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>* Fecha</label>
                        <input type="date" name="fecha" id="fecha" class="form-control" value="{{ date('Y-m-d') }}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-modal-anticipo" onclick="crearAnticipo();" type="button" class="btn btn-primary"><i class="fas fa-save"></i>
                    Guardar</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ base_url('js/caja/anticipos/index.js') }}"></script>
@endsection