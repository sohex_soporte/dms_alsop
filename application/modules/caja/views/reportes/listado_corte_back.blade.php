@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <hr/>
    <div class="row mb-4">
        <div class="col-md-12 text-right">
            <a href="{{ base_url('caja/reportes/nuevo_corte') }}" class="btn btn-primary" type="button">Nuevo corte caja</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tabla_caja" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ base_url('js/caja/corte_caja/listado_corte.js') }}"></script>
@endsection