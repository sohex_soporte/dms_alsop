<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cartera extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
        $this->load->library('curl'); 
        $this->load->helper('general');

        //Cargamos la lista de clientes
        $proveedores = $this->curl->curlGet('api/catalogo-proveedor');
        $data['data'] = procesarResponseApiJsonToArray($proveedores);

        $data['modulo'] = "CXP";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Cartera";
        $data['subtitulo'] = "Listado";
        
        $this->blade->render('cartera/listado', $data);
    }

    public function informacion($id='')
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/catalogo-proveedor/' . $id);
        $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataRegistro)  ? $dataRegistro[0] : [];
        
        $data['modulo'] = "CXP";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Cartera";
        $data['subtitulo'] = "Información";

        $this->blade->render('cartera/ficha', $data);
    }

}
