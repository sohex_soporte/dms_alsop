@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_solicitudes" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Buro de credito</th>
                            <th>Nombre de referencia</th>
                            <th>Fecha</th>
                            <th>--</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Buro de credito</th>
                            <th>Nombre de referencia</th>
                            <th>Fecha</th>
                            <th>--</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var tabla_cxc = $('#tbl_solicitudes').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        ajax: base_url + "cxc/index/ajax_solicitudes_credito",
        columns: [
            {
                'data': function(data) {
                    return data.id;
                },
                
            },
            {
                'data': function(data) {
                    return data.nombre_cliente;
                },
                
            },
            {
                'data': function(data) {
                    return data.buro_credito == 1 ? "Si" : "No" ;
                },
                
            },
            {
                'data': function(data) {
                    return data.referencia_nombre;
                },
                
            },
            {
                'data': function(data) {
                    return data.created_at;
                },
                
            },
            {
                'data': function(data) {
                    return "<a href='" + site_url + '/cxc/Index/detalleSolicitud/' + data.id + "' class='btn btn-primary'><i class='fas fa-list'></i></a>";
                }
            }
        ]
    });

</script>
@endsection