@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="cheque_estado"> <!-- enctype="multipart/form-data"  -->
                <h4>Datos de captura</h4>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Usuario que afecta:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->usuario)){print_r($data->usuario);} ?>"  id="usuario" name="usuario" placeholder="">
                            <div id="usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha:</label>
                            <input type="date" class="form-control" min="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->fecha)){print_r($data->fecha);}else {echo date('Y-m-d');} ?>" id="fecha" name="fecha" placeholder="">
                            <div id="fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Estatus del cheque:</label>
                            <input type="text" class="form-control" value="<?php if(isset($status)){if($status == '1') {echo 'COMPROBADO';}else{ echo 'DEVUELTO/RECHAZADO';}} ?>"  id="status" name="status" placeholder="">
                            <div id="status_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4>Datos del cliente</h4>
                <hr>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Código Cliente:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->clave_cliente)){print_r($data->clave_cliente);} ?>"  id="clave_cliente" name="clave_cliente" placeholder="">
                            <div id="clave_cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">Cliente:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->cliente)){print_r($data->cliente);} ?>"  id="cliente" name="cliente" placeholder="">
                            <div id="cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div> 

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">R.F.C:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->rfc)){print_r($data->rfc);} ?>"  id="rfc" name="rfc" placeholder="">
                            <div id="rfc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Telefono :</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->telefono)){print_r($data->telefono);} ?>"  id="telefono" name="telefono" placeholder="">
                            <div id="telefono_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Correo Electrónico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo)){print_r($data->correo);} else {echo 'notiene@notiene.com.mx';} ?>"  id="correo" name="correo" placeholder="">
                            <div id="correo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4>Datos del cheque</h4>
                <hr>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">Banco:</label>
                            <select class="form-control" id="banco" name="banco" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="banco_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Sucursal:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->sucursal)){print_r($data->sucursal);} ?>"  id="sucursal" name="sucursal" placeholder="M0658">
                            <div id="sucursal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Tipo de documento:</label>
                            <select class="form-control" id="tipo_documento" name="tipo_documento" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="tipo_documento_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">N° Documento:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->documento)){print_r($data->documento);} ?>"  id="documento" name="documento" placeholder="0249200126496">
                            <div id="documento_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha del documento:</label>
                            <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->fecha_doc)){print_r($data->fecha_doc);}else {echo '';} ?>" id="fecha_doc" name="fecha_doc" placeholder="">
                            <div id="fecha_doc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Valor del documento:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->pago)){print_r($data->pago);} ?>"  id="pago" name="pago" placeholder="25000">
                            <div id="pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Concepto:</label>
                            <textarea class="form-control" cols="4" id="concepto" name="concepto"><?php if(isset($data->concepto)){print_r($data->concepto);} ?></textarea>
                            <div id="concepto_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                @if(isset($status))
                    @if($status == '0')
                        <br>
                        <h4>Devolución</h4>
                        <hr>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Fecha de la devolución:</label>
                                    <input type="date" class="form-control" value="<?php if(isset($data->fecha_devolucion)){print_r($data->fecha_devolucion);}else {echo date('Y-m-d');} ?>" id="fecha_devolucion" name="fecha_devolucion" placeholder="">
                                    <div id="fecha_devolucion_error" class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="">Motivo de la devolución / rechazo:</label>
                                    <textarea class="form-control" cols="4" id="devolucion" name="devolucion"><?php if(isset($data->devolucion)){print_r($data->devolucion);} ?></textarea>
                                    <div id="devolucion_error" class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                    @endif    
                @endif

                <br>
                <h4>Cheque correspondiente al pago :</h4>
                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Tipo de cuenta:</label>
                            <select class="form-control" id="tipo_cuenta" name="tipo_cuenta" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="tipo_cuenta_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha del pago:</label>
                            <input type="date" class="form-control" value="<?php if(isset($data->fecha_pago)){print_r($data->fecha_pago);}else {echo date('Y-m-d');} ?>" id="fecha_pago" name="fecha_pago" placeholder="">
                            <div id="fecha_pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Monto:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->pago)){print_r($data->pago);} ?>"  id="pago" name="pago" placeholder="5000">
                            <div id="pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Moneda:</label>
                            <select class="form-control" id="modena" name="modena" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="MXN">MXN (Pesos Mexicanos)</option>
                            </select>
                            <div id="modena_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_formulario" class="btn btn-success">Guardar</button>
                        <input type="hidden" id="bloqueo" value="<?php echo isset($bloqueo) ? $bloqueo : "0" ?>">
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!--<script src="{{ base_url('js/facturas/index.js') }}"></script>-->
    <script type="text/javascript">
        if ($("#bloqueo").val() == '1') {
            $("input").attr('disabled',true);
            $("select").attr('disabled',true);
            $("textarea").attr('disabled',true);

            $("#guardar_formulario").css('display','none');
        } 
    </script>
@endsection