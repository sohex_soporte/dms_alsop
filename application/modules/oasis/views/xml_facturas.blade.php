@layout('tema_luna/layout')
<style>
    .bgwhite {
        background-color: #fff !important
    }

</style>
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <hr>
        <div class="col-md-12">
            <form enctype="multipart/form-data" id="subir_factura">
                <div class="form-group">
                    <label for="factura">Factura</label>
                    <input type="file" class="form-control-file" id="xml_remision" name="xml_remision">
                    <div id="xml_remision_error" class="invalid-feedback"></div>
                </div>
                <button type="button" id="btn_subir_remision" class="btn btn-primary">Cargar remision</button>
            </form>
        </div>
        <hr>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Versión</label>
                    {{ $version }}
                </div>
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
                <div class="col-sm-4">
                    <label for="">Folio</label>
                    {{ $folio }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha</label>
                    {{ $fecha }}
                </div>
                <div class="col-sm-4">
                    <label for="">Sello</label>
                    {{ $sello }}
                </div>
                <div class="col-sm-4">
                    <label for="">Certificado</label>
                    {{ $certificado }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">No certificado</label>
                    {{ $no_certificado }}
                </div>
                <div class="col-sm-4">
                    <label for="">Subtotal</label>
                    {{ $subtotal }}
                </div>
                <div class="col-sm-4">
                    <label for="">Tipo de cambio</label>
                    {{ $tipo_cambio }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Moneda</label>
                    {{ $moneda }}
                </div>
                <div class="col-sm-4">
                    <label for="">Total</label>
                    {{ $total }}
                </div>
                <div class="col-sm-4">
                    <label for="">Tipo de comprobante</label>
                    {{ $tipo_comprobante }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Método de pago</label>
                    {{ $metodo_pago }}
                </div>
                <div class="col-sm-4">
                    <label for="">Forma de pago</label>
                    {{ $forma_pago }}
                </div>
                <div class="col-sm-4">
                    <label for="">Lugar de expedición</label>
                    {{ $lugar_expedicion }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">R.F.C. del emisor</label>
                    {{ $emisor_rfc }}
                </div>
                <div class="col-sm-4">
                    <label for="">Nombre del emisor</label>
                    {{ $emisor_nombre }}
                </div>
                <div class="col-sm-4">
                    <label for="">Régimen fiscal del emisor</label>
                    {{ $emisor_regimen_fiscal }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">R.F.C. del receptor</label>
                    {{ $receptor_rfc }}
                </div>
                <div class="col-sm-4">
                    <label for="">Nombre del receptor</label>
                    {{ $receptor_nombre }}
                </div>
                <div class="col-sm-4">
                    <label for="">Régimen fiscal del receptor</label>
                    {{ $receptor_regimen_fiscal }}
                </div>
            </div>
            <div id="tabla-bicicletas"></div>
            <div class="row">
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $('.numeric').numeric();
        $('.positive').numeric();
        $(".busqueda").select2();
        $("#guardar").on('click', async function() {
            //clave_producto_servicio
            let clave_producto_servicio = [];
            $('.js_clave_producto').each(function(index, value){
                clave_producto_servicio[index] = $(this).val();
            });
            console.log("clave",clave_producto_servicio);
            //no_identificacion
            let no_identificacion = [];
            $('.js_no_identificacion').each(function(index, value){
                no_identificacion[index] = $(this).val();
            });
            //Cantidad
            let array_cantidad = [];
            $('.js_cantidad').each(function(index, value){
                array_cantidad[index] = $(this).val();
            });
            //Cantidad
            let clave_unidad = [];
            $('.js_clave_unidad').each(function(index, value){
                clave_unidad[index] = $(this).val();
            });
            //Unidad
            let array_unidad = [];
            $('.js_unidad').each(function(index, value){
                array_unidad[index] = $(this).val();
            });
            //Descripción
            let array_descripcion = [];
            $('.js_descripcion').each(function(index, value){
                array_descripcion[index] = $(this).val();
            });
            //Valor unitario
            let valor_unitario = [];
            $('.js_valor_unitario').each(function(index, value){
                valor_unitario[index] = $(this).val();
            });
            //Descripción
            let array_importe = [];
            $('.js_importe').each(function(index, value){
                array_importe[index] = $(this).val();
            });
            //Brand
            let array_brand = [];
            $('.js_brand').each(function(index, value){
                array_brand[index] = $(this).val();
            });
            //Color
            let array_color_id = [];
            $('.js_color_id').each(function(index, value){
                array_color_id[index] = $(this).val();
            });
            //Modelos
            let array_modelo_id = [];
            $('.js_modelo_id').each(function(index, value){
                array_modelo_id[index] = $(this).val();
            });
            //Tipo bicicleta
            let array_type_id = [];
            $('.js_type_id').each(function(index, value){
                array_type_id[index] = $(this).val();
            });
            const data = {
                version: $("#version").val(),
                serie: $("#serie").val(),
                folio: $("#folio").val(),
                fecha: $("#fecha").val(),
                sello: $("#sello").val(),
                no_certificado: $("#no_certificado").val(),
                certificado: $("#certificado").val(),
                subtotal: $("#subtotal").val(),
                tipo_cambio: $("#tipo_cambio").val(),
                moneda: $("#moneda").val(),
                total: $("#total").val(),
                tipo_comprobante: $("#tipo_comprobante").val(),
                metodo_pago: $("#metodo_pago").val(),
                forma_pago: $("#forma_pago").val(),
                lugar_expedicion: $("#lugar_expedicion").val(),
                emisor_rfc: $("#emisor_rfc").val(),
                emisor_nombre: $("#emisor_nombre").val(),
                emisor_regimen_fiscal: $("#emisor_regimen_fiscal").val(),
                receptor_rfc: $("#receptor_rfc").val(),
                receptor_nombre: $("#receptor_nombre").val(),
                receptor_regimen_fiscal: $("#receptor_regimen_fiscal").val(),
                estatus_id : 1,
                ubicacion_id : 1,
                id_status_unidad : 1,
                ultimo_servicio : "{{date('Y-m-d')}}",
                user_id: "{{ $this->session->userdata('id') }}",
                clave_producto_servicio,
                no_identificacion,
                cantidad : array_cantidad,
                clave_unidad,
                unidad:array_unidad,
                descripcion:array_descripcion,
                valor_unitario,
                importe : array_importe,
                brand : array_brand,
                color_id : array_color_id,
                modelo_id : array_modelo_id,
                type_id : array_type_id,

            }
            console.log('data send...', data);
            if ($("#id").val() != 0) {
                ajax.put('api/unidades/' + $("#id").val(), data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información actualizada con éxito";
                        utils.displayWarningDialog("Información actualizada con éxito", "success",
                            function(
                                data) {
                                return window.location.href = base_url +
                                    'oasis/listado';
                            })
                    })
            } else {
                ajax.post('api/unidades', data,
                    function(response, headers) {
                        console.log('response', response)
                        console.log('headers', headers)
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información guardada con éxito";
                        utils.displayWarningDialog("Información guardada con éxito", "success",
                            function(
                                data) {
                                return window.location.href = base_url +
                                    'oasis/listado';
                            })
                    })
            }

        })
        const saveCat = () => {
            if ($("#catalogo").val() == '' || $("#_clave_vehicular").val() == '' || $("#descripcion").val() == '') {
                alert('Es necesario ingresar todos los campos');
                return;
            }
            const data = {
                cat: $("#catalogo").val(),
                clave_vehicular: $("#_clave_vehicular").val(),
                descripcion: $("#descripcion").val()
            }
            ajax.post('api/lista-precios-un', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success",
                        function(data) {
                            $("#cat").append("<option value='" + response.id + "'>" + response.cat +
                                "</option>");
                            $("#cat").val(response.id);
                            $("#clave_vehicular").val(response.clave_vehicular);
                            $("#unidad_descripcion").val(response.descripcion);
                            $(".close").trigger('click');
                        })
                })
        }
        $("#js_add_cat").on('click', async function() {
            var url = site_url + "/oasis/agregar_cat/0";
            customModal(url, {}, "GET", "lg", saveCat, "", "Guardar", "Cancelar", "Guardar Catálogo",
                "modalAduana");
        });
        const saveColor = () => {
            const data = {
                nombre: $("#color").val(),
                clave: $("#clave_color").val(),
            }
            ajax.post('api/catalogo-colores', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success",
                        function(data) {
                            $("#colorIntId").append("<option value='" + response.id + "'>" + response.nombre +
                                "</option>");
                            $("#colorExtId").append("<option value='" + response.id + "'>" + response.nombre +
                                "</option>");
                            $("#" + id).val(response.id);
                            $(".close").trigger('click');
                        })
                })
        }
        $(".js_add_color").on('click', async function() {
            id = $(this).data('id');
            customModal(site_url + "/oasis/add_color/0", {}, "GET", "lg", saveColor, "", "Guardar color",
                "Cancelar", "Guardar",
                "modalColor");
        });
        $(".search_price").on('change', function() {
            const data = {
                [$(this).prop('id')]: $(this).val()
            }
            ajax.post('api/precios/unidades/get-parameters', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    if (!response.length) {
                        return;
                    } else {
                        const resp = response[0];
                        console.log('response', resp)
                        $("#clave_vehicular").val(resp.clave_vehicular);
                        $("#unidad_descripcion").val(resp.descripcion);
                        $("#costo_valor_unidad").val(resp.precio_lista);
                        $("#venta_valor_unidad").val(resp.precio_cliente);
                    }
                })
        })
        $("#serie").on('change', function() {
            const valor = $(this).val();
            if (valor.length == 17) {
                $("#serie_corta").val(valor.substr(valor.length - 6));
            }
        })
    </script>

    <script>
        $("#btn_subir_remision").on('click', function(e) {
            let factura = $('#xml_remision')[0].files[0];
            let paqueteDeDatos = new FormData();
            paqueteDeDatos.append('xml_remision', factura);
            if (!utils.isDefined(factura) && !factura) {
                return toastr.error("Adjuntar Remision");
            }

            ajax.postFile(`api/unidades/xml-remision`, paqueteDeDatos, function(response, header) {
                console.log("response...")
                console.log("RESPONSE", response);
                if (header.status == 200 || header.status == 204) {
                    rellenar_formularios(response)
                }
                if (header.status == 500) {
                    let titulo = "Error subiendo factura!"
                    utils.displayWarningDialog(titulo, 'warning', function(result) {});
                }
            })
        })

        const rellenar_formularios = (response) => {
            $(".busqueda").select2('destroy');
            const {
                comprobante
            } = response;
            const {
                conceptos
            } = response;
            const {
                emisor
            } = response;
            const {
                receptor
            } = response;

            $("#version").val(comprobante.Version);
            $("#serie").val(comprobante.Serie);
            $("#folio").val(comprobante.Folio);
            $("#fecha").val(comprobante.Fecha);
            $("#sello").val(comprobante.Sello);
            $("#no_certificado").val(comprobante.NoCertificado);
            $("#certificado").val(comprobante.Certificado);
            $("#subtotal").val(comprobante.SubTotal);
            $("#tipo_cambio").val(comprobante.TipoCambio);
            $("#moneda").val(comprobante.Moneda);
            $("#total").val(comprobante.Total);
            $("#tipo_comprobante").val(comprobante.TipoDeComprobante);
            $("#metodo_pago").val(comprobante.MetodoPago);
            $("#forma_pago").val(comprobante.FormaPago);
            $("#lugar_expedicion").val(comprobante.LugarExpedicion);

            //Emisor
            $("#emisor_rfc").val(emisor.Rfc);
            $("#emisor_nombre").val(emisor.Nombre);
            $("#emisor_regimen_fiscal").val(emisor.RegimenFiscal);

            //Receptor
            $("#receptor_rfc").val(receptor.Rfc);
            $("#receptor_nombre").val(receptor.Nombre);
            $("#receptor_regimen_fiscal").val(receptor.UsoCFDI);

            //Cargar las bicicletas
            var url = site_url + "/oasis/cargar_bicicletas/";
            ajaxLoad(url, {
                conceptos,
            }, "tabla-bicicletas", "POST", function() {
                $(".busqueda").select2();

            });

            $(".busqueda").select2();
        }
    </script>
@endsection
