@layout('tema_luna/layout')
@section('contenido')
    <div style="text-align: center;">
        <br>
        <br>
        <br>
        <br>

        <div style="text-align: center" id="qr"></div>
        <strong style="font-size: 65px;">{{ $serie }}</strong>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('assets/libraries/qr/qrcode.js') }}"></script>
    <script>
        const serie = "{{ $serie }}"
        new QRCode(document.getElementById('qr'), {
            width: 180,
            height: 180,
            text: PATH + '/oasis/buscar_serie_qr/' +serie,
        });
    </script>
@endsection
