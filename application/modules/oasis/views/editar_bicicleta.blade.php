@layout('tema_luna/layout')
<style>
    .bgwhite {
        background-color: #fff !important
    }

</style>
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <hr>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Clave de producto</label>
                    {{$clave_producto_servicio}}
                </div>
                <div class="col-sm-4">
                    <label for=""># Identificación</label>
                    {{$no_identificacion}}
                </div>
                <div class="col-sm-4">
                    <label for="">Cantidad</label>
                    {{$cantidad}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Clave unidad</label>
                    {{$clave_unidad}}
                </div>
                <div class="col-sm-4">
                    <label for="">Unidad</label>
                    {{$unidad}}
                </div>
                <div class="col-sm-4">
                    <label for="">Descripción</label>
                    {{$descripcion}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Valor unitario</label>
                   {{$valor_unitario}}
                </div>
                <div class="col-sm-4">
                    <label for="">Importe</label>
                    {{$importe}}
                </div>
                <div class="col-sm-4">
                    <label for="">Brand</label>
                    {{$brand}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Color</label>
                    {{$color_id}}
                </div>
                <div class="col-sm-4">
                    <label for="">Modelo</label>
                    {{$modelo_id}}
                </div>
                <div class="col-sm-4">
                    <label for="">Tipo bicicleta</label>
                    {{$type_id}}
                </div>
            </div>
            <div class="row">
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $('.numeric').numeric();
        $('.positive').numeric();
        $(".busqueda").select2();
        $("#guardar").on('click', async function() {
            const data = {
                clave_producto_servicio: $("#clave_producto_servicio").val(),
                no_identificacion: $("#no_identificacion").val(),
                cantidad: $("#cantidad").val(),
                clave_unidad: $("#clave_unidad").val(),
                unidad: $("#unidad").val(),
                descripcion: $("#descripcion").val(),
                valor_unitario: $("#valor_unitario").val(),
                importe: $("#importe").val(),
                brand: $("#brand").val(),
                color_id: $("#color_id").val(),
                modelo_id: $("#modelo_id").val(),
                type_id: $("#type_id").val(),
            }
            console.log('data send...', data);
            ajax.put('api/detalle-remision/' + $("#id").val(), data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información actualizada con éxito";
                    utils.displayWarningDialog("Información actualizada con éxito", "success",
                        function(
                            data) {
                            return window.location.href = base_url +
                                'oasis/listado';
                        })
                })

        })
    </script>
@endsection
