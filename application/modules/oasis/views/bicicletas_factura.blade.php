<hr>
<br>
@foreach($conceptos as $c => $concepto)
<hr>
<h2>Unidad #{{$c}}</h2>
<div class="row">
    <div class="col-sm-4">
        <label for="">Clave de producto</label>
        <input class="form-control js_clave_producto" type="text" name="ClaveProdServ[{{$c}}]" id="" value="{{$concepto['ClaveProdServ'][0]}}">
    </div>
    <div class="col-sm-4">
        <label for=""># Identificación</label>
        <input class="form-control js_no_identificacion" type="text" name="NoIdentificacion[{{$c}}]" id="" value="{{$concepto['NoIdentificacion'][0]}}">
    </div>
    <div class="col-sm-4">
        <label for="">Cantidad</label>
        <input class="form-control js_cantidad" type="text" name="Cantidad[{{$c}}]" id="" value="{{$concepto['Cantidad'][0]}}">
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <label for="">Clave unidad</label>
        <input class="form-control js_clave_unidad" type="text" name="ClaveUnidad[{{$c}}]" id="" value="{{$concepto['ClaveUnidad'][0]}}">
    </div>
    <div class="col-sm-4">
        <label for="">Unidad</label>
        <input class="form-control js_unidad" type="text" name="Unidad[{{$c}}]" id="" value="{{$concepto['Unidad'][0]}}">
    </div>
    <div class="col-sm-4">
        <label for="">Descripción</label>
        <input class="form-control js_descripcion" type="text" name="Descripcion[{{$c}}]" id="" value="{{$concepto['Descripcion'][0]}}">
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <label for="">Valor unitario</label>
        <input class="form-control js_valor_unitario" type="text" name="ValorUnitario[{{$c}}]" id="" value="{{$concepto['ValorUnitario'][0]}}">
    </div>
    <div class="col-sm-4">
        <label for="">Importe</label>
        <input class="form-control js_importe" type="text" name="Importe[{{$c}}]" id="" value="{{$concepto['Importe'][0]}}">
    </div>
    <div class="col-sm-4">
        <label for="">Brand</label>
        <input class="form-control js_brand" type="text" name="brand[{{$c}}]" id="" value="">
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <label for="">Color</label>
        <select class="form-control busqueda js_color_id" name="color_id[{{$c}}]" id="color_id[{{$c}}]">
            <option value="">-- Selecciona un color -- </option>
            @foreach($colores as $col => $color)
             <option value="{{$color->id}}">{{$color->nombre}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-4">
        <label for="">Modelo</label>
        <select class="form-control busqueda js_modelo_id" name="modelo_id[{{$c}}]" id="modelo_id[{{$c}}]">
            <option value="">-- Selecciona un modelo -- </option>
            @foreach($modelos as $m => $modelo)
             <option value="{{$modelo->id}}">{{$modelo->descripcion}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-4">
        <label for="">Tipo bicicleta</label>
        <select class="form-control busqueda js_type_id" name="type_id[{{$c}}]" id="type_id[{{$c}}]">
            <option value="">-- Selecciona un tipo de bicicleta -- </option>
            @foreach($tipo_bicicletas as $t => $tipo)
             <option value="{{$tipo->id}}">{{$tipo->tipo_bicicleta}}</option>
            @endforeach
        </select>
    </div>
</div>
@endforeach


