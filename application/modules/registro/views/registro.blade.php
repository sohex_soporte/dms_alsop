@layout('tema_luna/layout_registro')
@section('contenido')

<section class="content-registro" style="background: white;padding: 3rem;">
    <div class="view-header">
        <div class="header-icon">
            <i class="pe page-header-icon pe-7s-add-user"></i>
        </div>
        <div class="header-title">
            <h3>Registro</h3>
            <small> Por favor introduce tus datos de registro.</small>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php echo renderInputText("text", "nombre", "Nombre", ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "apellido_paterno", "Apellido paterno", ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "apellido_materno", "Apellido materno", ''); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php echo renderInputText("text", "rfc", "RFC", ''); ?>
        </div>
        <div class="col-md-6">
            <?php echo renderInputText("number", "telefono", "Telefono", ''); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo renderInputText("email", "email", "Email", ''); ?>
        </div>
        <div class="col-md-12">
            <?php echo renderInputText("text", "usuario", "Usuario", ''); ?>
            <?php echo renderInputText("password", "password", "Password", ''); ?>

            <div class="form-group">
                <label for="">Confirmar password</label>
                <input type="password" name="password_confirm" id="password_confirm" class="form-control">
                <small id="msg_confirmar" class="text-muted"></small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="button" id="guardar_registro" class="btn btn-accent col-md-2"> Guardar</button>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script>
    $("#password_confirm").on('keyup', function() {
        let password = document.getElementById("password").value;
        let password_confirm = document.getElementById("password_confirm").value;
        if (password !== password_confirm) {
            return $("#msg_confirmar")
                .text("Los password no coinciden")
                .attr("css", {
                    "color": 'red'
                })
                
                $("#guardar_registro").attr("disabled",true);
        }

        if (password === password_confirm) {
            return $("#msg_confirmar").text("");
            $("#guardar_registro").attr("disabled",false);
        }
    });

    $("#guardar_registro").on('click', function() {
        $(".invalid-feedback").html("");
        if (document.getElementById("password").value !== document.getElementById("password_confirm").value) {

            utils.displayWarningDialog("La contraseña no coincide", "warning")
        }
        ajax.post('api/usuarios/registrar', {
            nombre: document.getElementById("nombre").value,
            apellido_paterno: document.getElementById("apellido_paterno").value,
            apellido_materno: document.getElementById("apellido_materno").value,
            usuario: document.getElementById("usuario").value,
            password: document.getElementById("password").value,
            rfc: document.getElementById("rfc").value,
            email: document.getElementById("email").value,
            rol_id: 10
        }, function(response, headers) {
            if (headers.status == 200) {
                var titulo = "Usuario registrado";
                utils.displayWarningDialog(titulo, "success", function(data) {
                     window.location.href = base_url + 'login';
                })

            }
        })
    });
</script>
@endsection