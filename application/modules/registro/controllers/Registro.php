 <?php

defined('BASEPATH') or exit('No direct script access allowed');
class Registro extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  public function index()
  {
    $this->load->library('curl');
    $this->load->helper('general');
    $data['modulo'] = "";
    $data['subtitulo']  = "";
    $data['titulo'] = "";
    $this->blade->render('registro/registro', $data);
  }
}
