<!DOCTYPE html>
<html>
<head>
    @include('tema_luna/head')
</head>
<body>
    <!-- Wrapper-->
    <div class="wrapper">
        <!-- Header-->
         <nav class="navbar navbar-expand-md navbar-default fixed-top"> 
            <div class="navbar-header">
                <div id="mobile-menu">
                    <div class="left-nav-toggle">
                        <a href="#">
                            <i class="stroke-hamburgermenu"></i> 
                        </a>
                    </div>
                </div>
                <a class="navbar-brand" href="{{ base_url('login') }}" style="background: #6ba0cb; padding: 0; display: flex; text-align: center; align-items: center; justify-content: center;">
                    <img src="<?php echo base_url()?>luna/logodms.png" width="90%">
                </a>
            </div>
             
        </nav>
        <!-- End header-->

         <!--Navigation-->
          
         <!--End navigation-->
         <!--Main content-->
        <section class="content"> 
            <div class="container-fluid"> 
                 @yield('contenido') 
            </div>
        </section>
        <!-- End main content-->
    </div>
    <!-- End wrapper-->
    <!-- Vendor scripts -->
    @include('tema_luna/dependencias_footer')
    @yield('modal')
    @yield('scripts')
</body>

</html>