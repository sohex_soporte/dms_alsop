var _appsFunction = function () {
	this.init = function () {
		$('select[name=id_departamento]').attr('onchange','Apps.buscar_trabajadores();');
        $('select[name=id_puesto]').attr('onchange','Apps.buscar_trabajadores();');
        $('select[name=jornada]').attr('onchange','Apps.buscar_trabajadores();');


		Apps.check_percepcion_deduccion();
		Apps.ini_listado();

	}
	this.cargarDatos = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
	},

	this.buscar_trabajadores = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/reportes/horas_extras/index_trabajadores',
            data: {
                'id_departamento': $('select[name=id_departamento] option:selected').val(),
                'id_puesto': $('select[name=id_puesto] option:selected').val(),
                // 'jornada': $('select[name=jornada] option:selected').val()
            },
            success: function (response, status, xhr) {
                if(response.data == false){
                    $('div#listado_trabajadores').html('');
                    toastr.warning('No se encontraron trabajadores');
                }else{
                    var template = document.getElementById('tmpl_trabajadores').innerHTML;
                    var rendered = Mustache.render(template, response );
                    $('div#listado_trabajadores').html(rendered);
                }
            }
        });
    },

	this.descargar = function(){
		var data_save = $('form#form_content').serializeArray();
		data_save.push({ name: "departamento_desc", value: $('select[name=id_departamento] option:selected').text() });
		data_save.push({ name: "puesto_desc", value: $('select[name=id_puesto] option:selected').text() });
		data_save.push({ name: "jornada_desc", value: $('select[name=jornada] option:selected').text() });

		$.fileDownload( PATH + '/nomina/reportes/movimientos_trabajador/index_pdf',{
			httpMethod: "POST",
			data: data_save,
		}).done(function () {  }).fail(function () {  });
 
    	return false; //this is critical to stop the click event which will trigger a normal file download
	},

	this.check_percepcion_deduccion = function(){
		var pyd = $('input:radio[name=tipo_pyd]:checked').val();
		switch (pyd) {
			case '2':
				$('input[clave=D]').prop( "checked", false );
				$('input[clave=P]').prop( "checked", true );
				break;
			case '3':
				$('input[clave=P]').prop( "checked", false );
				$('input[clave=D]').prop( "checked", true );
				break;
			default:
				$('input.id_pyd').prop( "checked", true );
				break;
		}
	},
	this.ini_listado = function(){
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[0, 'asc']],
			ajax: {
				url: PATH + '/nomina/reportes/movimientos_trabajador/index_get',
				type: "POST",
				data: function(){
					return $('form#form_content').serializeArray();
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave_trabajador'
				},
				{
					title: 'Nombre del trabajador',
					render: function ( data, type, row, meta ) {
						var nombre = [row.Nombre, row.Apellido_1, row.Apellido_2];
						return nombre.join(' ');
					}
				},
				{
					title: 'Mov. Deducc. o Percep.',
					render: function ( data, type, row, meta ) {
						var nombre = [row.Clave_pyd, row.Descipcion_pyd];
						return nombre.join(' ');
					}
				},
				{
					title: 'J. Completa',
					data: 'JornadaCompleta'
				},
				{
					title: 'F. Inicio',
					data: 'FechaInicio',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'F. Termino',
					data: 'FechaTermino',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'Monto Mov.',
					data: 'MontoMov',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Monto Acum.',
					data: 'MontoAcum',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Monto Limite',
					data: 'MontoLimite',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Saldo',
					data: 'Saldo',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				

				
				
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});

		// var table = $('table#listado').DataTable();
		// $('table#listado').on( 'page.dt', function () {
			
		// 	var info = table.page.info();
		// 	//$('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
		// } );
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();	
	Apps.init();
});
