var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,

			ajax: {
				url: PATH + '/nomina/catalogosconsultas/calendarios/index_get',
				type: "POST"
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Descripción',
					'data': 'Descripcion'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/catalogosconsultas/calendarios/cambios/' + data.id + "' title='Cambios de calendarios' > <i class='fas fa-pencil-alt'></i> </a>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
