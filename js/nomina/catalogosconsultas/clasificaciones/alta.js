var _appsFunction = function () {
	this.init = function () {},

		this.guardar = function () {
			$.ajax({
				dataType: "json",
				type: 'POST',
				"url": PATH + '/nomina/api/api/runner/clasificaciones/post',
				data: $('form#formContent').serializeArray(),
				success: function (response, status, xhr) {
					if (response.status == 'success') {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							window.location.href = PATH + '/nomina/catalogosconsultas/clasificaciones/index?id=' + response.data.id;
						});
					}
				}

			});
		},
		this.render = function(){
			$.ajax({
				dataType: "json",
				type: 'GET',
				"url": PATH + '/nomina/api/api/runner/clasificaciones/clave',
				success: function (response, status, xhr) {
					if(response.status == 'success'){
						var template = $('script#template').html()
						var storange = {
							Clave: response.data.Clave
						};
						var renderedContent = Mustache.render(template,storange);
						$('form#formContent > div#contenedor').html(renderedContent);
					}
				}

			});
		}

}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.render();
});
