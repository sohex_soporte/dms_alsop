var _appsFunction = function () {
	this.init = function () {

		var change_input = {
			"Logo": {type: "hidden"},
			"LogoArchivo": {type: "hidden"},
			"NumeroEmpresa": {type: "number",max:"10",min:"1"},
			"NoDeSerie": {type: "number",max:"2",min:"1"},
			"CURP": {type: "text",max:"18",onblur:"Apps.validarCurp(this)"},
			"RFC": {type: "text",onblur:"Apps.validarRfc(this)"}
		};
		update_inputs(change_input);
		update_select2('form#general_form select');
		
		// if($('input[name=LogoArchivo]').val().length > 0 && $('input[name=Logo]').val().length > 0){
		// 	$('label[for=imagen_logo]').html( $('input[name=Logo]').val() );
		// }
		
	},

	this.adjuntar_archivo = function($this){
		try {
			var f = $this.files[0];
			var filename = $($this).val().split('\\').pop();

			if (f.size > 8388608 || f.fileSize > 8388608)
			{
			$('small#msg_Logo').html("Se superó el tamaño de archivo permitido. (Max. 8 MB)");
			$this.value = null;
			$('label[for=imagen_logo]').html('');
			}else{

				var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];    
				if ($this.type == "file") {
					var sFileName = $this.value;
					if (sFileName.length > 0) {
						var blnValid = false;
						for (var j = 0; j < _validFileExtensions.length; j++) {
							var sCurExtension = _validFileExtensions[j];
							if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
								blnValid = true;
								break;
							}
						}
						
						if (!blnValid) {
							$('small#msg_Logo').html("El archivo no es válido, las extensiones permitidas son: " + _validFileExtensions.join(", "));
							$this.value = null;
							return false;
						}
					}
				}

				var reader = new FileReader();
				reader.onload = function (e) {
					$('#preview_imagen').attr('src', e.target.result);
				}
				reader.readAsDataURL($this.files[0]);
			
				$('small#msg_Logo').html("");
				$('input[name=Logo]').val(filename);
				$('label[for=imagen_logo]').html(filename);
				return true;	
			}
		} catch (error) {
			$('small#msg_Logo').html("");
			$this.value = null;
			$('label[for=imagen_logo]').html('');
			$('#preview_imagen').attr('src', null);
		}
		
	}

	this.validarCurp = function (input) {
		return;
		var curp = input.value.toUpperCase(),valido = "No válido";
		if (curpValida(curp)) {
			$('#msg_CURP').html(curp + " formato " + valido);
		}else{
			$('#msg_CURP').html("");
		}
	},
	this.validarRfc = function (input) {
		return;
		var rfc = input.value.toUpperCase(),valido = "No válido";
		if (rfcValido(rfc)) {
			$('#msg_RFC').html(rfc + " formato " + valido);
		}else{
			$('#msg_RFC').html("");
		}
	},
	this.guardar = function () {

		Apps.guardarFormulario();

		// if($('input#imagen_logo').val().length > 1){
		// 	var data, xhr;
		// 	data = new FormData();
		// 	data.append( 'file', $('input#imagen_logo')[0].files[0] );
		// 	xhr = new XMLHttpRequest();
		// 	xhr.open( 'POST', PATH+'nomina/api/api/upload_file', true );
		// 	xhr.onreadystatechange = function () {
		// 		if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
		// 			var response = JSON.parse(xhr.responseText);
		// 			$('input[name=LogoArchivo]').val(response.data.archivo);
		// 			setTimeout(() => {
		// 				Apps.guardarFormulario();
		// 			}, 100);
		// 		}
		// 	};
		// 	xhr.send( data );
		// }else{
		// 	Apps.guardarFormulario();
		// }
		

	}	

	this.guardarFormulario = function(){

		 var data_send =  $('form#general_form').serializeArray();
		 data_send.push({ 'name': 'id','value':id });

        $.ajax({
            dataType: "json",
			cache: false,
			type: 'POST',
            url: PATH + '/nomina/configuracion/datos_empresa/editar_guardar',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					});
				}

            }

        });
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
