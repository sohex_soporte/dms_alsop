var _appsFunction = function () {
    this.fecha_hoy,
    this.init = function () {

        var change_input = {
            "DiasDuracion": {type: "number",'min':'0','value':'1','onblur':'Apps.calcularDiasContrato();'},
            "FechaInicio": {type: "date",'readonly':true},
        };
        
        $('div#exampleModalCenter').modal('hide')
        $('div#exampleModalCenter').on('show.bs.modal', function (event) {
            setTimeout(() => {
                var fecha_alta = $('input[name=FechaAlta]').val();
                $('input[name=FechaInicio]').val(fecha_alta);
                update_inputs(change_input);
                Apps.calcularDiasContrato();
                update_select2('form#contrato_trabajador select');
                $('select[name=id_TipoContratoGen]').attr('onchange','Apps.actualizarCamposContrato()');
                Apps.actualizarCamposContrato();
            },300);
         })
        
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        this.fecha_hoy =  yyyy + '-' + mm + '-' + dd;  
    },

    this.calcularDiasContrato = function(){
        var fecha_alta = $('input[name=FechaInicio]').val();
        var DiasDuracion = $('input[name=DiasDuracion]').val();
        var new_date = moment(fecha_alta)
        if(new_date.isValid()){
            new_date.add('days', DiasDuracion)
            $('input[name=FechaVencimiento]').val( new_date.format('YYYY-MM-DD') );
            $('input#FechaVencimiento').val( new_date.format('DD/MM/YYYY') );
        }else{
            $('input[name=FechaVencimiento]').val('');
            $('input#FechaVencimiento').val('');
        }
    },

    this.SelectedFile = function() {
        //Toma el archivo elegido por el input 
        $('div#preview_imagen_div').hide();
        var uploadFile = document.getElementById("imagen_logo").files[0];
        // //Este objeto FileReader te permite leer archivos
        // var reader = new FileReader();
        // //Esta función se ejecuta cuando el reader.readAsDataURL termina 
        // reader.onloadend = function (e) {
        //     NombreDeTuVariableAEnviar = e.target.result.split("base64,")[1];
        //     console.log(NombreDeTuVariableAEnviar);
        // }
        // //Aqui comienza a leer el archivo para posteriormente ejecutar la función onloadend
        // reader.readAsDataURL(value);

        // reader.onload = function () {
        //     console.log(reader.result);
        // };
        // reader.onerror = function (error) {
        //     console.log('Error: ', error);
        // };

        if (!window.FileReader) {
            $('small#msg_Fotografia').html('El navegador no soporta la lectura de archivos');
            return;
        }
    
        if (!(/\.(jpg|png|jpeg|gif)$/i).test(uploadFile.name)) {
            $('small#msg_Fotografia').html('El archivo a adjuntar no es una imagen');
        }
        else {
            var img = new Image();
            img.onload = function () {
                var $_this = this;
                // if (this.width.toFixed(0) != 200 && this.height.toFixed(0) != 200) {
                //     alert('Las medidas deben ser: 200 * 200');
                // }
                // else 
                if (uploadFile.size > 5000000)
                {
                    $('small#msg_Fotografia').html('El peso de la imagen no puede exceder los 5mb')
                }
                else {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {

                        var canvas = document.createElement('canvas');
                        var ctx = canvas.getContext('2d');

                        // We set the dimensions at the wanted size.
                        canvas.width = 200;
                        canvas.height = 200;

                        // We resize the image with the canvas method drawImage();
                        ctx.drawImage($_this, 0, 0, 200, 200);

                        var dataURI = canvas.toDataURL();


                        var NombreDeTuVariableAEnviar = e.target.result;
                        $('textarea#Fotografia').val(dataURI);
                        $('img#preview_imagen').attr('src',dataURI);
                        $('div#preview_imagen_div').show();

                        var filename = $('input#imagen_logo').val().split('\\').pop();
                        $('label[for=imagen_logo]').html(filename);
                        $('input[name=fotografia_nombre]').val(filename);
                    }
                    reader.readAsDataURL(uploadFile);
                }
            };
            img.src = URL.createObjectURL(uploadFile);
        }    
    },

    this.actualizarCamposContrato = function(){
        var id = $('select[name=id_TipoContratoGen] option:selected').val();
        switch (id) {
            case '1':
                $('div#form_DiasDuracion_content').hide();
                $('div#form_fvencimiento_content').hide();
                break;
            case '2':
            case '3':
            case '4':
                $('div#form_DiasDuracion_content').show();
                $('div#form_fvencimiento_content').show();
                break;
            default:
                $('div#form_DiasDuracion_content').hide();
                $('div#form_fvencimiento_content').hide();
                break;
        }
    },
    
    this.guardar = function () {
        
        var general = $('form#formContent_General').serializeArray();
        var dataObj_general = {};
        $(general).each(function(i, field){
            dataObj_general[field.name] = $.trim(field.value);
        });

        
        var salarioDias = [];
        $("form#formContent_Salario input:checkbox:checked").each(function(){
            salarioDias.push($(this).val());
        });
        var salario = $('form#formContent_Salario').serializeArray();
        var dataObj_salario = { 'DiasDescanso' : JSON.stringify(salarioDias) };
        $(salario).each(function(i, field){
            dataObj_salario[field.name] = $.trim(field.value);
        });

        var personales = $('form#formContent_Personales').serializeArray();
        var dataObj_personales = {};
        $(personales).each(function(i, field){
            dataObj_personales[field.name] = $.trim(field.value);
        });

        var imss = $('form#formContent_imss').serializeArray();
        var dataObj_imss = {};
        $(imss).each(function(i, field){
            dataObj_imss[field.name] = $.trim(field.value);
        });

        var salud = $('form#formContent_salud').serializeArray();
        var dataObj_salud = {};
        $(salud).each(function(i, field){
            dataObj_salud[field.name] = $.trim(field.value);
        });

        var fiscales = $('form#formContent_fiscales').serializeArray();
        var dataObj_fiscales = {};
        $(fiscales).each(function(i, field){
            dataObj_fiscales[field.name] = $.trim(field.value);
        });
        
        var dataSend = {
            general: dataObj_general,
            salario: dataObj_salario,
            personales: dataObj_personales,
            imss: dataObj_imss,
            salud: dataObj_salud,
            fiscales: dataObj_fiscales
        };
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/guardar_trabajador',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {

                    $('div#exampleModalCenter').modal('show')
                    
                } else {
                    switch (response.tab) {
                        case 1:
                            $('a[href="#generales"]').tab('show')
                            $('a[href="#generales"]').click();
                            break;
                        case 2:
                            $('a[href="#salario"]').tab('show')
                            $('a[href="#salario"]').click();
                            break;
                        case 3:
                            $('a[href="#personales"]').tab('show')
                            $('a[href="#personales"]').click();
                            break;
                        case 4:
                            $('a[href="#imss"]').tab('show')
                            $('a[href="#imss"]').click();
                            break;
                        case 5:
                            $('a[href="#salud"]').tab('show')
                            $('a[href="#salud"]').click();
                            break;
                        case 6:
                            $('a[href="#fiscales"]').tab('show')
                            $('a[href="#fiscales"]').click();
                            break;
                    }

                    if($.type(response.message) == 'object'){
                        
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }

                }
            }

        });
    },

    this.guardar_step_2 = function () {
        
        var general = $('form#formContent_General').serializeArray();
        var dataObj_general = {};
        $(general).each(function(i, field){
            dataObj_general[field.name] = $.trim(field.value);
        });

        
        var salarioDias = [];
        $("form#formContent_Salario input:checkbox:checked").each(function(){
            salarioDias.push($(this).val());
        });
        var salario = $('form#formContent_Salario').serializeArray();
        var dataObj_salario = { 'DiasDescanso' : JSON.stringify(salarioDias) };
        $(salario).each(function(i, field){
            dataObj_salario[field.name] = $.trim(field.value);
        });

        var personales = $('form#formContent_Personales').serializeArray();
        var dataObj_personales = {};
        $(personales).each(function(i, field){
            dataObj_personales[field.name] = $.trim(field.value);
        });

        var imss = $('form#formContent_imss').serializeArray();
        var dataObj_imss = {};
        $(imss).each(function(i, field){
            dataObj_imss[field.name] = $.trim(field.value);
        });

        var salud = $('form#formContent_salud').serializeArray();
        var dataObj_salud = {};
        $(salud).each(function(i, field){
            dataObj_salud[field.name] = $.trim(field.value);
        });

        var fiscales = $('form#formContent_fiscales').serializeArray();
        var dataObj_fiscales = {};
        $(fiscales).each(function(i, field){
            dataObj_fiscales[field.name] = $.trim(field.value);
        });

        var fiscales = $('form#contrato_trabajador').serializeArray();
        var contrato_trabajador = {};
        $(fiscales).each(function(i, field){
            contrato_trabajador[field.name] = $.trim(field.value);
        });
        
        var dataSend = {
            general: dataObj_general,
            salario: dataObj_salario,
            personales: dataObj_personales,
            imss: dataObj_imss,
            salud: dataObj_salud,
            fiscales: dataObj_fiscales,
            contrato: contrato_trabajador
        };
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/guardar_trabajador2',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {

                    Swal.fire({
                        icon: 'success',
                        title: '',
                        text: response.message,
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        window.location.href = PATH + '/nomina/inicio/trabajador/index?id=' + response.data.general;
                    });
                    
                } else {
                    $('div#exampleModalCenter').modal('hide');
                    switch (response.tab) {
                        case 1:
                            $('a[href="#generales"]').tab('show')
                            $('a[href="#generales"]').click();
                            break;
                        case 2:
                            $('a[href="#salario"]').tab('show')
                            $('a[href="#salario"]').click();
                            break;
                        case 3:
                            $('a[href="#personales"]').tab('show')
                            $('a[href="#personales"]').click();
                            break;
                        case 4:
                            $('a[href="#imss"]').tab('show')
                            $('a[href="#imss"]').click();
                            break;
                        case 5:
                            $('a[href="#salud"]').tab('show')
                            $('a[href="#salud"]').click();
                            break;
                        case 6:
                            $('a[href="#fiscales"]').tab('show')
                            $('a[href="#fiscales"]').click();
                            break;
                    }

                    if($.type(response.message) == 'object'){
                        
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }

                }
            }

        });
    },

    this.guardar_step = function(){

        var dataSend = $('form#contrato_trabajador').serializeArray();
        dataSend.push({name: 'id', value: identity});

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/trabajador/alta_guardar',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_step_2();
            }

        });
    },

    this.guardar_general = function () {

        if($('small.form-text.text-danger').length > 0){
            $('small.form-text.text-danger').each(function() {
                $( this ).empty();
            });
        }
        
        var dataSend = $('form#formContent_General').serializeArray();
        dataSend.push({name: 'id', value: identity});
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosgenerales/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_salario();
            },error: function (xhr, status, errors) {
                
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {
                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }

                    $('a[href="#generales"]').tab('show')
                    $('a[href="#generales"]').click();
                }else{
                    Apps.guardar_salario();
                }
                
            }

        });
    },

    this.factor_integracion = function () {

        if($.trim($('select[name=id_TablaSistema] option:selected').val()).length > 0 && $.trim($('input[name=FechaAlta]').val()).length > 0 && $.trim($('input[name=SalarioDia]').val()).length > 0){

            if($('small.form-text.text-danger').length > 0){
                $('small.form-text.text-danger').each(function() {
                    $( this ).empty();
                });
            }
            
            var dataSend = [];
            dataSend.push({name: 'catalogo', value: $('select[name=id_TablaSistema] option:selected').val() });
            dataSend.push({name: 'fecha_alta', value: $('input[name=FechaAlta]').val() });
            dataSend.push({name: 'monto', value: $('input[name=SalarioDia]').val() });

            $.ajax({
                dataType: "json",
                type: 'POST',
                url: PATH + '/nomina/api/calculos/factor_integracion',
                data: dataSend,
                success: function (response, status, xhr) {
                    // Apps.guardar_salario();
                    $('input[name=Calculado]').val(response.data.sdi_round);
                    $('input[name=SDICapturado]').val(response.data.sdi_round);

                },
                // error: function (xhr, status, errors) {
                    
                //     var response = JSON.parse(xhr.responseText);
                //     if (response.validation == true) {
                //         if($.type(response.message) == 'object'){
                //             $.each(response.message, function(index, value) {
                //                 if ($('small#msg_' + index).length) {
                //                     $('small#msg_' + index).html(value);
                //                 }else{
                //                     toastr["error"](value);
                //                 }
                //             });
                //             toastr["error"]("Es necesario verificar la información");
                //         }else if($.type(response.message) == 'string'){
                //             toastr["error"](response.message);
                //         }

                //         $('a[href="#generales"]').tab('show')
                //         $('a[href="#generales"]').click();
                //     }else{
                //         Apps.guardar_salario();
                //     }
                    
                // }

            });
        }
    },

    this.guardar_salario = function () {

        var salarioDias = [];
        $("form#formContent_Salario input:checkbox:checked").each(function(){
            salarioDias.push($(this).val());
        });
        var dataSend = $('form#formContent_Salario').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        dataSend.push({name: 'DiasDescanso', value: (salarioDias.length > 0)? JSON.stringify(salarioDias) : '' });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_salario/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_personales();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#salario"]').tab('show')
                    $('a[href="#salario"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_personales();
                }
                
            }

        });
    },

    this.guardar_personales = function () {

        var salarioDias = [];
        $("form#formContent_Salario input:checkbox:checked").each(function(){
            salarioDias.push($(this).val());
        });
        
        var dataSend = $('form#formContent_Personales').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        dataSend.push({name: 'DiasDescanso', value: (salarioDias.length > 0)? JSON.stringify(salarioDias) : '' });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datospersonales/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_imss();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#personales"]').tab('show')
                    $('a[href="#personales"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_imss();
                }
                
            }

        });
    },

    this.guardar_imss = function () {

        var dataSend = $('form#formContent_imss').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosimss/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_salud();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#imss"]').tab('show')
                    $('a[href="#imss"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_salud();
                }
                
            }

        });
    },

    this.guardar_salud = function () {

        var dataSend = $('form#formContent_salud').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datossalud/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_fiscales();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#salud"]').tab('show')
                    $('a[href="#salud"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_fiscales();
                }
                
            }

        });
    },

    this.guardar_fiscales = function () {

        var dataSend = $('form#formContent_fiscales').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosfiscales/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.finalizar_guardado();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#fiscales"]').tab('show')
                    $('a[href="#fiscales"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.finalizar_guardado();
                }
                
            }

        });
    },

    this.finalizar_guardado = function(){
        Swal.fire({
            icon: 'success',
            title: '',
            text: 'El registro se ha actualizado correctamente',
            confirmButtonText: "Aceptar"
        }).then((result) => {
            window.location.href = PATH + '/nomina/inicio/trabajador/index';
        });
    },


    this.LoadDatosGenerales = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosgenerales/find',
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    /** ************************************************************************************** */
                    $('input[name=Clave]').val(response.extends.Clave);
                    var datosGenerales_tpl = $('script#datosGenerales').html()
                    var storangeGenerales = {
                        Nombre: '',
                        Apellido1: '',
                        Apellido2: '',
                        FechaNacimiento: '',
                        EntidadesNacimiento: response.extends.EntidadesNacimiento,
                        NumeroIMSS: '',
                        Genero: response.extends.Genero,
                        RFC: '',
                        CURP: '',
                        Clasificaciones: response.extends.Clasificaciones,
                        Departamentos: response.extends.Departamentos,
                        Puestos: response.extends.Puestos,
                        FechaAlta: Apps.fecha_hoy
                    };
                    var renderedContent = Mustache.render(datosGenerales_tpl,storangeGenerales);

                    $('form#formContent_General').html(renderedContent);
                    $('div#preview_imagen_div').hide();
                    $('form#formContent_General select')
                        .select2({    
                            language: {
                            noResults: function() { return "No hay resultados"; },
                            searching: function() { return "Buscando.."; }
                            }
                        })
                        .val('')
                        .change();
                    
                    var change_input = {
                        "FechaNacimiento": {max: Apps.fecha_hoy},
                        "CURP": {maxlength: 18},
                        "FechaAlta": {onblur:'Apps.calcularFechaAlta();'},
                    };
                    update_inputs(change_input);
                }
            }

        });
    },
    this.LoadSalario = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_salario/find',
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var datosSalario_tpl = $('script#datosSalario').html()
                    var storangeSalario = {
                        

                        JornadaCompleta: response.data.JornadaCompleta,
                        SalarioDia: response.extends.ParametrosNominaGen.SalarioMinimoSM,
                        SalarioHora: (response.extends.ParametrosNominaGen.SalarioMinimoSM / response.extends.ParametrosNominaGen.HorasTrabajo ),
                        HorasDia: response.extends.ParametrosNominaGen.HorasTrabajo,
                        DiasSemana: response.extends.ParametrosNominaGen.DiasSemana,
                        DiasPeriodo: response.extends.ParametrosNominaGen.DiasPago,
                        FechaAplicacion: moment(response.data.FechaAplicacion).format('YYYY-MM-DD'),
                        id_FormaPago: response.data.FormaPago,
                        

                        FormaPago: response.extends.FormaPago,
                        BancoOperador: response.extends.BancoOperador,
                        BaseCotizacion: response.extends.BaseCotizacion,
                        TablaSDI: response.extends.TablasSistema
                    };
                    renderedContent = Mustache.render(datosSalario_tpl,storangeSalario);
                    $('form#formContent_Salario').html(renderedContent);
                    $('form#formContent_Salario select#JornadaCompleta').attr('attr-id','si');
                    $('select[name=id_BaseCotizacion]').attr('attr-id','1');
                    $('select[name=FormaPago]').attr('attr-id','1');
                    $('select[name=id_TablaSistema]').attr('attr-id','4');
                    // Apps.factor_integracion();
                    setTimeout(() => {
                        Apps.factor_integracion(); 
                    }, 1000);
                        
                    $('form#formContent_Salario select').each(function( index ) {
                        var id = $(this).attr('attr-id');
                        if($.trim(id).length > 0){
                            $(this).find('option[value="'+id+'"]').attr('selected', 'selected');
                            $(this).select2({    
                                language: {
                                    noResults: function() { return "No hay resultados"; },
                                    searching: function() { return "Buscando.."; }
                                }
                            });
                        }else{
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            }).val('').change();
                        }                        
                    });

                    //$('input[name=DiasPeriodo]').removeClass('form-control');
                    var max_salario = $('input[name=SalarioDia]').val();
                    var fecha_alta = $('input[name=FechaAlta]').val();

                    $('select[name=JornadaCompleta]').attr('onchange','Apps.jornada_completa();');
                    $('select[name=id_TablaSistema]').attr('onchange','Apps.factor_integracion();');
                    
                    var change_input = {
                        "SalarioHora": {type:'','class':'form-control-plaintext',readonly:'readonly'},
                        "SalarioDia": {type:'number',step:'0.01',min:'1',onblur:'Apps.CalcularDia(this);',min:max_salario},
                        "HorasDia": {type:'',class:'form-control-plaintext',step:'0.01',min:'1',onblur:'Apps.CalcularHoras(this);',max:24},
                        "DiasSemana": {type:'',class:'form-control-plaintext',step:'1',min:'1',max:'7',onblur:'Apps.calcularDiasSemana();'},
                        "DiasPeriodo": {type:'','class':'form-control-plaintext',readonly:'readonly'},
                        "FechaAplicacion": {type:'date',value:fecha_alta,min:fecha_alta},

                        "SDICapturado": {type:'','class':'form-control-plaintext',readonly:'readonly'},
                    };
                    update_inputs(change_input);
                    Apps.calcularDiasSemana();
                    
                }
            }
        });
    },

    this.jornada_completa = function(){
        var opcion = $('select[name=JornadaCompleta] option:selected').val();
        if(opcion == 'si'){
            var change_input = {
                "HorasDia": {type:'','class':'form-control-plaintext',readonly:true},
                "DiasSemana": {type:'','class':'form-control-plaintext',readonly:true}
            };
        }else{
            var change_input = {
                "HorasDia": {type:'number','class':'form-control',readonly:false},
                "DiasSemana": {type:'number','class':'form-control',readonly:false}
            };

        }
        
        update_inputs(change_input);
    }

    this.calcularDiasSemana = function(){
        $('form#formContent_Salario input[type=checkbox]').attr('checked',false);
        var max = $('input[name=DiasSemana]').val();
        $('form#formContent_Salario input[type=checkbox]').each(function( index ) {
            var $this = this;
            if(index < max){
                $($this).attr('checked',true); 
            }
        });

    },

    this.calcularFechaAlta = function(){
        var fecha_alta = $('input[name=FechaAlta]').val();
        $('input[name=FechaAplicacion]').val(fecha_alta);
        $('input[name=FechaInicio]').val(fecha_alta);
        $('input[name=FechaAplicacion]').attr('min',fecha_alta);
        Apps.factor_integracion();
    },

    this.CalcularDia = function(){
        var salarioDia = $('input[name=SalarioDia]').val();
        var horas = $('input[name=HorasDia]').val();
        var salarioHora = parseFloat(salarioDia/horas).toFixed(2);
        $('input[name=SalarioHora]').val(salarioHora);
        Apps.factor_integracion();

    },

    this.CalcularHoras = function(){
        var salarioDia = $('input[name=SalarioDia]').val();
        var horas = $('input[name=HorasDia]').val();
        var salarioHora = parseFloat(salarioDia/horas).toFixed(2);
        $('input[name=SalarioHora]').val(salarioHora);
        Apps.factor_integracion();
    },

    this.LoadDatosPersonales = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datospersonales/find',
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var datosPersonales_tpl = $('script#datosPersonales').html()
                    var storangePersonales = {
                        EntidadesFederativa: response.extends.EntidadesFederativa,
                        NivelesEstudio: response.extends.NivelesEstudio,
                        EstadoCivil: response.extends.EstadoCivil,
                        TipoSangre: response.extends.TipoSangre
                    };
                    renderedContent = Mustache.render(datosPersonales_tpl,storangePersonales);
                    $('form#formContent_Personales').html(renderedContent);

                    setTimeout(() => {
                        $('div#preview_imagen_div').hide();
                    }, 100);
                }
            }
        });
    },

    this.LoadIMSS = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosimss/find',
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var datosIMSS_tpl = $('script#datosIMSS').html()
                    var storangeIMSS = {
                        TipoEmpleado: response.extends.TipoEmpleado,
                        TipoJornadaImss: response.extends.TipoJornadaImss,
                        TurnosImss: response.extends.TurnosImss,
                        UMF: ''
                    };
                    renderedContent = Mustache.render(datosIMSS_tpl,storangeIMSS);
                    $('form#formContent_imss').html(renderedContent);
                    $('form#formContent_imss select')
                        .select2({    
                            language: {
                            noResults: function() { return "No hay resultados"; },
                            searching: function() { return "Buscando.."; }
                            }
                        })
                        .val('')
                        .change();
                }
            }
        });
    },

    this.LoaSalud = function(){
        var datosSalud_tpl = $('script#datosSalud').html()
        var renderedContent = Mustache.render(datosSalud_tpl,{});
        $('form#formContent_salud').html(renderedContent);
        $('form#formContent_salud select')
                .select2({    
                    language: {
                    noResults: function() { return "No hay resultados"; },
                    searching: function() { return "Buscando.."; }
                    }
                })
                .val('')
                .change();

        // $.ajax({
        //     dataType: "json",
        //     type: 'POST',
        //     url: PATH + '/nomina/api/api/runner/trabajador_datossalud/find',
        //     success: function (response, status, xhr) {
        //         if(response.status == 'success'){
        //             var datosSalud_tpl = $('script#datosSalud').html()
        //             var storangeSalud = {
        //                 // TipoEmpleado: response.data.TipoEmpleado,
        //                 // TipoJornadaImss: response.data.TipoJornadaImss,
        //                 // TurnosImss: response.data.TurnosImss,
        //                 // UMF: ''
        //                 // NivelesEstudio: response.data.NivelesEstudio,
        //                 // EstadoCivil: response.data.EstadoCivil,
        //                 // TipoSangre: response.data.TipoSangre
        //             };
        //             renderedContent = Mustache.render(datosSalud_tpl,storangeSalud);
        //             $('form#formContent_salud').html(renderedContent);
            
        //         }
        //     }
        // });
    },

    this.LoadDatosFiscales = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosfiscales/find',
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                        var datosFiscales_tpl = $('script#datosFiscales').html()
                        var storangeFiscales = {
                            RegimenContrato: response.extends.RegimenContrato,
                            EntidadesFederativa: response.extends.EntidadesFederativa,
                            TipoContrato: response.extends.TipoContrato,
                            TipoJornada: response.extends.TipoJornada,

                            // TipoJornadaImss: response.data.TipoJornadaImss,
                            // TurnosImss: response.data.TurnosImss,
                            // UMF: ''
                            // NivelesEstudio: response.data.NivelesEstudio,
                            // EstadoCivil: response.data.EstadoCivil,
                            // TipoSangre: response.data.TipoSangre
                        };
                        var renderedContent = Mustache.render(datosFiscales_tpl,storangeFiscales);
                        $('form#formContent_fiscales').html(renderedContent);
                        $('form#formContent_fiscales select')
                            .select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            })
                            .val('')
                            .change();
                }
            }
        });
    },

    this.showTab = function($this){
        if($($this).attr('loader_select') != 'true'){
            $($this).attr('loader_select','true');
            
            setTimeout(() => {
                var ids = $($this).attr('href');
                $('div.tab-pane'+ids+' form').attr('loader','1');
                $('div.tab-pane'+ids+' form select')
                    .select2({    
                        language: {
                        noResults: function() { return "No hay resultados"; },
                        searching: function() { return "Buscando.."; }
                        }
                    });
            }, 200);
        }

    },

    this.render = function(){
        this.LoadDatosGenerales();
        this.LoadSalario();
        this.LoadDatosPersonales();
        this.LoadIMSS();
        this.LoaSalud();
        this.LoadDatosFiscales();
        // $.ajax({
        //     dataType: "json",
        //     type: 'GET',
        //     url: PATH + '/nomina/api/api/runner/trabajador_datosgenerales/find',
        //     success: function (response, status, xhr) {
        //         if(response.status == 'success'){
        //             /** ************************************************************************************** */
        //             var datosGenerales_tpl = $('script#datosGenerales').html()
        //             var storangeGenerales = {
        //                 Clave: response.extends.Clave,
        //                 Nombre: '',
        //                 Apellido1: '',
        //                 Apellido2: '',
        //                 FechaNacimiento: '',
        //                 EntidadesNacimiento: response.extends.EntidadesNacimiento,
        //                 NumeroIMSS: '',
        //                 Genero: response.extends.Genero,
        //                 RFC: '',
        //                 CURP: '',
        //                 Clasificaciones: response.extends.Clasificaciones,
        //                 Departamentos: response.extends.Departamentos,
        //                 Puestos: response.extends.Puestos,
        //                 FechaAlta: ''
        //             };
        //             var renderedContent = Mustache.render(datosGenerales_tpl,storangeGenerales);

        //             $('form#formContent_General').html(renderedContent);
        //             $('form#formContent_General select')
        //                 .select2({    
        //                     language: {
        //                     noResults: function() { return "No hay resultados"; },
        //                     searching: function() { return "Buscando.."; }
        //                     }
        //                 })
        //                 .val('')
        //                 .change();
        //             return false;
        //             /** ************************************************************************************** */
        //             var datosSalario_tpl = $('script#datosSalario').html()
        //             var storangeSalario = {
        //                 SalarioDia: '',
        //                 SalarioHora: '',
        //                 FormaPago: response.data.FormaPago,
        //                 BancoOperador: response.data.BancoOperador,
        //                 BaseCotizacion: response.data.BaseCotizacion,
        //                 TablaSDI: response.data.TablaSDI

        //             };
        //             renderedContent = Mustache.render(datosSalario_tpl,storangeSalario);
        //             $('form#formContent_Salario').html(renderedContent);
        //             // $('form#formContent_Salario select')
        //             //     .select2({    
        //             //         language: {
        //             //         noResults: function() { return "No hay resultados"; },
        //             //         searching: function() { return "Buscando.."; }
        //             //         }
        //             //     })
        //             //     .val('')
        //             //     .change();
        //             /** ************************************************************************************** */
        //             var datosPersonales_tpl = $('script#datosPersonales').html()
        //             var storangePersonales = {
        //                 EntidadesFederativa: response.data.EntidadesFederativa,
        //                 NivelesEstudio: response.data.NivelesEstudio,
        //                 EstadoCivil: response.data.EstadoCivil,
        //                 TipoSangre: response.data.TipoSangre
        //             };
        //             renderedContent = Mustache.render(datosPersonales_tpl,storangePersonales);
        //             $('form#formContent_Personales').html(renderedContent);
        //             // $('form#formContent_Salario select')
        //             //     .select2({    
        //             //         language: {
        //             //         noResults: function() { return "No hay resultados"; },
        //             //         searching: function() { return "Buscando.."; }
        //             //         }
        //             //     })
        //             //     .val('')
        //             //     .change();

        //             /** ************************************************************************************** */
        //             var datosIMSS_tpl = $('script#datosIMSS').html()
        //             var storangeIMSS = {
        //                 TipoEmpleado: response.data.TipoEmpleado,
        //                 TipoJornadaImss: response.data.TipoJornadaImss,
        //                 TurnosImss: response.data.TurnosImss,
        //                 UMF: ''
        //                 // NivelesEstudio: response.data.NivelesEstudio,
        //                 // EstadoCivil: response.data.EstadoCivil,
        //                 // TipoSangre: response.data.TipoSangre
        //             };
        //             renderedContent = Mustache.render(datosIMSS_tpl,storangeIMSS);
        //             $('form#formContent_imss').html(renderedContent);
        //             // $('form#formContent_Salario select')
        //             //     .select2({    
        //             //         language: {
        //             //         noResults: function() { return "No hay resultados"; },
        //             //         searching: function() { return "Buscando.."; }
        //             //         }
        //             //     })
        //             //     .val('')
        //             //     .change();
                    
        //             /** ************************************************************************************** */
        //             var datosSalud_tpl = $('script#datosSalud').html()
        //             var storangeSalud = {
        //                 // TipoEmpleado: response.data.TipoEmpleado,
        //                 // TipoJornadaImss: response.data.TipoJornadaImss,
        //                 // TurnosImss: response.data.TurnosImss,
        //                 // UMF: ''
        //                 // NivelesEstudio: response.data.NivelesEstudio,
        //                 // EstadoCivil: response.data.EstadoCivil,
        //                 // TipoSangre: response.data.TipoSangre
        //             };
        //             renderedContent = Mustache.render(datosSalud_tpl,storangeSalud);
        //             $('form#formContent_salud').html(renderedContent);
        //             // $('form#formContent_Salario select')
        //             //     .select2({    
        //             //         language: {
        //             //         noResults: function() { return "No hay resultados"; },
        //             //         searching: function() { return "Buscando.."; }
        //             //         }
        //             //     })
        //             //     .val('')
        //             //     .change();

        //             /** ************************************************************************************** */
        //             var datosFiscales_tpl = $('script#datosFiscales').html()
        //             var storangeFiscales = {
        //                 RegimenContrato: response.data.RegimenContrato,
        //                 EntidadesFederativa: response.data.EntidadesFederativa,
        //                 TipoContrato: response.data.TipoContrato,
        //                 TipoJornada: response.data.TipoJornada,
        //                 // TipoJornadaImss: response.data.TipoJornadaImss,
        //                 // TurnosImss: response.data.TurnosImss,
        //                 // UMF: ''
        //                 // NivelesEstudio: response.data.NivelesEstudio,
        //                 // EstadoCivil: response.data.EstadoCivil,
        //                 // TipoSangre: response.data.TipoSangre
        //             };
        //             renderedContent = Mustache.render(datosFiscales_tpl,storangeFiscales);
        //             $('form#formContent_fiscales').html(renderedContent);
        //             // $('form#formContent_Salario select')
        //             //     .select2({    
        //             //         language: {
        //             //         noResults: function() { return "No hay resultados"; },
        //             //         searching: function() { return "Buscando.."; }
        //             //         }
        //             //     })
        //             //     .val('')
        //             //     .change();
        //         }
        //     }

        // });

        
    }

}

var Apps;
$(function () {
    Apps = new _appsFunction();
    Apps.init();
    Apps.render();
});
