var _appsFunction = function () {
	this.init = function () {},

	this.get = function () {
		
		$('table#table_content').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "desc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/movientos_nomina/index_get',
				type: "POST",
				data: function(){
					return {
						id: identity
					};
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave_PyD'
				},
				{
					title: 'Descripción',
					data: 'Descripcion_PyD'
				},
				{
					title: 'Tipo',
					data: 'Descripcion_MovAplicacion'
				},
				{
					title: 'Formula',
					data: function(){
						return '-';
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#table_content').append(
					$('<tfoot/>').append( $("table#table_content thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
	
});
