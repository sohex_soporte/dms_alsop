google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawStuff);

function drawStuff() {
var data = new google.visualization.arrayToDataTable(contenido);


var options = {
    title: 'Total de trabajadores por estatus',
    width: 900,
    legend: { position: 'none' },
    chart: { title: 'Total de trabajadores por estatus',
            subtitle: '' },
    bars: 'vertical', // Required for Material Bar Charts.
    axes: {
    y: {
        0: { side: 'buttom', label: 'Total de trabajadores'} // Top x-axis.
    }
    },
    bar: { groupWidth: "90%" }
};

var chart = new google.charts.Bar(document.getElementById('top_x_div'));
chart.draw(data, options);
};