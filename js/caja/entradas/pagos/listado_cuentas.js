function inicializaTabla() {
    $('#tbl_cxc').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'desc']
        ],
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            // Remove the formatting to get integer data for summation
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            total_pago = api
                .column(6)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_pagar_tabla").html('$ <span class="money_format">' + total_pago.toFixed(2) + '</span>');

        },
        "ajax": {
            url: PATH_API + "api/cuentas-por-cobrar",
            type: 'GET',
            order: 0
        },
        columns: [{
                title: "#",
                render: function(data, type, row) {
                    return '<small>' + row.id + '</small>'
                }
            },
            {
                title: "Folio",
                render: function(data, type, row) {
                    return '<small>' + row.folio + '</small>'
                }
            },
            {
                title: "Concepto",
                render: function(data, type, row) {
                    return '<small>' + row.concepto + '</small>'
                }
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    let numero_cliente = utils.isDefined(row.numero_cliente) ? row.numero_cliente : 's/n';
                    let nombre_completo = row.nombre_cliente + ' ' + row.apellido_paterno + ' ' + row.apellido_materno;
                    return '<small><b>' + numero_cliente + '</b> - ' + nombre_completo + '</small>'
                }
            },
            {
                title: "Número orden",
                render: function(data, type, row) {
                    return '<small> ' + utils.isDefined(row.numero_orden) ? row.numero_orden : '-' + '</small>'
                }
            },
            // {
            //     title: "Tipo Proceso",
            //     render: function(data, type, row) {
            //         return '<small>' + row.proceso + '<br/><b>' + row.clave_poliza + '</b></small>'
            //     }
            // },
            // {
            //     title: "Plazo credito",
            //     render: function(data, type, row) {
            //         return '<small>' + row.plazo_credito + '</small>'
            //     }
            // },
            // {
            //     title: "Estatus",
            //     render: function(data, type, row) {
            //         return '<small>' + row.estatus_cuenta + '</small>'
            //     }
            // },
            {
                title: "Fecha",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha)
                }
            },
            // {
            //     title: "Saldo neto",
            //     data: 'importe',
            //     render: function(data, type, row) {
            //         return data >= 1 ? '$ <small class="money_format">' + parseFloat(data).toFixed(2) + '</small>' : '';
            //     }
            // },
            {
                title: "Monto a pagar",
                data: 'total',
                render: function(data, type, row) {
                    return data >= 1 ? '$ <small class="money_format">' + parseFloat(data).toFixed(2) + '</small>' : '';
                }
            },
            // {
            //     title: "Monto pagado",
            //     data: 'saldo_acomulado',
            //     render: function(data, type, row) {
            //         return data >= 1 ? '$ <small class="money_format">' + parseFloat(data).toFixed(2) + '</small>' : ' ';
            //     }
            // },
            {
                title: "Tipo pago",
                render: function(data, type, row) {
                    return '<small>' + row.tipo_pago + '</small>'
                }
            },
            {
                title: '-',
                width: '120px',
                render: function(data, type, row) {
                    let btn_tipo = '';
                    let btn_detalle = '';
                    let btn_asientos = '';
                    let btn_factura = '';
                    let btn_cancelar = '';
                    let btn_estado_cuenta = '';
                    if (row.estatus_cuenta_id == 1) { //VENTA NUEVA
                        btn_tipo = '<button title="Elegir tipo de pago" onclick="tipoPago(this)" data-folio_id="' + row.folio_id + '" class="btn btn-primary btn-sm"><i class="fas fa-tag"></i></button>';
                    }
                    if (row.estatus_cuenta_id == 5 || row.estatus_cuenta_id == 3) { //VENTA PROCESO PASAR A PAGAR
                        btn_detalle = '<button title="Pagar cuenta" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-warning btn-sm"><i class="fas fa-cash-register "></i></button>';
                    }
                    if (row.estatus_cuenta_id == 2) { // GENERAR FACTURA
                        if (row.tipo_forma_pago_id == 1) {
                            btn_factura = '<button title="Facturar" onclick="generar_factura(this)" data-id="' + row.id + '" class="btn btn-primary btn-sm"><i class="fas fa-file-excel"></i></button>';
                        } else {
                            btn_detalle = '<button title="Detalle pago" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-warning btn-sm"><i class="fas fa-file "></i></button>';
                        }

                    }
                    if (row.estatus_cuenta_id == 2 || row.estatus_cuenta_id >= 6) {
                        btn_estado_cuenta = '<button title="Estado de cuenta" onclick="imprimir_estado_cuenta(this)" data-id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fas fa-file-pdf"></i></button>';
                    }
                    //btn_cancelar = '<button title="Cancelar cuenta" onclick="cancelar_cuenta(this)" data-folio_id="' + row.folio_id + '" data-id="' + row.id + '" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>';
                    btn_asientos = '<button title="Asientos" onclick="asientos(this)" data-id="' + row.id + '" class="btn btn-success btn-sm"><i class="fas fa-list-alt "></i></button>';
                    return btn_tipo + ' ' + btn_detalle + ' ' + btn_asientos + ' ' + btn_estado_cuenta + ' ' + btn_factura + ' ' + btn_cancelar;

                }
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            switch (data['estatus_cuenta_id']) {
                case 2:
                    $(row).find('td:eq(1)').css('background-color', '#8cdd8c');
                    break;
                case 3:
                    $(row).find('td:eq(1)').css('background-color', '#f6ffa4');
                    break;
                case 4:
                    $(row).find('td:eq(1)').css('background-color', '#e37f7f');
                    break;
                default:
                    break;
            }
        }
    });
}

function cancelar_cuenta(_this) {
    var params = {
        'folio': $(_this).data('folio_id'),
        'estatus': 'ANULADO'
    };
    $.ajax({
        dataType: "json",
        type: 'POST',
        data: params,
        url: API_CONTABILIDAD + '/asientos/api/aplicar_folio',
        success: function(response, _, _) {
            console.log(response);
            //filtrar()

        }
    });

}

function detallePago(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/detalle_pago/' + cuenta_por_cobrar;
    }, 200);
}

function asientos(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/asientos/' + cuenta_por_cobrar;
    }, 200);
}

function tipoPago(_this) {
    setTimeout(() => {
        var folio_id = $(_this).data('folio_id');
        window.location.href = PATH + '/caja/entradas/tipoPago/' + folio_id;
    }, 200);
}

function asientos_cuenta(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/asientos_cuenta/' + cuenta_por_cobrar;
    }, 200);
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return '<small>' + fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + '</small>'
}

function filtrar() {
    var params = $.param({
        'folio': $("#folio").val(),
        'numero_orden': $("#numero_orden").val(),
        'cliente_id': $("#cliente_id option:selected").val(),
        'estatus_cuenta_id': $("#estatus_cuenta_id option:selected").val(),
    });

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

function limpiarfiltro() {
    $("#folio").val('');
    $("#numero_orden").val('');
    $("#cliente_id").val('');
    $("#estatus_cuenta_id").val('');
    $("#cliente_id").trigger('change');
    var params = $.param({
        'folio': '',
        'numero_orden': '',
        'cliente_id': '',
        'estatus_cuenta_id': '',
    });

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

function openModal() {
    $('#modal-cuenta').modal('show');
    $('.select2Modal').select2({
        dropdownParent: $('#modal-cuenta')
    });
}


function imprimir_estado_cuenta(_this) {
    cuenta_id = $(_this).data('id');
    window.location.href = PATH + '/caja/entradas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id);
}

function generar_factura(_this) {
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/aplicar-factura`, { cuenta_por_cobrar_id: $(_this).data('id') }, function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog(headers.message, "success", function(data) {
                filtrar();
            })
        }
    })
}

let form_cuenta = function() {
    return {
        tipo_proceso: 7, //checar catalogo de procesos.
        cliente_id: document.getElementById("cliente_modal_id").value,
        estatus_cuenta_id: 1, //EN PROCESO
        tipo_forma_pago_id: 1, //Contado
        tipo_pago_id: 1, //Efectivo
        plazo_credito_id: 14, // una sola exhibicion
        concepto: document.getElementById("concepto").value,
        fecha: document.getElementById("fecha").value,
        venta_total: document.getElementById("venta_total").value,
    };
}

$("#btn-agregar_cuenta").on('click', function() {
    $("#tipo_pago_id").val();
    $("#concepto").val();
    $("#venta_total").val();

    $(".invalid-feedback").html("");
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/set-complemento-cxc`, form_cuenta(), function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog('Cuenta registrada correctamente', "success", function(data) {
                $('#modal-cuenta').modal('hide')
                filtrar();
            })
        }
    })
})

this.inicializaTabla();
$(".select2").select2();
$('.money_format').mask("#,##0.00", { reverse: true });