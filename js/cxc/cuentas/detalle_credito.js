function inicializaTabla() {
    $('#tbl_abonos').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        order: 2,
        "ajax": {
            url: PATH_API + "api/abonos-por-cobrar/listado-abonos-by-orden-entrada?orden_entrada_id=" + cuenta_por_cobrar_id,
            type: 'GET',
        },
        columns: [{
                title: "Num pago",
                render: function(data, type, row) {
                    if (row.tipo_abono_id == 2) {
                        let pagonum = count++;
                        return pagonum + '/' + cantidad_mes;
                    } else {
                        return '-';
                    }
                }
            },
            {
                title: "Tipo abono",
                data: "tipo_abono"
            },
            {
                title: "Fecha vencimiento",
                render: function(data, type, row) {
                    return utils.isDefined(row.fecha_vencimiento) ? obtenerFechaMostrar(row.fecha_vencimiento) : null;
                }
            },
            {
                title: "Monto a abonar",
                render: function(data, type, row) {
                    return parseFloat(row.total_abono).toFixed(2).toLocaleString();
                }
            },
            {
                title: "Fecha pago",
                render: function(data, type, row) {
                    return utils.isDefined(row.fecha_pago) ? obtenerFechaMostrar(row.fecha_pago) : null;
                }
            },
            {
                title: "Monto pagado",
                render: function(data, type, row) {
                    return utils.isDefined(row.total_pago) ? parseFloat(row.total_pago).toFixed(2).toLocaleString() : null;
                }
            },
            {
                title: "Dias <br/>moratorios",
                data: 'dias_moratorios',
            },
            {
                title: "Monto moratorio",
                render: function(data, type, row) {
                    return utils.isDefined(row.monto_moratorio) ? parseFloat(row.monto_moratorio).toFixed(2).toLocaleString() : null;
                }
            },
            {
                title: "Estatus abono",
                data: "estatus_abono"
            }
        ],
        "createdRow": function( row, data, dataIndex ) {
            switch (data['estatus_abono_id']) {
                case 2:
                    $(row).find('td:eq(8)').css('background-color', '#f6ffa4');
                    break;
                case 3:
                    $(row).find('td:eq(8)').css('background-color', '#8cdd8c');
                    if (data['total_abono'] < 1) {
                        $(row).hide();
                    }
                    break;
                default:
                    break;
            }  
        }
    });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

this.inicializaTabla();
$("#proveedor_id").select2();