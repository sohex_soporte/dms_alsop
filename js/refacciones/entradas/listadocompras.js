function inicializaTabla() {
	var tabla_compras = $('#tabla_compras').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/orden-compra/busqueda-compras",
			type: 'GET',
		},
		columns: [{
				title: "Num. Orden",
				data: 'id',
			},
			{
				title: "Folio",
				data: 'folio',
			},
			{
				title: "Proveedor",
				render: function(data, type, row) {
					return row.proveedor_numero + ' - ' + row.proveedor_nombre;
				}
			},
			{
				title: "Estatus",
				data: 'estatusCompra',
			},
			{
				title: "Observaciones",
				data: 'observaciones',
			},
			{
				title: "Fecha de venta",
				render: function(data, type, row) {
                    return obtenerFechaMostrar(row.created_at)

				}
			},
            {
                title: 'ir a Finalizar',
                render: function(data, type, row) {
					if(row.id_estatus_compra == 1){
						return '<button title="Finalizar venta" onclick="finalizarVenta(this)" data-orden_compra_id="' + row.id + '" class="btn btn-default"><i class="far fa-check-square"></i></button>';
					} else {
						return '-';
					}
				}
            },
            {
                title: '-',
                render: function(data, type, row) {
                    return '<button title="Detalle orden compra" onclick="detalleOrden(this)" data-orden_compra_id="' + row.id + '" class="btn btn-default"><i class="fas fa-list"></i></button>';

				}
            }
		]
	});
}

function finalizarVenta(_this){
    setTimeout(() => {
        var orden_compra_id = $(_this).data('orden_compra_id');
        window.location.href = PATH + '/refacciones/entradas/compra/' + orden_compra_id;
    }, 200);
}

function detalleOrden(_this){
    setTimeout(() => {
        var orden_compra_id = $(_this).data('orden_compra_id');
        window.location.href = PATH + '/refacciones/entradas/detalleOrden/' + orden_compra_id;
    }, 200);
}

function obtenerFechaMostrar(fecha){
    const dia = 2, mes = 1, anio = 0;
    fecha = fecha.split(' ');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
        'folio': $("#folio").val(),
        'proveedor_id': $("#proveedor_id option:selected").val(),
    });

    $('#tabla_compras').DataTable().ajax.url(PATH_API + 'api/orden-compra/busqueda-compras?' + params).load()
}
function limpiarfiltro() {
	$("#folio").val('');
	$("#proveedor_id").val('');
	$('#proveedor_id').trigger('change')
    var params = $.param({
        'folio': '',
        'proveedor_id': '',
    });

    $('#tabla_compras').DataTable().ajax.url(PATH_API + 'api/orden-compra/busqueda-compras?' + params).load()
}

this.inicializaTabla();
$("#proveedor_id").select2();
